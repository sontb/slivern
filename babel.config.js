module.exports = {
  presets: ["module:metro-react-native-babel-preset"],
  plugins: [
    ["@babel/plugin-proposal-decorators", { "legacy": true }],
    [
      "babel-plugin-module-resolver", {
        "extensions": [
          ".js",
          ".jsx",
          ".ts",
          ".tsx",
          ".png",
          ".svg",
        ],
        root: [
          "./src"
        ],
        alias: {
          components: './src/components',
          configs: './src/configs',
          helpers: './src/helpers',
          api: './src/api',
          models: './src/models',
          stores: './src/stores',
          assets: './src/assets',
          trans: './src/trans',
          node_customs: './node_customs'
        },
      }
    ]
  ]
};
