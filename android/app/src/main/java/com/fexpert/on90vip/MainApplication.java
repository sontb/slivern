package com.fexpert.on90vip;


import android.app.Application;
import android.util.Log;

// import org.unimodules.adapters.react.ModuleRegistryAdapter;
// import org.unimodules.adapters.react.ReactModuleRegistryProvider;
// import org.unimodules.core.interfaces.SingletonModule;

import com.facebook.react.PackageList;
import com.facebook.hermes.reactexecutor.HermesExecutorFactory;
import com.facebook.react.bridge.JavaScriptExecutorFactory;
import com.facebook.react.ReactApplication;
import com.oblador.vectoricons.VectorIconsPackage;
import com.corbt.keepawake.KCKeepAwakePackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.facebook.react.ReactInstanceManager;
import com.microsoft.codepush.react.CodePush;
// import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
// import com.swmansion.reanimated.ReanimatedPackage;
// import com.facebook.ads.AudienceNetworkAds;
// import suraj.tiwari.reactnativefbads.FBAdsPackage;
// import com.ammarahmed.rnadmob.nativeads.RNAdmobNativePackage;

// import com.PTR.IDFA.IDFAPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import co.apptailor.googlesignin.RNGoogleSigninPackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.horcrux.svg.SvgPackage;
// import com.github.yamill.orientation.OrientationPackage;
import com.brentvatne.react.ReactVideoPackage;
// import io.invertase.firebase.messaging.RNFirebaseMessagingPackage; 
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;
import android.content.Context;
import java.lang.reflect.InvocationTargetException;
// import io.invertase.firebase.admob.RNFirebaseAdMobPackage;
// import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage; 
// import io.invertase.firebase.database.RNFirebaseDatabasePackage;
import java.util.List;
import java.util.Arrays;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
// import expo.modules.ads.facebook.AdsFacebookPackage;

public class MainApplication extends MultiDexApplication implements ReactApplication {

  // private final ReactModuleRegistryProvider mModuleRegistryProvider = new ReactModuleRegistryProvider(new BasePackageList().getPackageList(), null);
  @Override
  protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
  }

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {

        @Override
        protected String getJSBundleFile() {
        return CodePush.getJSBundleFile();
        }
    
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      @SuppressWarnings("UnnecessaryLocalVariable")
      List<ReactPackage> packages = new PackageList(this).getPackages();
      // Packages that cannot be autolinked yet can be added manually here, for example:
      // packages.add(new RNFirebaseMessagingPackage());
      // packages.add(new RNFirebaseAdMobPackage());
      // packages.add(new RNFirebaseNotificationsPackage()); 
      // packages.add(new RNFirebaseDatabasePackage());
      // packages.add(new CodePush(getResources().getString(R.string.reactNativeCodePush_androidDeploymentKey), getApplicationContext(), BuildConfig.DEBUG));
      // List<ReactPackage> unimodules = Arrays.<ReactPackage>asList(
      //   new ModuleRegistryAdapter(mModuleRegistryProvider)
      // );
      // packages.addAll(unimodules);
      // packages.add(new FBAdsPackage());
      // packages.add(new RNAdmobNativePackage());
      packages.add(new ReactVideoPackage());
      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
    initializeFlipper(this, getReactNativeHost().getReactInstanceManager());
    // AudienceNetworkAds.initialize(this);
  }

  private static void initializeFlipper(Context context, ReactInstanceManager reactInstanceManager) {
    if (BuildConfig.DEBUG) {
      try {
        /*
         We use reflection here to pick up the class that initializes Flipper,
        since Flipper library is not available in release mode
        */
        Class<?> aClass = Class.forName("com.fexpert.on90vip.ReactNativeFlipper");
        aClass.getMethod("initializeFlipper", Context.class, ReactInstanceManager.class).invoke(null, context, reactInstanceManager);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      } catch (NoSuchMethodException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
      }
    }
  }
}
