import LocalizedStrings from 'react-native-localization';


export type key = {
  hello: string,
  yeubongda: string,
  kiemtracapnhat: string,
}

export const en: key = {
  hello: "Hello World!",
  yeubongda: "Love fotball",
  kiemtracapnhat: "Checking update..."
}

export const vi: key = {
  hello: "Xin Chao!",
  yeubongda: "Yêu bóng đá aaa",
  kiemtracapnhat: "Kiểm tra cập nhật..."
}

let strings = new LocalizedStrings({
  en,
  vi,
 });