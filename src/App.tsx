// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import BottomTab from 'BottomTab';
// import { createStackNavigator } from '@react-navigation/stack';
import { LoadingGlobal } from 'components';
import { COLOR, EVENT } from 'configs';
import { AdmobHelper, CodePushHelper, FirebaseDbHelper, NavigationService } from 'helpers';
import { Provider } from 'mobx-react';
// import * as FacebookAds from 'node_customs/react-native-fbads';
import React from 'react';
import { Alert, AppState, Image, Linking, LogBox, Platform, StatusBar } from 'react-native';
import codePush from "react-native-code-push";
// import { IDFA } from 'react-native-idfa';
import Orientation, { OrientationType } from 'react-native-orientation-locker';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import { stores } from 'stores';
import { trans } from 'trans';
import SCREENS, { SCREENS_NAME } from './screens';
// import { IronSource } from '@wowmaking/react-native-iron-source';

// import { KeyStorage, TypeStorage } from 'helpers/StorageHelper';
// import moment from 'moment';
// const adsManager = new FacebookAds.NativeAdsManager(AdFanId.native_kqltd, 1);
LogBox.ignoreLogs([
  'source.uri should not be an empty string',
  'Trying to load empty source',
  'Warning: StatusBarIOS has been',
  'DatePickerIOS has been merged with DatePickerAndroid',
  'Require cycle:',
  '[xmldom warning]'
]);
enableScreens();
FirebaseDbHelper.backgroundMessageHandler()
FirebaseDbHelper.notificationOpenedListener();
// FirebaseDbHelper.testScheduleNotification();
// FirebaseDbHelper.scheduleNotificaton('12345', 'test', 'test', { a: 1, b: 2 }, new Date(Date.now() + 60 * 1000).getMilliseconds())
const Stack = createNativeStackNavigator();
// const Tab = createBottomTabNavigator();
class App extends React.Component {

  notificationListener: any;
  // notificationOpenedListener: any;
  constructor(props: any) {
    super(props);
  }

  state = {
    appState: 'active',
  }
  gotoAppStore = () => {
    if (Platform.OS === 'ios') {
      Linking.canOpenURL('itms-apps://itunes.apple.com/us/app/id1549899465?mt=8').then(supported => {
        supported && Linking.openURL('itms-apps://itunes.apple.com/us/app/id1549899465?mt=8');
      }, (err) => console.log(err));
    } else {
      Linking.openURL("market://details?id=com.fexpert.on90vip");
    }
  }

  componentDidMount = async () => {
    try {
      // IDFA.getIDFA().then((idfa) => {
      //   console.log('--- idfa', idfa)
      // })
      //   .catch((e) => {
      //     console.log(e);
      //   });
      stores.setup();
      Orientation.addDeviceOrientationListener(this._deviceOrientationDidChange)
      AppState.addEventListener('change', this._handleAppStateChange);
      FirebaseDbHelper.subscribeToTopic("over90m_live");
      if (EVENT.DEV || __DEV__) {
        FirebaseDbHelper.subscribeToTopic("over90m_live_test");

      }
      if (Platform.OS === 'ios') {
        FirebaseDbHelper.subscribeToTopic("over90m_ios");

      } else {
        FirebaseDbHelper.subscribeToTopic("over90m_android");
      }
      if (__DEV__) {
        if (Platform.OS === 'ios') {
          FirebaseDbHelper.subscribeToTopic("over90m_ios_test");
        } else {
          FirebaseDbHelper.subscribeToTopic("over90m_android_test");
        }
      }
      FirebaseDbHelper.requestPermissionNotification();
      // AdmobHelper.requestPermissionTracking();
      this.notificationListener = FirebaseDbHelper.notificationListener();
      // FacebookAds.AdSettings.addTestDevice(FacebookAds.AdSettings.currentDeviceHash);
      // FacebookAds.AdSettings.clearTestDevices();
      if (Platform.OS === 'ios') {
        // AdmobHelper.setFanAdvertiserTrackingEnabled(true)
      } else {
        if (EVENT.APPSTORE == 1) {
          AdmobHelper.disableSSLCertificateVerify()
        }
      }

      // this.requestUpdateAppStore();
      // stores.loginStore.getUserPredict();

      // IronSource.initializeIronSource(Platform.OS === 'ios' ? 'ee29c0e9' : 'ee3fc6f9', 'on90', {
      //   validateIntegration: true,
      // }).then(() => {
      //   // console.warn('Init finished');
      // }).catch(() => {
      //   console.log("ironsource error")
      // });

      // FirebaseDbHelper.testAddRom()
    } catch (err) {
      console.log(err);
    }
  }


  componentWillUnmount() {
    this.notificationListener && this.notificationListener();
    // this.notificationOpenedListener();
    Orientation.removeDeviceOrientationListener(this._deviceOrientationDidChange)
    AppState.removeEventListener('change', this._handleAppStateChange);
  }
  _deviceOrientationDidChange = (orientation: OrientationType) => {
    let a = orientation
    Orientation.getAutoRotateState(autoRotateState => {
      if (autoRotateState) {
        if (orientation == 'LANDSCAPE-LEFT' || orientation == 'LANDSCAPE-RIGHT') {
          if (orientation == 'LANDSCAPE-LEFT') {
            Orientation.lockToLandscapeLeft()
          } else {
            Orientation.lockToLandscapeRight()
          }
        } else if (orientation == 'PORTRAIT' || orientation == 'PORTRAIT-UPSIDEDOWN') {
          if (orientation == 'PORTRAIT') {
            Orientation.lockToPortrait()
          } else {
            Orientation.lockToPortraitUpsideDown()
          }
        }
      }
    })
  }

  _handleAppStateChange = (nextAppState: any) => {
    let a = this.state.appState
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      stores.commonStore.timeToReload();
      // AdmobHelper.requestPermissionTracking();
      CodePushHelper.checkCodePushUpdate()
      // this.requestUpdateAppStore()
    }
    this.setState({ appState: nextAppState });
  };



  requestUpdateAppStore = () => {
    Alert.alert(
      'Thông báo',
      `Ứng dụng cũ đã bị gỡ, Các bạn vui lòng cài lại ứng dụng mới để tiếp tục xem các trận cầu đỉnh cao. !`,
      [
        {
          text: 'Cài đặt ngay', onPress: async () => {
            this.gotoAppStore();
          }
        },
      ],
      { cancelable: false },
    );
  }


  // BottomTab() {
  //   return (
  //     <Tab.Navigator screenOptions={({ route }) => ({
  //       tabBarVisible: stores.commonStore.tabBarVisible,
  //       tabBarIcon: ({ focused, color, size }) => {
  //         if (route.name === SCREENS_NAME.Match) {
  //           return <Image style={{ width: 24, height: 24, tintColor: color }} source={require('assets/ic_drawer_match.png')} resizeMode='contain' />
  //         } else if (route.name === SCREENS_NAME.Live) {
  //           return <Image style={{ width: 24, height: 24, tintColor: focused ? COLOR.RED_DARK : color }} source={require('assets/ic_drawer_live.png')} resizeMode='contain' />
  //         } else if (route.name === SCREENS_NAME.Tips) {
  //           return <Image style={{ width: 24, height: 24, tintColor: focused ? COLOR.GREEN : color }} source={require('assets/ic_drawer_tips.png')} resizeMode='contain' />
  //         } else if (route.name === SCREENS_NAME.League) {
  //           return <Image style={{ width: 24, height: 24, tintColor: focused ? COLOR.PROFESSION : color }} source={require('assets/ic_drawer_league.png')} resizeMode='contain' />
  //         } else if (route.name === SCREENS_NAME.User) {
  //           return <Image style={{ width: 24, height: 24, tintColor: color }} source={require('assets/ic_tab_user.png')} resizeMode='contain' />
  //         }
  //       },
  //     })}
  //       tabBarOptions={{
  //         activeTintColor: COLOR.MAIN,
  //         inactiveTintColor: COLOR.SUB_COLOR,
          
  //       }}
  //       backBehavior='initialRoute'
  //       detachInactiveScreens={true}
        
  //     >
  //       <Tab.Screen name={SCREENS_NAME.Match} component={SCREENS.Match} options={{ title: trans.tabKQLTD }} />
  //       {
  //         EVENT.APPSTORE == 1 &&
  //         <Tab.Screen name={SCREENS_NAME.Live} component={SCREENS.Live} options={{ title: trans.tabLive }} />
  //       }
  //       <Tab.Screen name={SCREENS_NAME.Tips} component={SCREENS.Tips} options={{ title: trans.tabNhanDinh }} />
  //       <Tab.Screen name={SCREENS_NAME.League} component={SCREENS.League} options={{ title: trans.league }} />
  //       <Tab.Screen name={SCREENS_NAME.User} component={SCREENS.User} options={{ title: trans.tabNguoiDung }} />

  //     </Tab.Navigator>
  //   );
  // }
  render() {
    // return(
    //   <View style={{flex: 1}}>
    //    <TestAd media={true} type='image' delay={100}/>
    //   </View>
    // )
    return (
      <Provider {...stores}>
        <SafeAreaProvider>
          <StatusBar
            translucent
            backgroundColor="rgba(0, 0, 0, 0.2)"
            barStyle="light-content"
            animated
          />
          <NavigationContainer ref={navigatorRef => {
            NavigationService.setAppNavigator(navigatorRef);
          }}>
            <Stack.Navigator

              screenOptions={{
                headerShown: false,
              }}
            >

              <Stack.Screen name={SCREENS_NAME.Splash} component={SCREENS.Splash} />
              <Stack.Screen name={SCREENS_NAME.MainTab} component={BottomTab} />
              <Stack.Screen name={SCREENS_NAME.PlayerAndroid} component={SCREENS.PlayerAndroid} />
              <Stack.Screen name={SCREENS_NAME.TipsDetail} component={SCREENS.TipsDetail} />
              <Stack.Screen name={SCREENS_NAME.Chat} component={SCREENS.Chat} />
              <Stack.Screen name={SCREENS_NAME.TopPredict} component={SCREENS.TopPredict} />
              <Stack.Screen name={SCREENS_NAME.Info} component={SCREENS.Info} />
              <Stack.Screen name={SCREENS_NAME.LeagueTab} component={SCREENS.LeagueTab} />
              <Stack.Screen name={SCREENS_NAME.MatchDetail} component={SCREENS.MatchDetail} />
              <Stack.Screen name={SCREENS_NAME.PredictHistory} component={SCREENS.PredictHistory} />
              <Stack.Screen name={SCREENS_NAME.PointHelp} component={SCREENS.PointHelp} />

              <Stack.Screen name={SCREENS_NAME.Policy} component={SCREENS.Policy} />

            </Stack.Navigator>

          </NavigationContainer>

          <LoadingGlobal />
          {/* </View> */}
          {/* <Text style={{ padding: 10 }}>isEnableHermes {isEnableHermes?'true': 'false'}</Text> */}
        </SafeAreaProvider>

      </Provider>

    )
  }
}

let codePushOptions = { checkFrequency: codePush.CheckFrequency.MANUAL, installMode: codePush.InstallMode.IMMEDIATE };

export default codePush(codePushOptions)(App)


// unimodules-core@5.1.0 from /Users/sontb/workplace/slivern/node_modules/@unimodules/core
//  unimodules-react-native-adapter@