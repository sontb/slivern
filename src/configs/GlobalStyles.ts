import { StyleSheet } from 'react-native';
const GlobalStyles = StyleSheet.create({
  ShadowItemStyle: {
    shadowColor: '#000',
    shadowRadius: 10,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.03,
  }
});
export default GlobalStyles