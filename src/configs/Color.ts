export const COLOR = {
  GREY: '#C7C7CD',
  DISABLE: '#7c7c7c',
  RED: '#FF0000',
  RED_DARK: '#d31e1e',
  WHITE: '#fff',
  BLACK: '#1e1e1e',
  BLACK_DARK: '#000000',
  YELLOW: '#fcd462',
  GREEN: '#37a13b',
  PURPLE: '#551A8B',
  BLUE: '#2196F3',
  NEW_BLUE: '#12579E',
  MAIN_TEXT: 'rgba(0,0,0,0.8)',
  SUB_TEXT: 'rgba(34,53,73,0.6)',
  SUB_TEXT_LIGHT: 'rgb(213,215,217)',
  MAIN: '#A5293D',
  SUB_COLOR: 'rgb(174,174,174)',
  BG: '#efeef4',
  BG_GREY: '#e1e1e1',
  BORDER: 'rgb(234,234,236)',
  TITLE_BOX: '#6D6D72',
  PRIMARY_COLOR: '#055515',
  TEST: '',
  OVERLEY_DARK:'rgba(0,0,0,0.1)',
  OVERLEY_DARK_LIGHT:'rgba(0,0,0,0.05)',

  OVERLEY_DARK_DRAK:'rgba(0,0,0,0.5)',

  TRANS:'rgba(0,0,0,0)',

  BG_OVERLEY_DARK:'rgba(0,0,0,0.3)',
  OVERLEY_LIGHT:'rgba(255,255,255,0.5)',
  OVERLEY_LIGHT_LIGHT:'rgba(255,255,255,0.8)',

  BG_HIGHLIGHT: '#d7f9e9',
  BG_LOWLIGHT: 'rgba(215,249,233,0.5)',

  AMATEUR: '#005b97',
  SEMIPRO: '#a2bf45',
  PROFESSION: '#e19f09',
  EXPERT: '#21c3f3',

  ACCENT: '#FFBA07'
};
