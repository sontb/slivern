import { API } from 'configs';
import { Requests } from 'helpers';

export default class LeagueApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  getData = async () => {
    let response;
    try {
      response = await this.requests.get(API.MATCH_LEAGUEPOPULAR, null);
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
      this.requests.cancelRequest();
  }

}