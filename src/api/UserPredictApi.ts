import axios, { CancelTokenSource } from "axios";
import { Requests, TYPE_NOTIFY } from 'helpers';
import { API } from 'configs';

export default class UserPredictApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  predict = async (email: string, matchId: number, typeResult: number, point: number) => {
    let response;
    try {
      response = await this.requests.get(API.USER_PREDICT, { email, matchId, typeResult, point });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  userPredict = async (uid: string) => {
    let response;
    try {
      response = await this.requests.get(API.USER_PREDICT_INFO, { uid });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  userPredictEmail = async (email: string) => {
    let response;
    try {
      response = await this.requests.get(API.USER_PREDICT_EMAIL, { email });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  userAddRewardPoint = async (email: string) => {
    let response;
    try {
      response = await this.requests.get(API.USER_ADD_REWARD_POINT, { email });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  userCommentMatch = async (email: string, matchId: number, content: string , type: number, mention?: string) => {
    let response;
    try {
      response = await this.requests.get(API.USER_COMMENT_MATCH, { email, matchId, content, mention: mention ? mention : '', type });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  notifyNow = async (mess: string, data?: any, topic?: string, type?: string) => {
    let response;
    let edata = encodeURIComponent(JSON.stringify(data))
    try {
      response = await this.requests.get(API.USER_PUSH_NOTIFY, { title: 'Bình luận mới', mess, type, edata, version: topic }, true);
      // ?title=Trực tiếp&mess=Newcastle - Tottenham&type=1
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  userListComment = async (matchId: number, page: number, item: number, type: number) => {
    let response;
    try {
      response = await this.requests.get(API.USER_LIST_COMMENT, { matchId, page, item, type });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }
}