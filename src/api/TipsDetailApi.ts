import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';

export default class TipsDetailApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  getData = async (href: string) => {
    let response;
    try {
      response = await this.requests.get(API.MATCH_DETAILTIPS, { href });
      console.log('response tips'); 
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }

}