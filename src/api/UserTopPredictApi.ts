import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';

export default class UserTopPredictApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  getData = async (page: number, item: number) => {
    let response;
    try {
      response = await this.requests.get(API.USER_TOP_PREDICT, {page, item});
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }
}