import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';

export default class LiveApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  timeoutout: any;
  getData = async () => {
    let response;
    try {
      response = await this.requests.get(API.CHANNELS_LIST, null);
    } catch (err) {
      console.log(err);
    }
    return response;
  }


  getDataV2 = async () => {
    let response;
    try {
      response = await this.requests.get(API.CHANNELS_LIST_V2, null);
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  updateNotify = async (id: number, notify: number) => {
    let response;
    try {
      response = await this.requests.get(API.CHANNELS_UPDATE_NOTIFY, { id, notify });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  notifyNow = async (mess: string, data?: any) => {
    let response;
    let edata = encodeURIComponent(JSON.stringify(data))
    try {
      response = await this.requests.get(API.USER_PUSH_NOTIFY, { title: 'Trực tiếp', mess, type: 1, edata, version: 'over90m_live' }, true);
      // ?title=Trực tiếp&mess=Newcastle - Tottenham&type=1
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  delete = async (id: number) => {
    let response;
    try {
      response = await this.requests.get(API.CHANNELS_DELETE, { id });
      // ?title=Trực tiếp&mess=Newcastle - Tottenham&type=1
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }

}