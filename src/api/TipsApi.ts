import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';

export default class TipsApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  getData = async (page: number) => {
    let response;
    try {
      response = await this.requests.get(API.MATCH_LISTIPS, { page });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }
}