import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';
export default class MatchOddApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  getListMatchOdd = async (date: string, page: number, uid: string) => {
    let response;
    try {
      response = await this.requests.get(API.MATCH_ODDS, { date, page, item: 20, uid });
    } catch (err) {
      console.log(err);
    }
    // console.log('responsesss ' + date);
    return response;
  }
  
  getMatchDetail = async (matchId: number, email: string) => {
    let response;
    try {
      response = await this.requests.get(API.MATCH_DETAIL, { matchId, email });
    } catch (err) {
      console.log(err);
    }
    // console.log('responsesss ' + date);
    return response;
  }
  

  getListMatchPredict = async (email: string, page: number, item: number) => {
    let response;
    try {
      response = await this.requests.get(API.MATCH_PREDICT, { email, page, item });
    } catch (err) {
      console.log(err);
    }
    // console.log('responsesss  ' + date);
    return response;
  }

  getListLiveMatch = async () =>{
    let response;
    try {
      let res = await this.requests.getFullUrl('http://lichthidau.com.vn/process/refresh_data_content.jsp',{});
      response = res.content
    } catch (err) {
      console.log(err);
    }
    // console.log('responsesss  ' + date);
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }

}