import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';

export default class NewsApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  getData = async (page: number, category: number) => {
    let response;
    try {
      response = await this.requests.get(API.MATCH_LISTNEWS, { page, category });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  getListNews = async (categoryId: number, page: number, item: number) => {
    let response;
    try {
      response = await this.requests.get(API.NEWS_LIST, { categoryId, page, item });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  readNews = async (href: string) => {
    let response;
    try {
      response = await this.requests.get(API.NEWS_READ, { href });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  delete = async (id: number) =>{
    let response;
    try {
      response = await this.requests.get(API.NEWS_DELETE, { id });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  notifyNow = async ( title: string, data: any)=>{
    let response;
    let edata = encodeURIComponent(JSON.stringify(data))
    try {
      response = await this.requests.get(API.USER_PUSH_NOTIFY, { title: 'Tin hot', mess: title, type: 4, edata, version: 'over90m_live' }, true);
      // ?title=Trực tiếp&mess=Newcastle - Tottenham&type=1
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }
}