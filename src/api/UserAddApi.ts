import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';

export default class UserAddApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }

  add = async (uid: string, name: string, email: string, photo: string, fcmToken: string) => {
    let response;
    try {
      response = await this.requests.get(API.USER_ADD, { uid, name, email, photo, fcmToken });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }
}