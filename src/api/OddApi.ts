import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';
import { IItemOdds } from "screens/odds/ItemOdds";

export default class LiveApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  timeoutout: any;
  getData = async (type: string) => {
    let listOdds;
    try {
      listOdds = await this.requests.getFullUrl(API.ODDS_LIST, { type });
      for (let i = 0; i < listOdds.length; i++) {
        let item: IItemOdds = listOdds[i];
        if (item.Event1 === `''` || item.Event2 === `''`) {
          listOdds.splice(i, 1);
          i--;
        }
        item.Event1 = item.Event1.replace(`'`, '').replace(`'`, '');
        item.Event2 = item.Event2.replace(`'`, '').replace(`'`, '');
        item.Giaidau = item.Giaidau.replace(`'`, '').replace(`'`, '');
        item.FTHDP1 = item.FTHDP1.replace(`'`, '').replace(`'`, '');
        item.FTHDP2 = item.FTHDP2.replace(`'`, '').replace(`'`, '');
        item.FTHDP3 = item.FTHDP3.replace(`'`, '').replace(`'`, '');
        item.FTOU1 = item.FTOU1.replace(`'`, '').replace(`'`, '');
        item.FTOU2 = item.FTOU2.replace(`'`, '').replace(`'`, '');
        item.FTOU3 = item.FTOU3.replace(`'`, '').replace(`'`, '');
        item.HHDP1 = item.HHDP1.replace(`'`, '').replace(`'`, '');
        item.HHDP2 = item.HHDP2.replace(`'`, '').replace(`'`, '');
        item.HHDP3 = item.HHDP3.replace(`'`, '').replace(`'`, '');
        item.HOU1 = item.HOU1.replace(`'`, '').replace(`'`, '');
        item.HOU2 = item.HOU2.replace(`'`, '').replace(`'`, '');
        item.HOU3 = item.HOU3.replace(`'`, '').replace(`'`, '');
        item.Time = item.Time.replace('<font color=red>LIVE</font> ', '').replace(`\\''`, '');
      }
      listOdds.sort((a: any, b: any) => {
        try {
          if (type === 'e') {
            return 0
          }
          return a.GID - b.GID;
        } catch (err) {
          console.log(err);
        }
        return 0;
      });
      console.log('listOdds');
    } catch (err) {
      console.log(err);
    }
    return listOdds;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }

}