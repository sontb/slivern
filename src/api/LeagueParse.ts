import moment from 'moment';
import HTMLParser from 'react-native-html-parser';
import { IRankData } from 'screens/league/LeagueRank';
import { IItemMatch } from 'screens/match/ItemMatch';
import { IMatch, IPlayer } from 'screens/match/MatchDetail';

var DomParser = HTMLParser.DOMParser

const getDocument = async (url: string) => {
  console.log(url)

  let res = await fetch(url, {
    headers: {
      'Cache-Control': 'no-store'
    }
  });
  let body = await res.text()
  let doc = new DomParser().parseFromString(body, 'text/html')
  return doc;
}

const getListMatch = (doc: any): IItemMatch[] => {
  let listMatch: IItemMatch[] = []
  try {
    let eListTable = doc.getElementsByClassName('Table_LTD')[0].getElementsByTagName('table')
    for (let i = 0; i < eListTable.length; i++) {
      if (i > 0) {
        let leagueName = "";

        let elementLeague = eListTable[i]
        leagueName = elementLeague.getElementsByClassName('LTD_table_row_title')[0]
          .getElementsByTagName("h2")[0]
          .getElementsByTagName("a", false)[0].textContent
        let eListMatch = elementLeague.getElementsByClassName("LTD_table_row_1")
        for (let j = 0; j < eListMatch.length; j++) {
          try {
            let elementMatch = eListMatch[j]
            let elementTime = elementMatch.getElementsByClassName("LTD_tabale_col_1")[0];
            let status = 0; // 0 kết quả, 1 lịch thi đấu, 2 trực tiếp.
            let homeName = ""
            let awayName = ""
            let timeExt = "";
            let dateTime = 0;
            let id = 0;
            let hrefMatch = "";
            let goalHome = 0;
            let goalAway = 0;
            try {
              id = Number(elementTime.getAttribute("id").split("_")[1])
            } catch (err) {
              console.log(err)
            }
            let score = "0 - 0"
            hrefMatch = elementMatch.getElementsByTagName("a")[0].getAttribute('href');
            if (elementTime.getElementsByTagName("img").length > 0 || elementTime.textContent.includes("HT")) {
              status = 2;
              score = elementTime.getElementsByTagName("strong")[0].textContent;
              timeExt = elementTime.getElementsByTagName("span")[0].textContent;
            } else if (elementTime.getElementsByTagName("strong").length > 0) {
              score = elementTime.getElementsByTagName("strong")[0].textContent;
              if (score === 'Hoãn') {
                status = 4
              } else {
                status = 0;
              }

            } else {
              status = 1;
              let dateTime1 = elementTime.textContent.replace(/\s\s+/g, ' ').replace('h', ':');
              dateTime = moment(dateTime1, 'DD/MM HH:mm').unix()
            }
            if (status != 1 && status != 4) {
              try {
                let scoreArr = score.split("-")
                goalHome = Number(scoreArr[0].replace(" ", ""))
                goalAway = Number(scoreArr[1].replace(" ", ""))

              } catch {

              }
            }

            let matchStrElement = elementMatch.getElementsByClassName("LTD_tabale_col_2")[0];
            let sups = matchStrElement.getElementsByTagName('sup')
            let lengthSups = sups.length
            if (lengthSups > 0) {
              for (let pp = 0; pp < lengthSups; pp++) {
                matchStrElement.removeChild(sups[pp])
              }
            }
            let matchStr = matchStrElement.textContent
            homeName = matchStr.split("vs")[0].trim();
            awayName = matchStr.split("vs")[1].trim();
            let eListTd = elementMatch.getElementsByTagName("td");
            let a1 = eListTd[2].textContent.trim();
            let a2 = eListTd[3].textContent.trim();
            let a3 = eListTd[4].textContent.trim();
            let b1 = eListTd[5].textContent.trim();
            let b2 = eListTd[6].textContent.trim();
            let b3 = eListTd[7].textContent.trim();
            let c1 = eListTd[8].textContent.trim();
            let c2 = eListTd[9].textContent.trim();
            let c3 = eListTd[10].textContent.trim();
            let d1 = eListTd[11].textContent.trim();
            let d2 = eListTd[12].textContent.trim();
            let d3 = eListTd[13].textContent.trim();
            listMatch.push({
              id,
              leagueName,
              homeName,
              awayName,
              status,
              timeExt,
              dateTime,
              canPredict: 0,
              a1,
              a2,
              a3,
              b1,
              b2,
              b3,
              c1,
              c2,
              c3,
              d1,
              d2,
              d3,
              hrefMatch,
              goalHome,
              goalAway,
            })
          } catch (err) {
            console.log(err)
          }
        }
      }
    }
  } catch (err) {
    console.log(err)
  }
  return listMatch
}

const getRoundData = (doc: any): { listRound: { name: string, href: string }[], selected: number } => {
  let listRound = []
  let selected = 0
  try {
    let eListRound = doc.getElementsByClassName('selectpicker')[0].getElementsByTagName('option')
    for (let i = 0; i < eListRound.length; i++) {
      let eRound = eListRound[i]
      listRound.push({
        name: eRound.textContent.trim(),
        href: 'https://lichthidau.com/' + eRound.getAttribute('value')
      })
      if (eRound.hasAttribute('selected')) {
        selected = i
      }
    }
  } catch (err) {
    console.log(err)
  }
  return { listRound, selected }
}

const getListMatchFromHref = async (href: string) => {
  let doc = await getDocument(href)
  let listMatch = getListMatch(doc)
  return listMatch
}

const getListRank = (doc: any): IRankData[] => {
  let listRank: IRankData[] = []
  try {
    let eListTable = doc.getElementsByClassName('BXH_table_content')[0].getElementsByTagName('table')[0].getElementsByTagName('tr')
    if (!eListTable[0].hasAttribute("bgcolor")) {
      listRank.push({
        Title: 'regular',
        Data: []
      })
    }
    for (let i = 0; i < eListTable.length; i++) {
      let eTable = eListTable[i]
      if (!eTable.hasAttribute("bgcolor")) {
        let DataH = ""
        let DataHS = ""
        let DataB = ""
        let DataT = ""
        let DataD = ""
        let TeamName = ""
        let DataST = ""
        TeamName = eTable.getElementsByClassName('BXH_table_content_col_2')[0].getElementsByTagName('a')[0].textContent.trim()
        DataST = eTable.getElementsByClassName('BXH_table_content_col_3')[0].textContent.trim()
        DataT = eTable.getElementsByClassName('BXH_table_content_col_3')[1].textContent.trim()
        DataH = eTable.getElementsByClassName('BXH_table_content_col_3')[2].textContent.trim()
        DataB = eTable.getElementsByClassName('BXH_table_content_col_3')[3].textContent.trim()
        let DataBT = eTable.getElementsByClassName('BXH_table_content_col_3')[4].textContent.trim()
        let DataBB = eTable.getElementsByClassName('BXH_table_content_col_3')[5].textContent.trim()
        DataHS = `${Number(DataBT) - Number(DataBB)}`
        DataD = eTable.getElementsByClassName('BXH_table_content_col_4')[1].textContent.trim()

        listRank[listRank.length - 1].Data.push({
          DataH,
          DataHS,
          DataB,
          DataT,
          DataD,
          TeamName,
          DataST,
        })
      } else {
        let tableTitle = eTable.getElementsByTagName('span')[0].textContent
        listRank.push({
          Title: tableTitle,
          Data: []
        })
      }

    }
  } catch (err) {
    console.log(err)
  }
  return listRank
}

const getLeagueDetail = async (href: string) => {
  let doc = await getDocument(href)
  return {
    listMatch: getListMatch(doc),
    listRank: getListRank(doc),
    roundData: getRoundData(doc),
  }

}

const getMatchDetail = async (href: string, matchId: number, status: number) => {
  let doc = await getDocument(href)
  let matchDetail = {

  }
  let logo1 = ""
  let logo2 = ""
  // let date = ""
  // let score = ""
  // let scoreH1 = ""
  let leagueName = ""
  let goal1: string[] = []
  let goal2: string[] = []
  let players1: IPlayer[] = []
  let players2: IPlayer[] = []
  let subPlayers1: IPlayer[] = []
  let subPlayers2: IPlayer[] = []
  let statistics = []
  let win1 = 0
  let win2 = 0
  let draw = 0
  let listMatch: IMatch[] = []
  let listMatch1: IMatch[] = []
  let listMatch2: IMatch[] = []
  let scoreExt: string[] = []
  try {
    let eDetail = doc.getElementsByClassName('TRANDAU_TYSO_bg')[0]
    leagueName = eDetail.getElementsByTagName('p')[0].textContent.trim()
    let eListLogo = eDetail.getElementsByClassName('TRANDAU_TYSO_1')
    logo1 = eListLogo[0].getElementsByTagName('img')[0].getAttribute('src')
    logo2 = eListLogo[1].getElementsByTagName('img')[0].getAttribute('src')
    let eScore = eDetail.getElementsByClassName('TRANDAU_TYSO_3')[0]
    // let eScoreResult = eScore.getElementsByClassName('TRANDAU_TYSO_boxts')
    // let eScoreTime = eScore.getElementsByClassName('kq_Trandau')
    if (status == 0) {
      try {
        scoreExt = eDetail.getElementsByClassName('trd_note')[0].textContent
          .trim().split(';').filter((item: string) => item.includes('HP'))
      } catch {

      }
    }
    // if (eScoreResult.length > 0) {
    //   score = eScoreResult[0].textContent.trim()
    //   date = "FT"
    //   let eScoreH1 = eScore.getElementsByClassName('h1_Trandau')
    //   if (eScoreH1.length > 0) {
    //     scoreH1 = eScoreH1[0].textContent.trim()
    //   }
    // } else if (eScoreTime.length > 0) {
    //   score = eScoreTime[0].textContent.trim()
    //   date = eScore.getElementsByClassName('event_Trandau')[0].textContent.trim()

    // }
  } catch (err) {
    console.log(err)
  }

  if (status == 0 || status == 2) {
    let goal12 = await getMatchGoallLive(matchId.toString())
    goal1 = goal12.goal1
    goal2 = goal12.goal2
  }

  try {
    let eListPlayer1 = doc.getElementsByClassName('TRANDAU_DOIHINH_content_right')
    let player1all = getPlayer1FromElements(eListPlayer1)
    players1 = player1all.players
    subPlayers1 = player1all.subPlayers
    let eListPlayer2 = doc.getElementsByClassName('TRANDAU_DOIHINH_content_left')
    let player2all = getPlayer1FromElements(eListPlayer2, true)
    players2 = player2all.players
    subPlayers2 = player2all.subPlayers
  } catch (err) {
    console.log(err)
  }
  try {
    let eListStatistics = doc.getElementsByClassName('TRANDAU_TRUCTIEP_TK_chitiet')[0].getElementsByTagName('tr')
    for (let i = 0; i < eListStatistics.length; i++) {
      try {
        let eStatistic = eListStatistics[i].getElementsByTagName('td')
        let valueStr1 = eStatistic[0].textContent.trim()
        let value1 = 0;
        let name = eStatistic[1].textContent.trim()
        let valueStr2 = eStatistic[2].textContent.trim()
        let value2 = 0
        try {
          value1 = Number(valueStr1.replace('%', '').replace(/ *\([^)]*\) */g, ""))
          value2 = Number(valueStr2.replace('%', '').replace(/ *\([^)]*\) */g, ""))
        } catch {

        }
        statistics.push({ name, value1, value2, valueStr1, valueStr2 })
      } catch {

      }
    }
  } catch (err) {
    console.log(err)
  }

  try {
    let eListMatchAll = doc.getElementsByClassName('PHONGDO_DOIDAU_table')
    try {
      let eListMatch = eListMatchAll[0]
      listMatch = getListMatchFromElement(eListMatch)
    } catch {

    }
    try {
      let eListMatch1 = eListMatchAll[1]
      listMatch1 = getListMatchFromElement(eListMatch1)
    } catch {

    }
    try {
      let eListMatch2 = eListMatchAll[2]
      listMatch2 = getListMatchFromElement(eListMatch2)
    } catch {

    }

  } catch {

  }

  try {
    let eWinMatch = doc.getElementsByClassName('TRANDAU_PHONGDO_BIEUDO')[0]
    let win1Str = eWinMatch.getElementsByClassName('TRANDAU_PHONGDO_BIEUDO_1')[0]
      .getElementsByTagName('span')[0]
      .textContent.trim().replace('%', '')
    win1 = Math.round(Number(win1Str) * listMatch.length / 100)
    let drawStr = eWinMatch.getElementsByClassName('TRANDAU_PHONGDO_BIEUDO_2')[0]
      .getElementsByTagName('span')[0]
      .textContent.trim().replace('%', '')
    draw = Math.round(Number(drawStr) * listMatch.length / 100)
    let win2Str = eWinMatch.getElementsByClassName('TRANDAU_PHONGDO_BIEUDO_3')[0]
      .getElementsByTagName('span')[0]
      .textContent.trim().replace('%', '')
    win2 = Math.round(Number(win2Str) * listMatch.length / 100)
    console.log("")

  } catch (err) {
    console.log(err)
  }

  matchDetail = {
    logo1,
    logo2,
    leagueName,
    goal1,
    goal2,
    players1,
    players2,
    subPlayers1,
    subPlayers2,
    statistics,
    listMatch,
    listMatch1,
    listMatch2,
    win1,
    draw,
    win2,
    scoreExt
  }
  return matchDetail
}

const getMatchGoallLive = async (matchId: string): Promise<{ goal1: string[], goal2: string[] }> => {
  let goal1 = []
  let goal2 = []
  try {
    let doc = await getDocument('http://lichthidau.com.vn/process/match_live.jsp?mid=' + matchId)
    let eListEvent = doc.getElementsByTagName('li')
    for (let i = 0; i < eListEvent.length; i++) {
      let eEvent = eListEvent[i]
      let eEvent1 = eEvent.getElementsByClassName('TRANDAU_TRUCTIEP_dienbien_1')[0]
      let minute = eEvent.getElementsByClassName('TRANDAU_TRUCTIEP_dienbien_2')[0].
        getElementsByTagName('p')[0].textContent.trim()
      let eEvent2 = eEvent.getElementsByClassName('TRANDAU_TRUCTIEP_dienbien_3')[0]
      let name1Str = getNameGoal(eEvent1)
      let name2Str = getNameGoal(eEvent2, true)
      if (name1Str) {
        goal1.push(name1Str + ' - ' + minute)
      }
      if (name2Str) {
        goal2.push(minute + ' - ' + name2Str)
      }
    }
  } catch (err) {
    console.log(err)
  }
  return { goal1, goal2 }
}

const getNameGoal = (eEvent12: any, isRight?: boolean): string => {
  let name = ''
  try {
    let attr = eEvent12.getElementsByTagName('img')[0].getAttribute('src')
    if (attr.includes('ghiban.png')) {
      name = eEvent12.textContent.trim()
      if (name.includes('GOAL OVERTURNED BY VAR')) {
        name = ''
      }
    } else if (attr.includes('goalpenalty.png')) {
      if (isRight) {
        name = '(P) ' + eEvent12.textContent.trim()
      } else {
        name = eEvent12.textContent.trim() + ' (P)'
      }
    } else if (attr.includes('ownergoal.png')) {
      if (isRight) {
        name = '(OG) ' + eEvent12.textContent.trim()
      } else {
        name = eEvent12.textContent.trim() + ' (OG)'
      }
    }
  } catch {

  }
  return name
}

const getListMatchFromElement = (element: any) => {
  let listMatch = []
  try {
    let eListMatch = element.getElementsByTagName('tr')
    for (let i = 0; i < eListMatch.length; i++) {
      let eMatch = eListMatch[i];
      let date = eMatch.getElementsByClassName('PHONGDO_DOIDAU_table_col_1')[0].textContent.trim()
      let score = '0 - 0'
      let eWinMatch = eMatch.getElementsByClassName('PHONGDO_DOIDAU_table_col_3 PHONGDO_DOIDAU_table_bg_thang')
      let eLostMatch = eMatch.getElementsByClassName('PHONGDO_DOIDAU_table_col_3 PHONGDO_DOIDAU_table_bg_thua')
      let eDawMatch = eMatch.getElementsByClassName('PHONGDO_DOIDAU_table_col_3 PHONGDO_DOIDAU_table_bg_hoa')
      if (eWinMatch.length > 0) {
        score = eWinMatch[0].textContent.trim()
      } else if (eLostMatch.length > 0) {
        score = eLostMatch[0].textContent.trim()
      } else if (eDawMatch.length > 0) {
        score = eDawMatch[0].textContent.trim()
      }
      let result = 2
      let name1 = ''
      let name2 = ''
      let eNameBold1 = eMatch.getElementsByClassName('PHONGDO_DOIDAU_table_col_2 bold')
      let eNameBold2 = eMatch.getElementsByClassName('PHONGDO_DOIDAU_table_col_4 bold')
      if (eNameBold1.length > 0) {
        result = 1
        name1 = eNameBold1[0].textContent.trim()
      } else {
        name1 = eMatch.getElementsByClassName('PHONGDO_DOIDAU_table_col_2 ')[0].textContent.trim()
      }
      if (eNameBold2.length > 0) {
        result = 3
        name2 = eNameBold2[0].textContent.trim()
      } else {
        name2 = eMatch.getElementsByClassName('PHONGDO_DOIDAU_table_col_4 ')[0].textContent.trim()
      }
      listMatch.push({ date, name1, name2, result, score })
    }
  } catch (err) {
    console.log(err)
  }
  return listMatch
}

const getPlayer1FromElements = (eListPlayer1: any, isRight?: boolean) => {
  let players = []
  let subPlayers = []
  let className = ""
  let classNumber = ""
  if (isRight) {
    classNumber = 'TRANDAU_DOIHINH_content_number '
    className = 'TRANDAU_DOIHINH_content_text'
  } else {
    classNumber = 'TRANDAU_DOIHINH_content_number_2 '
    className = 'TRANDAU_DOIHINH_content_text_2'
  }
  for (let i = 0; i < eListPlayer1.length; i++) {
    let ePlayer = eListPlayer1[i]
    let name = ePlayer.getElementsByClassName(className)[0].textContent.trim()
    let type = 0
    let number = ""
    let eTM = ePlayer.getElementsByClassName(classNumber + 'TRANDAU_DOIHINH_bg_TM')
    let eHV = ePlayer.getElementsByClassName(classNumber + 'TRANDAU_DOIHINH_bg_HV')
    let eTV = ePlayer.getElementsByClassName(classNumber + 'TRANDAU_DOIHINH_bg_TV')
    let eTD = ePlayer.getElementsByClassName(classNumber + 'TRANDAU_DOIHINH_bg_TĐ')
    let eSub = ePlayer.getElementsByClassName(classNumber)

    if (eTM.length > 0) {
      type = 1;
      number = eTM[0].textContent.trim()
    } else if (eHV.length > 0) {
      type = 2;
      number = eHV[0].textContent.trim()
    } else if (eTV.length > 0) {
      type = 3;
      number = eTV[0].textContent.trim()
    } else if (eTD.length > 0) {
      type = 4;
      number = eTD[0].textContent.trim()
    } else {
      number = eSub[0].textContent.trim()
    }
    if (type == 0) {
      subPlayers.push({ name, type, number })
    } else {
      players.push({ name, type, number })
    }
  }
  return { players, subPlayers }
}

export default {
  getLeagueDetail,
  getListMatchFromHref,
  getMatchDetail
}