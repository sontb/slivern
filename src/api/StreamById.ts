import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';

export default class StreamById {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  getData = async (channelId: string) => {
    let response;
    try {
      response = await this.requests.get(API.CHANNELS_STREAMBYID, { channelId });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }

}