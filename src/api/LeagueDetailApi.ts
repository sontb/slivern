import axios, { CancelTokenSource } from "axios";
import { Requests } from 'helpers';
import { API } from 'configs';

export default class LeagueDetaiApi {
  requests: Requests;
  constructor() {
    this.requests = new Requests();
  }
  getData = async (leagueId: number) => {
    let response;
    try {
      response = await this.requests.get(API.MATCH_LEAGUEDETAIL, { leagueId });
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async () => {
    this.requests.cancelRequest();
  }
}