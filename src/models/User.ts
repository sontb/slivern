export default interface User {
  displayName: string;
  email: string;
  photo: string; 
  uid: string;
}