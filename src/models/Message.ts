export default interface Message {
  id: string;
  text: string;
  name: string;
  timestamp?: number;
  key?: string;
  photo?: string;
  score: number;
  position: number;
}