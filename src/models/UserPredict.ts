export default interface UserPredict {

  position: number;
  guide: string;
  guideId: number;
  guideId1: number;

  point: number;
}