// import React, { Component } from 'react';
// import { View, Text, Dimensions } from 'react-native';

// import * as FacebookAds from 'node_customs/react-native-fbads';
// import { COLOR } from 'configs';
// const { AdIconView, MediaView, TriggerableView, AdChoicesView } = FacebookAds;

// const screenWidth = Dimensions.get('window').width
// type Props = { nativeAd: any,alignTop?: any }
// let styleTop = { top: 0, left: 0, right: 0 };
// let styleBottom = { bottom: 0, left: 0, right: 0  };
// class AdComponentSmall extends React.PureComponent<Props> {

//     // shouldComponentUpdate(nextProps: Props, nextState: any) {
//     //   let a = nextProps.nativeAd.headline !== this.props.nativeAd.headline;
//     //   return a;
//     // }

//     render() {
//         let { nativeAd } = this.props;
//         return (
//             <View style={[{ backgroundColor: COLOR.RED, position: 'absolute' }, this.props.alignTop ? styleTop : styleBottom]}>
//                 <View style={{ flexDirection: 'row', backgroundColor: COLOR.WHITE }}>
//                     <AdIconView style={{ width: 60, height: 60 }} />

//                     <View style={{ flex: 1, marginLeft: 10, justifyContent: 'center' }}>
//                         <TriggerableView>
//                             <Text style={{ color: COLOR.MAIN_TEXT, fontSize: 16 }} numberOfLines={1}>{nativeAd.headline}</Text>
//                         </TriggerableView>
//                         <Text style={{ color: COLOR.MAIN_TEXT, fontSize: 14, marginTop: 5 }} numberOfLines={2}>{nativeAd.bodyText}</Text>

//                         {/* <Text style={{ color: COLOR.SUB_TEXT, fontSize: 13, marginTop: 3 }} numberOfLines={1}>{nativeAd.sponsoredTranslation}</Text> */}
//                     </View>
//                     <View style={{ backgroundColor: COLOR.GREEN, alignSelf: 'stretch', justifyContent: 'center' }}>
//                         <TriggerableView >
//                             <View style={{ padding: 10 }}>
//                                 <Text style={{ color: COLOR.WHITE, fontSize: 15 }}>{nativeAd.callToActionText}</Text>
//                             </View>
//                         </TriggerableView>
//                     </View>
//                     <AdChoicesView style={{ backgroundColor: COLOR.OVERLEY_LIGHT, position: 'absolute', left: 30, bottom: 0 }} iconSize={15} />

//                 </View>
//                 <AdMediaView style={{ width: 0, height: 0}} />

//             </View>
//         );
//     }
// }

// export default AdComponentSmall;