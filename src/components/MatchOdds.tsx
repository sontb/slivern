import { COLOR } from 'configs'
import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { IItemMatch } from 'screens/match/ItemMatch'

type Props = { itemMatch: IItemMatch }

const MatchOdds = ({ itemMatch }: Props) => {
  if ((itemMatch.a1 !== '' && itemMatch.a2 !== '' && itemMatch.a3 != '' &&
    (itemMatch.status === 1 || itemMatch.status === 2))) {

    return (
      <View style={{}}>
        <View style={styles.rowOdd}>
          <View style={[styles.cellOdd, { borderRightColor: COLOR.BORDER, borderRightWidth: 1 }]}>
            <Text style={styles.cellFont}>FT</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.a1}</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.a2}</Text>
          </View>
          <View style={[styles.cellOdd, { borderRightColor: COLOR.BORDER, borderRightWidth: 1 }]}>
            <Text style={styles.cellFont}>{itemMatch.a3}</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.c1}</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.c2}</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.c3}</Text>
          </View>

        </View>
        <View style={[styles.rowOdd]}>
          <View style={[styles.cellOdd, { borderRightColor: COLOR.BORDER, borderRightWidth: 1 }]}>
            <Text style={styles.cellFont}>HT</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.b1}</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.b2}</Text>
          </View>
          <View style={[styles.cellOdd, { borderRightColor: COLOR.BORDER, borderRightWidth: 1 }]}>
            <Text style={styles.cellFont}>{itemMatch.b3}</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.d1}</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.d2}</Text>
          </View>
          <View style={styles.cellOdd}>
            <Text style={styles.cellFont}>{itemMatch.d3}</Text>
          </View>

        </View>
      </View>

    )
  } else {
    return null
  }
}

const styles = StyleSheet.create({
  rowOdd: { borderTopColor: COLOR.BORDER, borderTopWidth: 1, flexDirection: 'row' },
  cellOdd: { flex: 1, justifyContent: 'center', alignItems: 'center', padding: 2 },
  cellFont: { fontSize: 12 },
})

export default MatchOdds