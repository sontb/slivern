import { COLOR } from 'configs';
import React, { Component, useEffect, useState } from 'react';
import {
  Platform,
  View,
  Text,
  FlatList,
  StyleProp,
  ViewStyle,
  TouchableOpacity,
  ListRenderItem
} from 'react-native';
import moment from 'moment';
import { trans } from 'trans';
import { useRef } from 'react';

type Props = { curentDate: Date, onSelectedDate: Function, style?: StyleProp<ViewStyle> }
const ITEM_WIDTH = 70

const today = moment().startOf('day')

const getListDateOfMonth = (selectedDate?: Date) => {
  selectedDate = selectedDate ?? today.toDate()
  const curMonth = selectedDate.getMonth()
  const curYear = selectedDate.getFullYear()
  const tempDate = new Date(curYear, curMonth + 1, 0);
  const maxDateOfMonth = tempDate.getDate()
  let listDate = []
  for (var i = 0; i < maxDateOfMonth; i++) {
    let d = new Date()
    d.setHours(0, 0, 0, 0)
    d.setMonth(curMonth)
    d.setFullYear(curYear)
    d.setDate(i + 1)
    listDate[i] = d
  }
  return listDate
}

const CalendarCustom = (props: Props) => {
  const flatList = useRef<FlatList>(null)
  const [selectedDate, setSelectedDate] = useState<Date>(props.curentDate)
  const [listDateOfMonth, setListDateOfMonth] = useState<Date[]>()
  const previewSelectedDate = useRef(props.curentDate)
  useEffect(() => {
    const listDate = getListDateOfMonth(selectedDate)
    setListDateOfMonth(listDate)
  }, [])

  useEffect(() => {
    if (!moment(selectedDate).isSame(previewSelectedDate.current)) {
      previewSelectedDate.current = selectedDate
      props.onSelectedDate(selectedDate)

    }

  }, [selectedDate])

  const scrollToDate = (date: Date, animation?: boolean) => {
    let index = 0
    if (date?.getDate() - 1 > 0) {
      index = date.getDate() - 1
    }
    flatList.current?.scrollToIndex({ index, animated: !!animation, viewOffset: 50 })
    // this.flatlist.scrollToOffset({x: ITEM_WIDTH* index - 10, y: 0, animated: false})
  }
  if (!moment(previewSelectedDate.current).isSame(props.curentDate)) {
    if (previewSelectedDate.current.getMonth() != props.curentDate.getMonth()) {
      const listDate = getListDateOfMonth(props.curentDate)
      setListDateOfMonth(listDate)
    }
    setSelectedDate(props.curentDate)
    previewSelectedDate.current = props.curentDate
    scrollToDate(props.curentDate, true)
  }

  const onSelectedDate = (item: Date) => {
    setSelectedDate(item)
    // props.onSelectedDate(item)
  }

  const onLayout = () => {
    scrollToDate(selectedDate)
  }
  const _renderItem: ListRenderItem<Date> = ({ item, index }) => <ItemDate item={item} selectedDate={selectedDate} onSelectedDate={onSelectedDate} />

  return (
    <View style={[{ flexDirection: 'column', paddingBottom: 10 }, props.style]} onLayout={onLayout}>
      <FlatList
        ref={flatList}
        getItemLayout={(_, index) => ({
          length: ITEM_WIDTH,
          offset: ITEM_WIDTH * index,
          index,
        })}
        data={listDateOfMonth}
        renderItem={_renderItem}
        keyExtractor={(item: Date, index) => `${item.getDate()}`}
        horizontal
        showsHorizontalScrollIndicator={false}
      />
    </View>
  )
}

type ItemProps = { item: Date, selectedDate?: Date, onSelectedDate?: (item: Date) => void }

const compareItem = (prevProps: ItemProps, nextProps: ItemProps) => {
  const isSameDate = moment(prevProps.item).isSame(nextProps.item);
  const isPreviewSelected = moment(prevProps.item).isSame(prevProps.selectedDate)
  const isNextSelected = moment(nextProps.item).isSame(nextProps.selectedDate)
  return isSameDate && isPreviewSelected == isNextSelected
}

const ItemDate = React.memo(({ item, selectedDate, onSelectedDate }: ItemProps) => {
  const bgColor = moment(item).isSame(selectedDate) ? COLOR.ACCENT : COLOR.TRANS
  const textColor = moment(item).isSame(selectedDate) ? COLOR.MAIN : moment(item).isSame(today) ? COLOR.GREEN : COLOR.WHITE
  return (
    <TouchableOpacity style={{
      width: ITEM_WIDTH, alignItems: 'center', justifyContent: 'center',
    }}
      onPress={() => onSelectedDate && onSelectedDate(item)}>
      <View style={{
        width: 54,
        paddingTop: 5,
        paddingBottom: 3,
        alignItems: 'center', justifyContent: 'center',
        borderRadius: 10,
        backgroundColor: bgColor
      }}>
        <Text style={{ color: textColor, fontSize: 13 }}>{trans.dayNames[item.getDay()]}</Text>
        <Text style={{ color: textColor, fontSize: 20, fontWeight: 'bold' }}>{item.getDate()}</Text>
      </View>


    </TouchableOpacity>
  )
}, compareItem)

export default CalendarCustom