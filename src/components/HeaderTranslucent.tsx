import {COLOR} from 'configs';
import {NavigationService} from 'helpers';
import React, {Component} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import SafeAreaView from 'react-native-safe-area-view';
import Icon5 from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/Ionicons';

type Props = {
  hasTabs?: any;
  title: string;
  canBack?: boolean;
  onPressRight?: () => void;
  iconRight?: any;
  onLongPressRight?: () => void;
  showPopupRight?: boolean;
  textRight?: string;
  onShowPopup?: () => void;
  backgroundImage?: string;
  titleFontSize?: number;
};

export default class HeaderTranslucent extends Component<Props> {
  onPressBack = () => NavigationService.back();
  render() {
    return (
      <SafeAreaView
        style={{backgroundColor: COLOR.MAIN}}
        forceInset={{top: 'always'}}>
        {
          // this.props.hasTabs &&
          <LinearGradient
            colors={['#FFFFFF4D', '#FFFFFF03']}
            style={{
              top: this.props.canBack ? -320 : -400,
              left: this.props.canBack ? 100 : -100,
              position: 'absolute',
              height: this.props.canBack ? 400 : 500,
              width: this.props.canBack ? 400 : 500,
              borderRadius: 10000,
            }}
          />
        }
 <Image
              style={{
                position: 'absolute',
                width: 100,
                height: 200,
                marginLeft: 60,
                top: 20,
                alignSelf: 'center',
              }}
              resizeMethod="resize"
              source={require('assets/bg_worldcup.png')}
              resizeMode="contain"
            />
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingVertical: 10,
          }}>
          {/* {this.props.backgroundImage && ( */}
           
          {/* )} */}
          {this.props.canBack ? (
            <View
              style={{
                width: 50,
                marginLeft: 6,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity style={{padding: 7}} onPress={this.onPressBack}>
                {/* <Icon name='arrow-back' style={{ color: COLOR.WHITE, fontSize: 30 }} /> */}
                <Icon5
                  name="chevron-left"
                  style={{color: COLOR.WHITE, fontSize: 25}}
                />
              </TouchableOpacity>
            </View>
          ) : null}

          {this.props.canBack ? (
            <View
              style={{flexDirection: 'row', justifyContent: 'center', flex: 1}}>
              <Text
                style={{
                  color: COLOR.WHITE,
                  fontSize: this.props.titleFontSize
                    ? this.props.titleFontSize
                    : 20,
                  fontWeight: 'bold',
                }}>
                {this.props.title}
              </Text>
            </View>
          ) : (
            <View style={{flexDirection: 'row', flex: 1, marginLeft: 30}}>
              <Text
                style={{color: COLOR.WHITE, fontSize: 25, fontWeight: 'bold'}}>
                {this.props.title}
              </Text>
            </View>
          )}
          <View
            style={{
              minWidth: 50,
              height: 45,
              marginRight: 6,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {this.props.onPressRight && (
              <TouchableOpacity
                style={{padding: 5}}
                onPress={this.props.onPressRight}
                onLongPress={this.props.onLongPressRight}>
                {this.props.iconRight}
              </TouchableOpacity>
            )}
          </View>
          {this.props.showPopupRight && (
            <TouchableOpacity
              style={{padding: 10, flexDirection: 'row', alignItems: 'center'}}
              onPress={this.props.onShowPopup}>
              <Text
                style={{
                  color: COLOR.WHITE,
                  fontSize: 18,
                  fontWeight: 'bold',
                  marginRight: 10,
                }}>
                {this.props.textRight}
              </Text>
              <Icon
                name="caret-down-circle-outline"
                style={{color: COLOR.WHITE, fontSize: 20}}
              />
            </TouchableOpacity>
          )}
        </View>
      </SafeAreaView>
    );
  }
}
