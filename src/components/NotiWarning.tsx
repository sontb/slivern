import React, { Component } from 'react';
import {
  View,
  ActivityIndicator,
  Platform,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Text,
  SafeAreaView,
} from 'react-native';
import { STYLE, Screen, COLOR } from 'uikitt';
import { inject, observer } from 'mobx-react';
import { Requests } from 'helpers';

@inject('commonStore') @observer
export default class Warning extends Component {

  onCancel = async () => {
    this.props.commonStore.disableWaring();
  }

  render() {
    if (!this.props.commonStore.isShowWarning) {
      return null;
    } else {
      return (
        <View style={{
          position: 'absolute', backgroundColor: COLOR.CL_BG_WARNING, borderRadius: 8,
          top: Platform.OS === 'ios' ? 40 : 10, left: 5, right: 5, paddingVertical: 15, paddingLeft: 15,
          flexDirection: 'row',
          elevation: 1,
          shadowOffset: { width: 0, height: 0 },
          shadowColor: '#000',
          shadowOpacity: 0.08,
          shadowRadius: 5,
        }}>
          <Image style={{ width: 18, height: 18, marginRight: 10, marginTop: 3 }} source={require('@assets/checklist/ic_warning.png')} resizeMode='contain' />
          <Text style={{ fontSize: 16, color: COLOR.RED, flex: 1, textAlign: 'left' }}>{this.props.commonStore.warning}</Text>
          <TouchableOpacity style={{ padding: 7, marginRight: 10 }} onPress={this.onCancel}>
            <Image style={{ width: 10, height: 10, tintColor: COLOR.RED }} source={require("@assets/checklist/ic_close.png")} />
          </TouchableOpacity>
        </View>
      );
    }
  }
}

const styles = {
};
