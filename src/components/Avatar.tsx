import { COLOR } from 'configs'
import React from 'react'
import { Image, StyleProp, View, ViewStyle } from "react-native"
type Props = {
  photo?: string,
  size: number,
  style?: StyleProp<ViewStyle>
}
const Avatar = ({ photo, size, style }: Props) => {
  return (
    <View style={[{ width: size + (size / 16), height: size + (size / 16), borderRadius: (size + (size / 16)) / 2, justifyContent: 'center', alignItems: 'center', backgroundColor: COLOR.OVERLEY_DARK, marginBottom: 5 },
      style]}>
      <Image style={{ width: size, height: size, borderRadius: size, backgroundColor: COLOR.WHITE }} source={{ uri: photo }} resizeMethod='resize' />
    </View>
  )
}

export default Avatar