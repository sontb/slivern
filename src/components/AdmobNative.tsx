// import { COLOR } from 'configs';
// import React from 'react';
// import { Platform, StyleProp, View, ViewStyle } from 'react-native';
// import NativeAdView, {
//   AdvertiserView, CallToActionView,
//   HeadlineView, IconView,
//   NativeMediaView, TaglineView,
//   AdBadge,
// } from 'node_customs/react-native-admob-native-ads';


// type Props = {
//   adUnitID: string, showAds: boolean, style?: StyleProp<ViewStyle>,
//   onAdFailedToLoad?: () => void
// }

// class AdmobNative extends React.PureComponent<Props>{

//   // shouldComponentUpdate(nextProps: Props, nextState: any) {
//   //   let a = nextProps.nativeAd.headline !== this.props.nativeAd.headline;
//   //   return a;
//   // }
//   state = {
//     onAdLoad: false,
//     showAds: this.props.showAds,
//   }

//   nativeMediaVIew: any

//   static getDerivedStateFromProps(nextProps: Props, prevState: Props) {
//     if (nextProps.showAds !== prevState.showAds) {
//       return ({ showAds: nextProps.showAds })
//     }
//     return null
//   }

//   componentDidMount() {
//     this.nativeMediaVIew.loadAd()
//   }

//   componentWillUnmount() {
//     console.log("sdf")
//   }

//   onAdLoaded = () => {
//     this.setState({ onAdLoad: true })
//   }

//   render() {
//     if (this.props.showAds) {
//       return (
//         <View style={[{ backgroundColor: COLOR.WHITE, height: this.state.onAdLoad ? -1 : 0, overflow: 'hidden', marginHorizontal: 10, borderRadius: 10 }, this.state.onAdLoad ? this.props.style : {}]}>
//           <NativeAdView
//             ref={(ref: any) => this.nativeMediaVIew = ref}
//             adUnitID={this.props.adUnitID}
//             onAdLoaded={this.onAdLoaded}
//             // mediationOptions={{ nativeBanner: true }}
//             onAdFailedToLoad={(error: { error: { message: string } }) => {
//               console.log(error)
//               this.props.onAdFailedToLoad && this.props.onAdFailedToLoad()
//             }
//             }
//           >
//             <View
//               style={{
//                 flexDirection: 'column',
//                 backgroundColor: COLOR.WHITE,
//               }}>
//               <AdBadge
//                 style={{
//                   width: 15,
//                   height: 15,
//                   borderWidth: 1,
//                   borderRadius: 2,
//                   borderColor: "green",
//                   marginTop: 3,
//                   marginLeft: 10,
//                 }}
//                 textStyle={{
//                   fontSize: 9,
//                   color: "green",
//                 }}
//               />
//               <View
//                 style={{
//                   flexDirection: "row",
//                   alignItems: "center",
//                   marginTop: 20,
//                   marginHorizontal: 10,
//                 }}>
//                 <IconView
//                   style={{
//                     width: 40,
//                     height: 40,
//                   }} />
//                 <View
//                   style={{
//                     flex: 1,
//                     marginLeft: 10,
//                   }}>
//                   <HeadlineView
//                     style={{
//                       fontWeight: "bold",
//                       fontSize: 15,
//                     }} />
//                   <AdvertiserView
//                     style={{
//                       fontSize: 14,
//                       color: "gray",
//                     }} />
//                 </View>


//               </View>
//               <NativeMediaView style={{ width: '100%', height: 150, marginTop: 5 }} />
//               {/* <ImageView style={{width: '100%',height: 150}}/> */}

//               <TaglineView
//                 numberOfLines={2}
//                 style={{
//                   fontSize: 14,
//                   marginHorizontal: 15,
//                   marginTop: 5,
//                 }} />


//               <CallToActionView
//                 style={{
//                   height: 40,
//                   elevation: 10,
//                   alignSelf: 'stretch',
//                   marginHorizontal: 10,
//                   marginBottom: 10,
//                   marginTop: 5,
//                   backgroundColor: COLOR.GREEN,
//                   borderRadius: 5,
//                   justifyContent: 'center',
//                   alignItems: 'center',
//                 }}
//                 buttonAndroidStyle={{
//                   backgroundColor: COLOR.GREEN,
//                   borderRadius: 5,
//                 }}
//                 allCaps
//                 textStyle={{
//                   fontSize: 13,
//                   flexWrap: 'wrap',
//                   textAlign: 'center',
//                   color: 'white',
//                   marginBottom: Platform.OS === 'android' ? 5 : 0,
//                 }}
//               />
//             </View>
//           </NativeAdView>
//         </View >

//       );
//     }
//     else return null
//   }
// }

// export default AdmobNative;
