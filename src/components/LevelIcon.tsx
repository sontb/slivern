import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  StyleProp,
  ViewStyle,
  Image,
} from 'react-native';
// import SvgUri from 'react-native-svg-uri';
import { COLOR } from 'configs';

interface Props {
  containStyles?: StyleProp<ViewStyle>,
  score: number,
  size: number,
  notHide?: boolean,
}

export default class LevelIcon extends Component<Props> {

  render() {
    if (this.props.score < 20 || !this.props.score) {
      if (this.props.notHide) {
        return <View style={[{ width: this.props.size, height: this.props.size }, this.props.containStyles]} />;
      } else {
        return null;
      }
    } else {
      return (
        <View style={this.props.containStyles}>
          {/* {
          (this.props.score <20 && this.props.notHide) &&
          // <Image style={{ width: 24, height: 24, marginLeft: 10 }} source={require(im_amateur)} />
          <View style={{width: this.props.size, height: this.props.size}} />
        } */}
          {
            (this.props.score >= 20 && this.props.score < 160) &&
            <Image style={{ width: this.props.size, height: this.props.size }} source={require('assets/ic_amateur.png')} resizeMode='contain' />
          }
          {
            (this.props.score >= 160 && this.props.score < 440) &&
            <Image style={{ width: this.props.size, height: this.props.size }}  source={require('assets/ic_semipro.png')} resizeMode='contain'/>
          }
          {
            (this.props.score >= 440 && this.props.score < 1000) &&
            <Image style={{ width: this.props.size, height: this.props.size }} source={require('assets/ic_profession.png')} resizeMode='contain'/>
          }
          {
            this.props.score >= 1000 &&
            <Image style={{ width: this.props.size, height: this.props.size }} source={require('assets/ic_expert.png')} resizeMode='contain'/>
          }

        </View>
      );
    }

  }
}

const styles = StyleSheet.create({

});
