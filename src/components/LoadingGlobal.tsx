import { COLOR } from 'configs';
import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Alert,
  Platform,


  StyleSheet, TouchableWithoutFeedback, View
} from 'react-native';
import { ICommonStore } from './../stores/CommonStore';

interface AppProps {
  commonStore?: ICommonStore
}

@inject('commonStore')
@observer
export default class Loading extends Component<AppProps> {

  render() {
    if (!this.props.commonStore!.loading) return null;
    return (
      <TouchableWithoutFeedback >

        <View style={styles.loadingPage} >
          <View style={styles.loadingPage_inner}>
            <ActivityIndicator
              color={COLOR.PRIMARY_COLOR}
              animating
              // style={styles.centering}
              size={Platform.OS === 'ios' ? 'small' : 'small'}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>

    );
  }
}

const styles = StyleSheet.create({
  loadingPage: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'rgba(0,0,0,0.01)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingPage_inner: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 0,
    borderColor: 'rgba(0,0,0,0.03)',
    borderWidth: 1,
    shadowColor: 'black',
    shadowRadius: 12,
    shadowOpacity: 0.07,
    shadowOffset: { height: 0, width: 0 },
    borderRadius: 10,
  },
  centering: {
    height: 80,
  },
  cancelText: {
    color: COLOR.RED,
    fontSize: 15,
    // fontWeight: 'bold',
  },
  cancelIcon: {
    width: 30,
    height: 30,
  },
  cancelContainer: {
    padding: 10,
  },
});
