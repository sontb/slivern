import { COLOR } from 'configs';
import React, { Component } from 'react';
import {
  ActivityIndicator,
  Platform,


  StyleSheet, View
} from 'react-native';

type Props = {
  loading: boolean,
}

export default class Loading extends Component<Props> {

  render() {
    return (
      this.props.loading ?
      <View style={styles.loadingPage} pointerEvents='none'>
      <View style={styles.loadingPage_inner}>
        <ActivityIndicator
          color={COLOR.PRIMARY_COLOR}
          animating
          // style={styles.centering}
          size={Platform.OS === 'ios' ? 'small' : 'small'}
        />
      </View>
    </View>
        :
        null
    );
  }
}

const styles = StyleSheet.create({
  loadingPage: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    right: 0,
    // backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  loadingPage_inner: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: 0,
    borderColor: 'rgba(0,0,0,0.03)',
    borderWidth: 1,
    shadowColor: 'black',
    shadowRadius: 12,
    shadowOpacity: 0.07,
    shadowOffset: { height: 1, width: 0 },
    borderRadius: 10,
  },
  centering: {
    height: 80,
  },
  cancelText: {
    color: COLOR.RED,
    fontSize: 15,
    // fontWeight: 'bold',
  },
  cancelIcon: {
    width: 30,
    height: 30,
  },
  cancelContainer: {
    padding: 10,
  },
});
