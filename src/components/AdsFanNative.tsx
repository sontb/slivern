// import React, { Component } from 'react';
// import { View, Text, Dimensions, StyleProp, ViewStyle, Platform } from 'react-native';

// import { COLOR, AdFanId } from 'configs';
// import { BannerView, NativeAdViewTemp } from 'node_customs/react-native-fbads';

// type Props = { placementId: string, showAds: boolean, style?: StyleProp<ViewStyle> }
// class AdsFanNative extends React.Component<Props> {

//   state = {
//     onAdLoad: false,
//     showAds: this.props.showAds,
//   }

//   constructor(props: any) {
//     super(props)
//   }

//   nativeAdView: any
//   focusListener: any;
//   blurListener: any;

//   static getDerivedStateFromProps(nextProps: Props, prevState: Props) {
//     if (nextProps.showAds !== prevState.showAds) {
//       return ({ showAds: nextProps.showAds })
//     }
//     return null
//   }

//   refreshAdview() {
//     if(this.nativeAdView){
//       this.nativeAdView.refreshAdView()
//     }
//   }

//   render() {
//     let { placementId } = this.props;
//     if (this.state.showAds) {
//       return (
//         <View style={[{ backgroundColor: COLOR.WHITE, height: this.state.onAdLoad ? -1 : 0, overflow: 'hidden', marginHorizontal: 10, borderRadius: 10 }, this.state.onAdLoad ? this.props.style : {}]}>
//           <NativeAdViewTemp
//             ref={(ref: any) => {
//               this.nativeAdView = ref
//             }}
//             placementId={placementId}
//             onAdPress={() => console.log('adclick')}
//             onAdError={(error: any) => {
//               console.log(error.nativeEvent)
//               this.setState({ onAdLoad: false })
//             }}
//             onAdLoad={() => {
//               this.setState({ onAdLoad: true })
//               console.log('onAdLoad')
//             }}
//           />
//           {/* <BannerView placementId={AdFanId.banner_kqltd} type='standard'
//             onAdPress={() => console.log('adclick')}
//             onAdError={(error: any) => this.setState({ onAdLoad: false })}
//             onAdLoad={() => this.setState({ onAdLoad: true })} /> */}
//         </View>
//       );
//     }

//     else return null
//   }
// }

// export default AdsFanNative