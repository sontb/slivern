import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  StyleProp,
  ViewStyle,
  Text,
  Image,
  Platform,
} from 'react-native';
import { COLOR } from 'configs';
// import SvgUri from 'react-native-svg-uri';

type Props = {
  containStyles?: StyleProp<ViewStyle>,
  position: number,
  size: number,
  notHide?: boolean,
}

export default class TopIcon extends Component<Props> {

  render() {
    if (this.props.position < 1 || this.props.position > 10 || !this.props.position) {
      if (this.props.notHide) {
        return <View style={[{ width: this.props.size, height: this.props.size }, this.props.containStyles]} />;
      } else {
        return null;
      }
    } else {
      return (
        <View style={this.props.containStyles}>
          {/* {
          ((this.props.position < 1 || this.props.position > 10) && this.props.notHide) &&
          <View style={{ width: this.props.size, height: this.props.size }} />
        } */}
          {
            (this.props.position === 1) &&
            <Image style={{ width: this.props.size, height: this.props.size }} source={require('assets/ic_top1.png')} resizeMode='contain' />
          }
          {
            (this.props.position === 2) &&
            <Image style={{ width: this.props.size, height: this.props.size }} source={require('assets/ic_top2.png')} resizeMode='contain' />
          }
          {
            (this.props.position === 3) &&
            <Image style={{ width: this.props.size, height: this.props.size }} source={require('assets/ic_top3.png')} resizeMode='contain' />
          }
          {
            (this.props.position > 3 && this.props.position <= 10) &&
            <View>
              <Image style={{ width: this.props.size, height: this.props.size }} source={require('assets/ic_top.png')} resizeMode='contain' />
              <Text style={{ paddingLeft: 1, marginTop: Platform.OS === 'ios' ? this.props.size / 10 : 0, position: 'absolute', alignSelf: 'center', fontSize: this.props.size / 2, color: COLOR.WHITE, fontWeight: 'bold' }}>{this.props.position}</Text>
            </View>
          }
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({

});
