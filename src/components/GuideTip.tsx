import { COLOR } from 'configs'
import React from 'react'
import { StyleProp, StyleSheet, ViewProps, ViewStyle } from 'react-native';
import { Text, View, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';

const GuideTip = ({ text, onPressClose, style }: { text: string, onPressClose: () => void, style?: StyleProp<ViewStyle> }) => {
  return (
    <View style={[{
      backgroundColor: COLOR.YELLOW, borderRadius: 10, margin: 10, padding: 10,
      flexDirection: 'row'
    }, style]}>
      <Text style={{ fontSize: 14, flex: 1, marginRight: 20, color: COLOR.MAIN_TEXT }}>{text}</Text>
      <TouchableOpacity style={{ position: 'absolute', right: 0, top: 0, padding: 10 }} onPress={onPressClose}>
        <Icon name='times-circle' size={20} color={COLOR.BLACK} />
      </TouchableOpacity>
    </View>
  )
}

export default GuideTip