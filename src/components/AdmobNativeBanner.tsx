// import { COLOR } from 'configs';
// import React from 'react';
// import { Platform, StyleProp, Text, View, ViewStyle } from 'react-native';
// import NativeAdView, {
//   CallToActionView,
//   HeadlineView, IconView,
//   NativeMediaView, TaglineView,
//   AdBadge,
// } from 'node_customs/react-native-admob-native-ads';
// import { trans } from 'trans';


// type Props = { adUnitID: string, showAds: boolean, style?: StyleProp<ViewStyle> }

// class AdmobNativeBanner extends React.Component<Props>{

//   state = {
//     onAdLoad: false,
//     showAds: this.props.showAds,
//   }

//   constructor(props: any) {
//     super(props)
//   }
//   nativeMediaVIew: any

//   timeout: any

//   componentDidMount = async () => {
//     this.nativeMediaVIew.loadAd()

//   }

//   // static getDerivedStateFromProps(nextProps: Props, prevState: Props) {
//   //   if (nextProps.showAds !== prevState.showAds) {
//   //     return ({ showAds: nextProps.showAds })
//   //   }
//   //   return null
//   // }

//   componentWillUnmount() {
//     if (this.timeout) {
//       clearTimeout(this.timeout)
//     }
//   }

//   handleOnload = () => {
//     this.setState({ onAdLoad: true })
//     this.timeout = setTimeout(() => {
//       this.setState({ showAds: false });
//     }, 14000);
//   }

//   render() {
//     if (this.state.showAds) {
//       return (

//         <View style={[{
//           height: this.state.onAdLoad ? -1 : 0,
//           overflow: 'hidden'
//         }, this.props.style]}>
//           <Text style={{ color: COLOR.WHITE, marginLeft: 10, marginBottom: 5 }}>{trans.adWillCose}</Text>

//           <NativeAdView
//             ref={(ref: any) => this.nativeMediaVIew = ref}

//             adUnitID={this.props.adUnitID}
//             onAdLoaded={this.handleOnload}

//             mediationOptions={{ nativeBanner: Platform.OS === 'android' }}
//             onAdFailedToLoad={(error: { error: { message: string } }) => {
//               this.setState({ onAdLoad: false })
//             }
//             }>

//             <View
//               style={{
//                 flexDirection: "row",
//                 alignItems: "center",
//                 paddingVertical: 15,
//                 backgroundColor: COLOR.WHITE,
//               }}>
//               <AdBadge
//                 style={{
//                   width: 15,
//                   height: 15,
//                   borderWidth: 1,
//                   borderRadius: 2,
//                   borderColor: "green",
//                   marginTop: 1,
//                   marginLeft: 0,
//                 }}
//                 textStyle={{
//                   fontSize: 9,
//                   color: "green",
//                 }}
//               />
//               {
//                 Platform.OS === 'ios' &&
//                 <NativeMediaView style={{ position: 'absolute', width: 40, height: 40 }} />

//               }

//               <IconView
//                 style={{
//                   width: 40,
//                   height: 40,
//                 }} />

//               <View
//                 style={{
//                   flex: 1,
//                   marginLeft: 10,
//                 }}>

//                 <HeadlineView
//                   style={{
//                     fontWeight: "bold",
//                     fontSize: 15,
//                   }} />

//                 <TaglineView
//                   numberOfLines={1}
//                   style={{
//                     fontSize: 14,
//                     color: "#1a1a1a",
//                   }} />

//               </View>

//               <CallToActionView
//                 style={{
//                   width: 90,
//                   height: 40,
//                   backgroundColor: COLOR.GREEN,
//                   borderRadius: 5,
//                   justifyContent: 'center',
//                   alignItems: 'center',
//                 }}
//                 buttonAndroidStyle={{
//                   backgroundColor: COLOR.GREEN,
//                   borderRadius: 5,
//                 }}
//                 textStyle={{ color: "white", fontSize: 12, fontWeight: 'bold' }} />

//             </View>


//           </NativeAdView>
//         </View>

//       );
//     }
//     else return null
//   }
// }

// export default AdmobNativeBanner;
