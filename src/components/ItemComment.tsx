import { Avatar, LevelIcon, TopIcon } from 'components'
import { COLOR } from 'configs'
import { GlobalFuntion } from 'helpers'
import React, { useState } from 'react'
import { Alert, StyleProp, Text, TouchableOpacity, View, ViewStyle } from 'react-native'
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';

export interface IItemComment {
  id: number;
  email: string;
  content: string;
  mention?: string;
  date_create: number;
  name: string;
  point: number
  photo: string
  position: number
}

type Props = {
  itemComment: IItemComment,
  onPressComment?: () => void,
  style?: StyleProp<ViewStyle>
  onPressName?: () => void,
  onPressAvatar?: () => void,
}

const ItemComment = ({ itemComment, onPressComment, style, onPressName, onPressAvatar }: Props) => {
  const [report, setReport] = useState(false)
  const [hidden, setHidden] = useState(false)
  const onReport = () => {
    Alert.alert(
      'Báo vi phạm',
      `Bạn muốn ẩn hoặc gắn cờ nội dung này?`,
      [
        {
          text: 'Hủy',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Ẩn nội dung này', onPress: () => setHidden(true),
        },
        {
          text: 'Gắn cờ', onPress: () => setReport(true),
          style: 'destructive'
        },
      ],
      { cancelable: false },
    );
  }
  if (hidden) {
    return null
  } else {

    return (
      <View onStartShouldSetResponder={() => true}>

        <TouchableOpacity style={[{ flexDirection: 'row' }, style]} onPress={onPressComment} disabled={onPressComment ? false : true} >
          <TouchableOpacity style={{ padding: 5, alignSelf: 'baseline' }}
            onPress={onPressAvatar} disabled={onPressAvatar ? false : true}>
            <Avatar size={25} photo={itemComment.photo} />

          </TouchableOpacity>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={{
              padding: 10,
              borderRadius: 20, backgroundColor: COLOR.OVERLEY_LIGHT
            }}>
              <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}
                onPress={onPressName} disabled={onPressName ? false : true}>
                <LevelIcon score={itemComment.point} size={20} containStyles={{ marginRight: 7 }} />

                <Text style={{
                  fontSize: 15, fontWeight: 'bold',
                  marginRight: 5,
                  color: GlobalFuntion.getLevelColor(itemComment.point),
                }}>{`${itemComment.name} ${GlobalFuntion.getLevelName(itemComment.point)}`}</Text>
                <TopIcon position={itemComment.position} size={18} />
                <TouchableOpacity style={{ padding: 3 }} onPress={onReport}>
                  {
                    report ?
                      <Icon name='flag' size={17} color={COLOR.BLUE} />
                      :
                      <Icon name='flag' size={17} color={COLOR.GREY} />
                  }
                </TouchableOpacity>
              </TouchableOpacity>
              <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                <Text style={{ color: COLOR.MAIN_TEXT }}><Text
                  style={{
                    fontWeight: 'bold',
                  }}
                  ellipsizeMode='tail'>{itemComment.mention ? `${itemComment.mention} ` : ''}</Text>{itemComment.content}</Text>

              </View>
              <Text style={{ color: COLOR.SUB_TEXT, marginTop: 5, fontSize: 12 }}>{moment.unix(itemComment.date_create).format('DD/MM HH:mm')}</Text>
            </View>
          </View>

        </TouchableOpacity>
      </View>

    )
  }

}

export default ItemComment