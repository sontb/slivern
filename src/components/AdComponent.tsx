// import React, { Component } from 'react';
// import { View, Text, Dimensions } from 'react-native';

// import * as FacebookAds from 'node_customs/react-native-fbads';
// import { COLOR } from 'configs';
// const { AdIconView, MediaView, TriggerableView, AdChoicesView } = FacebookAds;

// const screenWidth = Math.round(Dimensions.get('window').width) - 20;
// type Props = {nativeAd: any}
// class AdComponent extends React.PureComponent<Props> {


//   render() {
//     let { nativeAd } = this.props;
//     return (
//       <View style={{ backgroundColor: COLOR.WHITE, marginTop: 10, padding: 10, marginBottom: 10 }}>
//         <View style={{ flexDirection: 'row' }}>
//           <AdIconView style={{ width: 50, height: 50 }} />

//           <View style={{ flex: 1, marginLeft: 10, alignSelf: 'stretch', justifyContent: 'center' }}>
//             <TriggerableView>
//               <Text style={{ color: COLOR.MAIN_TEXT, fontSize: 15 }} numberOfLines={2}>{nativeAd.headline}</Text>
//             </TriggerableView>
//             <Text style={{ color: COLOR.SUB_TEXT, fontSize: 13, marginTop: 3 }} numberOfLines={1}>{nativeAd.sponsoredTranslation}</Text>
//           </View>
//           <AdChoicesView style={{ backgroundColor: COLOR.WHITE }}/>

//         </View>

//         <MediaView style={{ width: '100%', height: 200, marginTop: 10 }} />
//         <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
//           <View style={{ flex: 1, marginRight: 5 }}>
//             <Text style={{ color: COLOR.MAIN_TEXT, fontSize: 15 }} numberOfLines={2}>{nativeAd.bodyText}</Text>
//             <Text style={{ color: COLOR.SUB_TEXT, fontSize: 13, marginTop: 5 }} numberOfLines={1}>{nativeAd.socialContext}</Text>
//           </View>
//           <View style={{ backgroundColor: COLOR.GREEN, borderRadius: 5 }}>

//             <TriggerableView >
//               <View style={{ padding: 10 }}>
//                 <Text style={{ color: COLOR.WHITE, fontSize: 15 }}>{nativeAd.callToActionText}</Text>
//               </View>
//             </TriggerableView>
//           </View>

//         </View>
//       </View>
//     );
//   }
// }

// export default AdComponent;