import React, { Component } from "react"
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { inject, observer } from 'mobx-react';
import { ICommonStore } from "stores/CommonStore";
import SCREENS, { SCREENS_NAME } from './screens';
import { Image } from "react-native";
import { COLOR, EVENT } from "configs";
import { trans } from "trans";

const Tab = createBottomTabNavigator();
type Props = { commonStore?: ICommonStore };

@inject('commonStore')
@observer
export default class BottomTab extends Component<Props> {
  render() {

    return (
      <Tab.Navigator screenOptions={({ route }) => ({
        // tabBarVisible: this.props.commonStore?.tabBarVisible,
        tabBarIcon: ({ focused, color, size }) => {
          if (route.name === SCREENS_NAME.Match) {
            return <Image style={{ width: 24, height: 24, tintColor: color }} source={require('assets/ic_drawer_match.png')} resizeMode='contain' />
          } else if (route.name === SCREENS_NAME.Live) {
            return <Image style={{ width: 24, height: 24, tintColor: focused ? COLOR.RED_DARK : color }} source={require('assets/ic_drawer_live.png')} resizeMode='contain' />
          } else if (route.name === SCREENS_NAME.Tips) {
            return <Image style={{ width: 24, height: 24, tintColor: focused ? COLOR.GREEN : color }} source={require('assets/ic_drawer_tips.png')} resizeMode='contain' />
          } else if (route.name === SCREENS_NAME.League) {
            return <Image style={{ width: 24, height: 24, tintColor: focused ? COLOR.PROFESSION : color }} source={require('assets/ic_drawer_league.png')} resizeMode='contain' />
          } else if (route.name === SCREENS_NAME.User) {
            return <Image style={{ width: 24, height: 24, tintColor: color }} source={require('assets/ic_tab_user.png')} resizeMode='contain' />
          }
        },
      })}
        tabBarOptions={{
          activeTintColor: COLOR.MAIN,
          inactiveTintColor: COLOR.SUB_COLOR,

        }}
        backBehavior='initialRoute'
        detachInactiveScreens={true}

      >
        <Tab.Screen name={SCREENS_NAME.Match} component={SCREENS.Match} options={{ title: trans.tabKQLTD }} />
        {
          EVENT.APPSTORE == 1 &&
          <Tab.Screen name={SCREENS_NAME.Live} component={SCREENS.Live} options={{ title: trans.tabLive }} />
        }
        <Tab.Screen name={SCREENS_NAME.Tips} component={SCREENS.Tips} options={{ title: trans.tabNhanDinh }} />
        <Tab.Screen name={SCREENS_NAME.League} component={SCREENS.League} options={{ title: trans.league }} />
        <Tab.Screen name={SCREENS_NAME.User} component={SCREENS.User} options={{ title: trans.tabNguoiDung }} />

      </Tab.Navigator>
    );
  }

}