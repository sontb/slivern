import { COLOR, EVENT } from "configs";
import { Alert, Platform } from "react-native";
import { ILoginStore } from "stores/LoginStore";
import { trans } from "trans";

const getLevelName = (score: number): string => {
  if (score >= 160 && score < 440) {
    return '- Bán chuyên';
  }
  if (score >= 440 && score < 1000) {
    return '- Chuyên nghiệp';
  }
  if (score >= 1000) {
    return '- Chuyên gia';
  }
  return '';
}

const getLevelColor = (score: number) => {

  if (score >= 20 && score < 160) {
    return COLOR.AMATEUR;
  }
  if (score >= 160 && score < 440) {
    return COLOR.SEMIPRO;
  }
  if (score >= 440 && score < 1000) {
    return COLOR.PROFESSION;
  }
  if (score >= 1000) {
    return COLOR.EXPERT;
  }
  return COLOR.BLACK;
}

const requestLogin = async (loginStore: ILoginStore, onSuccess?: (isLogin?: boolean) => void) => {
  if (loginStore.user) {
    onSuccess && onSuccess()
  } else {
    if (EVENT.APPSTORE == 1) {
      await loginStore.signIn();
      if (loginStore.user) {
        onSuccess && onSuccess()
      }
    } else {
      Alert.alert(
        trans.dieuKhoanDN,
        `${trans.truocKhiDN}
       ${trans.khongDuocSuDung}
       ${trans.khongDuocLamDung}
       ${trans.neuViPham}`,
        [
          {
            text: trans.huy,
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: trans.toiDaDoc, onPress: async () => {
              if (Platform.OS === 'ios') {
                Alert.alert(trans.login, trans.chooseLoginMethod, [
                  {
                    text: trans.huy,
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {
                    text: 'Google',
                    onPress: async () => {
                      await loginStore.signIn();
                      if (loginStore.user) {
                        onSuccess && onSuccess(true)
                      }
                    },
                  },
                  {
                    text: 'Apple',
                    onPress: async () => {
                      await loginStore.signIn(true);
                      if (loginStore.user) {
                        onSuccess && onSuccess(true)
                      }
                    }
                  }
                ])
              } else {
                await loginStore.signIn();
                if (loginStore.user) {
                  onSuccess && onSuccess()
                }
              }


            }
          },
        ],
        { cancelable: false },
      );
    }
  }
}

export default {
  getLevelName,
  getLevelColor,
  requestLogin
}