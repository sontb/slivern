// import { StackActions, NavigationActions } from 'react-navigation';
import { CommonActions, TabActions, StackActions, NavigationAction } from '@react-navigation/native';
// import { SCREENS_NAME } from 'screens/ScreenName';
let _app_navigator: any;
let _tab_navigator: any;

function setAppNavigator(navigatorRef: any) {
  _app_navigator = navigatorRef;
}

function getNavigation(){
  return _app_navigator
}

function navigate(routeName: any, params: any) {
  _app_navigator.dispatch(
    CommonActions.navigate({
      name: routeName,
      params,
    })
  );
}

function back() {
  _app_navigator.dispatch(
    CommonActions.goBack()
  );
}

function gotoMain(initRounteName: string, params?: any) {
  _app_navigator.dispatch(
    StackActions.replace("MainTab", {
      screen: initRounteName,
      params
    })
  );
}

function reset() {
  _app_navigator.dispatch(
    CommonActions.reset({
      index: 0,
      routes: [
        {name: "Drawer"},
      ]
    })
  );
}

// add other navigation functions that you need and export them

export default {
  navigate,
  back,
  gotoMain,
  setAppNavigator,
  getNavigation,
  reset,
};
