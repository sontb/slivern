import AsyncStorage from '@react-native-community/async-storage';

export const KeyStorage = {
  USER: 'USER',
  ADMIN_DATA: 'ADMIN_DATA',
  GUIDE_CLOSED_TIPS_MATCH: 'GUIDE_CLOSED_TIPS_MATCH',
  GUIDE_CLOSED_TIPS_TOP_PREDICT: 'GUIDE_CLOSED_TIPS_TOP_PREDICT',
  GUIDE_CLOSED_TIPS_COMMENT: 'GUIDE_CLOSED_TIPS_COMMENT',

  LAST_SELECTED_NEW_CATEGORY_POSITION: 'LAST_SELECTED_NEW_CATEGORY_POSITION',
  LAST_TIME: 'LAST_TIME',
  SCHEDULE_MATCH: 'SCHEDULE_MATCH',
}

export const TypeStorage = {
  string: 'string',
  number: 'number',
  object: 'object',
  boolean: 'boolean',
}

class StorageHelper {

  setItem = async (key: string, value: any) => {
    if (value === undefined || value === null) return false;
    try {
      const test = JSON.stringify(value);
      const valueType = typeof value;
      const stringValue = valueType !== TypeStorage.object ? String(value) : JSON.stringify(value);
      await AsyncStorage.setItem(key, stringValue);
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }

  }

  removeItem = async (key: string) => {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  }

  getItem = async (key: string, type: string) => {
    let value = await AsyncStorage.getItem(key);
    if ((value === null || value === undefined) && type == TypeStorage.number) return 0;
    if (value === null || value === undefined) return null;
    if (type === TypeStorage.string) return value;
    if (type === TypeStorage.number) return Number(value);
    if (type === TypeStorage.object) {
      try {
        let parsed = JSON.parse(value);
        return parsed;
      } catch (err) {
        return null;
      }
    }
    if (type === TypeStorage.boolean) return value === 'true';
  }
}
export default new StorageHelper();
