import { GoogleSignin, statusCodes } from 'react-native-google-signin';
// import firebase from 'react-native-firebase';
import auth from '@react-native-firebase/auth';

import { Platform } from 'react-native';
// Calling this function will open Google for login.
class GoogleHelper {

  constructor() {
    // GoogleSignin.hasPlayServices({ autoResolve: true });
    GoogleSignin.hasPlayServices({ autoResolve: true, showPlayServicesUpdateDialog: true })
    GoogleSignin.configure({
      iosClientId: '754554945190-dj0cdiv5a68vs1g3d48c5tq3f7pm6cab.apps.googleusercontent.com',
      webClientId: Platform.OS === 'ios' ? '754554945190-dj0cdiv5a68vs1g3d48c5tq3f7pm6cab.apps.googleusercontent.com' : '754554945190-u9sbr8eto3jah31uugd7l0u1e3d41per.apps.googleusercontent.com'
    });
  }

  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      let userInfo = await GoogleSignin.signIn();
      const credential = auth.GoogleAuthProvider.credential(userInfo.idToken, userInfo.accessToken)
      const firebaseUserCredential = await auth().signInWithCredential(credential);
      let user = {
        displayName: userInfo.user.name,
        email: firebaseUserCredential.user.email,
        photo: userInfo.user.photo,
        uid: firebaseUserCredential.user.uid,
        type: 0,
      };

      return user;
    } catch (error) {
      console.log(error);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
      return null;
    }
  }

  // getCurrentUserInfo = async () => {
  //   try {
  //     let currentUser = firebase.auth().currentUser;
  //     console.log("isSignInWithEmailLink", isSignInWithEmailLink);
  //     let userInfo = await GoogleSignin.signInSilently();
  //     const credential = firebase.auth.GoogleAuthProvider.credential(userInfo.idToken, userInfo.accessToken);
  //     console.log(userInfo);
  //     const firebaseUserCredential = await firebase.auth().signInWithCredential(credential);
  //     console.log(firebaseUserCredential);
  //     return userInfo.user;
  //   } catch (error) {
  //     if (error.code === statusCodes.SIGN_IN_REQUIRED) {
  //       // user has not signed in yet
  //     } else {
  //       // some other error
  //     }
  //   }
  // };

  // isSignedIn = async () => {
  //   const isSignedIn = await GoogleSignin.isSignedIn();
  //   return isSignedIn;
  // };

  // getCurrentUser = async () => {
  //   const currentUser = await GoogleSignin.getCurrentUser();
  //   return currentUser;
  // };

  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      await auth().signOut();
      return true;
    } catch (error) {
      console.error(error);
    }
    return false;
  }
}



export default new GoogleHelper();