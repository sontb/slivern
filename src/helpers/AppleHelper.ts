import { appleAuth } from '@invertase/react-native-apple-authentication';
// import firebase from 'react-native-firebase';
import auth from '@react-native-firebase/auth';

import { Platform } from 'react-native';
// Calling this function will open Google for login.
class AppleHelper {

  constructor() {
    
  }

  signIn = async () => {
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });
      const { identityToken, nonce } = appleAuthRequestResponse;
      const appleCredential = auth.AppleAuthProvider.credential(identityToken, nonce);
      console.log(appleCredential)
      const firebaseUserCredential = await auth().signInWithCredential(appleCredential);
      let user = {
        displayName: firebaseUserCredential.user.displayName,
        email: firebaseUserCredential.user.email,
        photo: firebaseUserCredential.user.photoURL,
        uid: firebaseUserCredential.user.uid,
        type: 1,
      };

      return user;
    } catch (error) {
      console.log(error);
      return null;
    }
  }

  

  signOut = async () => {
    try {
      // await GoogleSignin.revokeAccess();
      // await GoogleSignin.signOut();
      await firebase.auth().signOut();
      return true;
    } catch (error) {
      console.error(error);
    }
    return false;
  }
}



export default new AppleHelper();