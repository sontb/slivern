// // import firebase from 'react-native-firebase';
// // import { AdEventType, InterstitialAd } from '@react-native-firebase/admob';

// import { EVENT } from 'configs';
// // import { AdManager } from 'node_customs/react-native-admob-native-ads';
// import { Alert, Platform } from "react-native";
// import permissions from 'react-native-permissions';
// import { trans } from "trans";


// // const AdRequest = admob.AdRequest;

// class AdmobHelper {
//   initialize() {
//     // firebase.admob().initialize(AdmobUnitId.app)
//   }

//   advert: any;
//   unitId = '';
//   isShowingAlertPermission = false;
//   interstitial(unitId: string, onAdsLoad?: (isOk: boolean) => void, onAdsClosed?: () => void) {

//     try {
//       // const interstitial = InterstitialAd.createForAdRequest(unitId);

//       // interstitial.load();
//       // interstitial.onAdEvent((type) => {
//       //   if (type === AdEventType.LOADED) {
//       //     interstitial.show();
//       //   }
//       // });
      
//       let timeout = setTimeout(() => {
//         if (onAdsLoad) {
//           onAdsLoad(false)
//           onAdsLoad = undefined
//           onAdsClosed = undefined
//         }
//       }, 10000)
      
//       AdManager.interstitialLoad(unitId).then(() => {
//         clearTimeout(timeout)
//         if (onAdsLoad) {
//           onAdsLoad(true)
//           onAdsLoad = undefined
//         }
//         if (onAdsClosed) {
//           let listenerAdsClosed = AdManager.onInterstitialClosed(() => {
//             if(onAdsClosed){
//               onAdsClosed()
//               onAdsClosed = undefined
//               if(listenerAdsClosed){
//                 listenerAdsClosed.remove()
//               }
//             }
//           })
//         }
//       }).catch(() => {
//         clearTimeout(timeout)
//         if (onAdsLoad) {
//           onAdsLoad(false)
//           onAdsLoad = undefined
//         }
//       })
      

//     } catch (e) {
//       console.log(e)
//     }

//   }


//   requestPermissionTracking = async () => {
//     console.log(this.isShowingAlertPermission)
//     if (Platform.OS === 'ios') {
//       let permission = await permissions.request(permissions.PERMISSIONS.IOS.APP_TRACKING_TRANSPARENCY)
//       if (permission != 'granted' && !this.isShowingAlertPermission && EVENT.APPSTORE == 1) {
//         this.isShowingAlertPermission = true
//         Alert.alert(trans.titleAlowTracking, trans.descAlowTracking, [
//           {
//             text: trans.actionAlowTracing,
//             onPress: () => {
//               permissions.openSettings()
//               this.isShowingAlertPermission = false
//             },
//           },
//           {
//             text: trans.huy,
//             onPress: () => console.log('Cancel Pressed'),
//             style: 'cancel',
//           }
//         ])
//       }
//     }
//   }

//   setFanAdvertiserTrackingEnabled = async (isTrackingEnabled: boolean) => {
//     AdManager.setAdvertiserTrackingEnabled(isTrackingEnabled)
//   }

//   disableSSLCertificateVerify = ()=>{
//     AdManager.disableSSLCertificateVerify().then(()=>{
//       console.log('')
//     }).catch(()=>{
//       console.log('')
//     })
//   }
// }
// export default new AdmobHelper();