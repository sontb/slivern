import codePush from "react-native-code-push";
import { Platform } from "react-native";

const checkCodePushUpdate = () => {
  let updateDialogOptions = {
    title: "Có bản cập nhật mới",
    optionalUpdateMessage: "Cập nhật ngay bây giờ?",
    optionalIgnoreButtonLabel: "Hủy",
    optionalInstallButtonLabel: "Cập Nhật",
  };
  codePush.sync({
    // updateDialog: updateDialogOptions,
    installMode: codePush.InstallMode.IMMEDIATE
  });
}

const checkCodePushFromNotify = () => {
  codePush.restartApp();
}



export default {
  checkCodePushUpdate,
  checkCodePushFromNotify,
}