// import firebase from 'react-native-firebase'
import database, { FirebaseDatabaseTypes } from '@react-native-firebase/database';
import messaging, { FirebaseMessagingTypes } from '@react-native-firebase/messaging';
import NavigationService from './NavigationService';
import Message from 'models/Message';
// import { Notification, NotificationOpen } from 'react-native-firebase/notifications';
import { Platform, Alert } from 'react-native';
// import inAppMessaging from '@react-native-firebase/in-app-messaging';
import PushNotification from "react-native-push-notification";
import { SCREENS_NAME } from 'screens/ScreenName';
import StorageHelper, { KeyStorage, TypeStorage } from './StorageHelper';
import moment from 'moment';
import { stores } from 'stores';


// Calling this function will open Google for login.
const DATABASE = database()

export const TYPE_NOTIFY = {
  Live: "1",
  Match: "2",
  Info: "3",
  Tips: "4",
  TopPredict: "5",
  MatchComment: "12",
  TipsComment: "14",
}

class FirebaseDbHelper {

  ref: FirebaseDatabaseTypes.Reference | undefined

  subscribeToTopic = async (topic?: string) => {
    if (topic) {
      await messaging().subscribeToTopic(topic);
    } else {
      await messaging().subscribeToTopic('over90m_live');
    }
  }

  getFcmToken = async () => {

    // .messaging()
    // .ios.registerForRemoteNotifications()
    // .then(() => {

    //   firebase
    //     .messaging()
    //     .ios.getAPNSToken()
    //     .then(token => {
    //       Alert.alert(
    //         'APNS TOKEN AFTER REGISTRATION FOR BACKGROUND',
    //         token,
    //       );
    //     });
    // });
    return await messaging().getToken()
  }

  backgroundMessageHandler = () => {
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
    });
  }

  notificationListener() {
    if (Platform.OS === 'ios') {
      return messaging().onMessage((remoteMessage: FirebaseMessagingTypes.RemoteMessage) => {
        let notShowComment = false;
        let notiData = remoteMessage.data
        try {
          let edata = JSON.parse(notiData?.eData)
          if (edata && edata.emailComment == stores.loginStore?.user?.email) {
            notShowComment = true
          }
        } catch {

        }
        if (!notShowComment) {
          PushNotification.localNotification({
            channelId: "com.fexpert.on90vip",
            title: remoteMessage.notification?.title,
            message: remoteMessage.notification?.body!,
            userInfo: remoteMessage.data,
            // messageId: remoteMessage.messageId,
            // id: remoteMessage.messageId,
          })
        }

      });
    } else {
      return undefined
    }

  }

  notificationOpenedListener() {
    PushNotification.configure({
      onNotification: (notification) => {
        console.log("NOTIFICATION:", notification)
        let notiData = notification.data
        if (notification.userInteraction) {
          this.replaceScreen(notiData)
        } else if (notification.foreground) {
          let notShowComment = false;
          try {
            let edata = JSON.parse(notiData?.eData)
            if (edata && edata.emailComment === stores.loginStore?.user?.email) {
              notShowComment = true
            }
          } catch {

          }
          if (!notShowComment) {
            PushNotification.localNotification({
              channelId: "com.fexpert.on90vip",
              title: notification.title,
              message: notification.message,
              userInfo: notification.data,
            })

          }

        }
      },
      popInitialNotification: false,
      onAction: (notification) => {
        console.log(notification)
      }
    })
  }


  handleNotifyAppClosed = async () => {

    // const remoteMessage = await messaging().getInitialNotification()
    let notiData = await new Promise((resolve) => {
      PushNotification.popInitialNotification((notification) => {
        resolve(notification?.data)
      });
    })
    this.replaceScreen(notiData)
  }


  scheduleNotificaton = async (id: string, title: string, message: string, data: any, dateTime: number, edata?: any) => {
    let mapSchedule = await this.getMatchSchedule()
    if (!mapSchedule[id]) {
      PushNotification.localNotificationSchedule({
        channelId: "com.fexpert.on90vip",
        title: title,
        message: message,
        userInfo: { type: "2", eData: JSON.stringify(edata) },
        id,
        date: moment.unix(dateTime).toDate(),
        allowWhileIdle: true,
      });
    } else {
      PushNotification.cancelLocalNotification(id )
    }

  }

  getMatchSchedule = async () => {
    let mapSchedule = await new Promise((resolve) => {
      let mapSchedule = {}
      PushNotification.getScheduledLocalNotifications((arr) => {
        arr.forEach((item) => {
          mapSchedule[item.id] = 1
        })
        resolve(mapSchedule)
      });
    }
    )
    return mapSchedule ? mapSchedule : {}
  }



  deleteSchedule = async (id: string) => {
    let mapSchedule = await StorageHelper.getItem(KeyStorage.SCHEDULE_MATCH, TypeStorage.object)
    mapSchedule = mapSchedule ? mapSchedule : {}
    if (mapSchedule[id]) {
      delete mapSchedule[id]
      StorageHelper.setItem(KeyStorage.SCHEDULE_MATCH, mapSchedule)
    }
  }

  // getMatchSchedule = async () => {
  //   const matchSchedule = await StorageHelper.getItem(KeyStorage.SCHEDULE_MATCH, TypeStorage.object)
  //   return matchSchedule ? matchSchedule : {}
  // }

  replaceScreen = (notiData?: any) => {
    let notiType = notiData?.type
    if (notiType === TYPE_NOTIFY.Live) {
      NavigationService.gotoMain(SCREENS_NAME.Live, { notiData });
    } else if (notiType === TYPE_NOTIFY.Match || notiType === TYPE_NOTIFY.MatchComment) {
      NavigationService.gotoMain(SCREENS_NAME.Match, { notiData });
    } else if (notiType === TYPE_NOTIFY.Info) {
      NavigationService.gotoMain(SCREENS_NAME.Info);
    } else if (notiType === TYPE_NOTIFY.Tips || notiType === TYPE_NOTIFY.TipsComment) {
      NavigationService.gotoMain(SCREENS_NAME.Tips, { notiData });
    } else if (notiType === TYPE_NOTIFY.TopPredict) {
      NavigationService.gotoMain(SCREENS_NAME.TopPredict);
    } else {
      NavigationService.gotoMain(SCREENS_NAME.Match);
    }
  }

  requestPermissionNotification = async () => {

    if (Platform.OS === 'android') {
      PushNotification.createChannel(
        {
          channelId: "com.fexpert.on90vip",
          channelName: "com.fexpert.on90vip",
          channelDescription: "com.fexpert.on90vip",
          soundName: "default",
          importance: 4,
          vibrate: true,
        },
        (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
      );
    }
    const authStatus = await messaging().hasPermission();
    console.log('a')
    if (authStatus == messaging.AuthorizationStatus.AUTHORIZED || authStatus == messaging.AuthorizationStatus.PROVISIONAL) {
      return true;
    } else {
      try {
        await messaging().requestPermission();
      } catch (error) {
        return false;
      }
    }
  }

  getAdminData(getAdminData: (adminData: any) => any) {
    // console.log('getAdminData');
    DATABASE.ref('/admin').limitToLast(50).once('value', snapshot => {
      getAdminData(snapshot.val())
    });

  }

  goOnline() {
    database().goOnline();
  }

  startChat(channelId: string | undefined, onReceiveMessage: (object: { newMessage?: Message, refresh?: boolean, isFirst?: boolean, lastItem?: boolean, isEmpty?: boolean }) => any) {
    let currentData = {};
    let refChannel = '/chat_all'
    if (channelId) {
      refChannel = '/channel_' + channelId
    }
    this.ref = DATABASE.ref(refChannel)
    this.ref.limitToLast(50).off('value');
    this.ref.limitToLast(50).on('value', snapshot => {
      let refresh = false;
      let isFirst = true;
      let length = Object.keys(currentData).length;
      if (length > snapshot.numChildren() || length === 0) {
        currentData = {};
        refresh = true;
      }
      let index = 0;
      let numChildren = snapshot.numChildren();
      if (snapshot.hasChildren()) {
        snapshot.forEach((child: any) => {
          let item = child.val();
          item.key = child.key;
          let lastItem = false;
          if (index === numChildren - 1) {
            lastItem = true;
          }
          if (!(child.key in currentData)) {
            //
            onReceiveMessage({ newMessage: item, refresh, isFirst, lastItem });
            isFirst = false;
          }
          index++;
          return false;
        })
        currentData = snapshot.val();

      } else {
        onReceiveMessage({ isEmpty: true })
      }

    });
  }

  goOffline() {
    database().goOffline();
  }

  stopChat() {
    this.ref?.limitToLast(50).off('value');
  }

  sendMessage = async (message: Message) => {
    this.ref?.push({
      id: message.id,
      text: message.text,
      name: message.name,
      timestamp: database.ServerValue.TIMESTAMP,
      photo: message.photo,
      score: message.score,
      position: message.position,
    });
  }

  testAddRom = () => {
    DATABASE.ref('/chat_all').set({})
  }
}



export default new FirebaseDbHelper();