import axios, { CancelTokenSource } from 'axios';
import { Alert } from 'react-native';
import { API } from 'configs';

export default class RequestHelper {
  source: CancelTokenSource;
  // source: any;

  constructor() {
    this.source = axios.CancelToken.source();
  }

  get = async (url: string, paramsObj: any, extra?: boolean) => {
    try {
      let urlFull = `${extra ? API.HOST_EXTRA : API.HOST}${url}${paramsObj ? '?' + this.qerystring(paramsObj) : ''}`
      console.log('RequestHelper url = ' + urlFull);
      let res:any = await axios({
        method: 'get',
        url: urlFull,
        cancelToken: this.source.token,
      });
      // console.log(res);
      if (res.status == 200) {
        return res.data;
      }
      // Alert.alert(`${res.status} - ${res.statusText}`);
      throw res;
    } catch (err) {
      // Alert.alert(`${res.status} - ${res.statusText}`);
      // Alert.alert(err.response.data.Message);
      console.log(err);
      throw err;
    }
  }

  getFullUrl = async (url: string, paramsObj: any) => {
    try {
      let urlFull = `${url}${paramsObj && '?' + this.qerystring(paramsObj)}`
      console.log('RequestHelper url = ' + urlFull);
      const res = await axios({
        method: 'get',
        url: urlFull,
        cancelToken: this.source.token,
      });
      // console.log(res);
      if (res.status == 200) {
        return res.data;
      }
      // Alert.alert(`${res.status} - ${res.statusText}`);
      throw res;
    } catch (err) {
      // Alert.alert(err.response.data.Message);
      throw err;
    }
  }

  cancelRequest = () => {
    try {
      if (this.source) {
        this.source.cancel();
      }
    } catch (err) {
      console.log(err.toString());
    }
  }

  qerystring = (paramsObj: any) => {
    var str = Object.keys(paramsObj).map(function (key) {
      return key + '=' + paramsObj[key];
    }).join('&');
    return str;
  }
}
