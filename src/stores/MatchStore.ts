// match/list-mine
import { observable, action } from 'mobx';
import { Requests } from 'helpers';
import axios, { CancelTokenSource } from 'axios';
const API = {
  MATCH: 'match/list-mine',
};

export interface IMatchStore {
  getListMatch(date: string, page: number, item: number, sourceCancel?: CancelTokenSource): void;
  cancel(source: any): void;
}

export class MatchStore implements IMatchStore {
  @action getListMatch = async (date: string, page: number, item: number, source: CancelTokenSource) => {
    let response = [];
    try {
      response = await Requests.get(API.MATCH, { date, page, item }, source);
      console.log('getListMatch');
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async (source: CancelTokenSource) => {
    await Requests.cancelRequest(source);
  }

}