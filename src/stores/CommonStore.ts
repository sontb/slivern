import { observable, action } from 'mobx';
import moment from 'moment';
import { NavigationService } from 'helpers';
import { SCREENS_NAME } from 'screens/ScreenName';
export interface ICommonStore {
  loading: boolean;
  tabBarVisible: boolean;
  loadingCompleted(): void;
  toggleLoading(): void;
  setLastTimeReload(): void;
  timeToReload(): void;
  hidingTabBar(): void;
  showTabBar(): void;
}

const FORMAT_TIME = 'YYYYMMDDHHmmss';
const DURATION_RELOAD = 2// hours

export class CommonStore implements ICommonStore {

  setup = async () => {
    // this.setLastTimeReload();
  }

  @observable loading = false;
  @observable tabBarVisible = true;
  lastTime = moment(new Date()).format(FORMAT_TIME);

  @action loadingCompleted() {
    this.loading = false;
  }

  @action toggleLoading() {
    this.loading = this.loading ? false : true;
  }

  @action setLastTimeReload = () => {
    this.lastTime = moment(new Date()).format(FORMAT_TIME);
  }

  @action timeToReload = async () => {
    try {
      let diff = - moment(this.lastTime, FORMAT_TIME).diff(new Date, "hour");
      if (diff >= DURATION_RELOAD) {
        this.lastTime = moment(new Date()).format(FORMAT_TIME);
        NavigationService.gotoMain(SCREENS_NAME.Match);
      }
      console.log(diff);
    } catch (err) {
      console.log(err)
    }
  }

  @action hidingTabBar = () => {
    this.tabBarVisible = false
  }
  @action showTabBar = () => {
    this.tabBarVisible = true
  }
}
