import { observable, action } from 'mobx';
import { StorageHelper, GoogleHelper, AppleHelper, FirebaseDbHelper } from 'helpers';
import { TypeStorage, KeyStorage } from 'helpers/StorageHelper'
import User from 'models/User';
import UserPredict from 'models/UserPredict';
// import { stores } from 'stores';
import { EventRegister } from 'react-native-event-listeners';
import { EVENT } from 'configs';
import { UserPredictApi } from 'api';
import { UserAddApi } from 'api';
export interface ILoginStore {
  user: User;
  signIn(isApple?: boolean): void;
  signOut(): void;
  userPredict: UserPredict;
  getUserPredict(): void;
  closeTipMatch(): void;
  addRewardPoint(): void;
  isShowTipMatch: boolean;
  isShowTipTopPredict: boolean;
  isShowTipComment: boolean;
  closeTipTopPredict(): void;
  closeTipComment(): void;
  selectedPositionNewsCategory: number;
  setSelectedPositionNewsCategory(position: number): void
}

export class LoginStore implements ILoginStore {


  @observable user: User | any = null;
  @observable userPredict: UserPredict = { point: 0, position: 0, guide: '', guideId1: -1 };
  userPredictApi = new UserPredictApi();
  userAddApi = new UserAddApi();
  guideId = 0;
  @observable isShowTipMatch = true;
  @observable isShowTipTopPredict = true;
  @observable isShowTipComment = true;
  selectedPositionNewsCategory = 0;

  setup = async () => {
    let user = await StorageHelper.getItem(KeyStorage.USER, TypeStorage.object);
    this.guideId = await StorageHelper.getItem(KeyStorage.GUIDE_CLOSED_TIPS_MATCH, TypeStorage.number);
    this.isShowTipTopPredict = ! await StorageHelper.getItem(KeyStorage.GUIDE_CLOSED_TIPS_TOP_PREDICT, TypeStorage.boolean)
    this.isShowTipComment = ! await StorageHelper.getItem(KeyStorage.GUIDE_CLOSED_TIPS_COMMENT, TypeStorage.boolean)
    this.selectedPositionNewsCategory = await StorageHelper.getItem(KeyStorage.LAST_SELECTED_NEW_CATEGORY_POSITION, TypeStorage.number)
    let fcmToken = await FirebaseDbHelper.getFcmToken()
    if (user) {
      this.user = user;
      this.userAddApi.add(user.uid, user.displayName === null ? '' : user.displayName, user.email, user.photo, fcmToken);
    }
    this.getUserPredict();
  }

  @action signIn = async (isApple?: boolean) => {
    let user = isApple ? await AppleHelper.signIn() : await GoogleHelper.signIn();
    if (user) {
      this.user = user;
      StorageHelper.setItem(KeyStorage.USER, user);
      this.userAddApi.add(user.uid, user.displayName === null ? '' : user.displayName, user.email!, user.photo!);
      this.getUserPredict();
      // stores.chatStore.goOnline();
      EventRegister.emit(EVENT.LOGIN_STATUS);
    }
  }

  @action signOut() {
    let signOut = this.user.type == 0 ? GoogleHelper.signOut() : AppleHelper.signOut();
    if (signOut) {
      this.user = null;
      StorageHelper.removeItem(KeyStorage.USER);
      // stores.chatStore.goOffline();
      this.getUserPredict();
    }
    EventRegister.emit(EVENT.LOGIN_STATUS);
    // this.loading = this.loading ? false : true;
  }


  @action getUserPredict = async () => {
    let userPredict: UserPredict = { point: 0, position: 0, guide: '', guideId: -1, guideId1: -1 };
    try {
      let email = '';
      if (this.user) {
        email = this.user.email;
      }
      userPredict = await this.userPredictApi.userPredictEmail(email);
      // userPredict = { total: 0, correct: 0, score: 0 }

    } catch (e) {
      console.log(e);
    }
    if (userPredict) {
      this.userPredict = userPredict;
    }
    let isShowTipMatch = this.guideId !== this.userPredict.guideId1 && this.userPredict.guideId1 != -1;
    this.isShowTipMatch = isShowTipMatch;
  }

  @action addRewardPoint = async () => {
    try {
      if (this.user) {
        let email = this.user.email;
        let res = await this.userPredictApi.userAddRewardPoint(email)
        if (res) {
          this.userPredict = { ...this.userPredict, point: this.userPredict.point + res.point }
        }
      }
    } catch {

    }
  }

  @action closeTipMatch() {
    StorageHelper.setItem(KeyStorage.GUIDE_CLOSED_TIPS_MATCH, this.userPredict.guideId1);
    this.guideId = this.userPredict.guideId1;
    this.isShowTipMatch = false;
  }


  @action closeTipTopPredict() {
    StorageHelper.setItem(KeyStorage.GUIDE_CLOSED_TIPS_TOP_PREDICT, true);
    this.isShowTipTopPredict = false
  }


  @action closeTipComment(){
    StorageHelper.setItem(KeyStorage.GUIDE_CLOSED_TIPS_COMMENT, true);
    this.isShowTipComment = false
  }

  setSelectedPositionNewsCategory(position: number) {
    StorageHelper.setItem(KeyStorage.LAST_SELECTED_NEW_CATEGORY_POSITION, position);
  }

}
