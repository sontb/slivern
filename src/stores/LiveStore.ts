// match/list-mine
import { observable, action } from 'mobx';
import { Requests } from 'helpers';
import axios, { CancelTokenSource } from 'axios';
const API = {
  LIVE: 'channels/list-v5?type=1&code=%1$s&lang=%2$s',
};

export interface ILiveStore {
  getListLive(date: string, page: number, item: number, sourceCancel?: CancelTokenSource): void;
  cancel(source: any): void;
}

export class LiveStore implements ILiveStore {
  @action getListLive = async (date: string, page: number, item: number, source: CancelTokenSource) => {
    let response = [];
    try {
      response = await Requests.get(API.LIVE, { date, page, item }, source);
      console.log('getListMatch');
    } catch (err) {
      console.log(err);
    }
    return response;
  }

  cancel = async (source: CancelTokenSource) => {
    await Requests.cancelRequest(source);
  }

}