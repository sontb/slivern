import { observable, action } from 'mobx';
import { FirebaseDbHelper, StorageHelper } from 'helpers';
import Message from 'models/message';
import { EventRegister } from 'react-native-event-listeners';
import { EVENT } from 'configs';
import { KeyStorage, TypeStorage } from 'helpers/StorageHelper';

export interface IChatStore {
  listMessage: any;
  adminData: any;
  goOnline(): void;
  goOffline(): void;
  setOnStartChatListener(channelId: string | undefined, onFinish?: () => any): void;
  stopChat(): void;
  sendMessage(message: Message): void;
  setOnMessageListner(onReceive: () => any): void;
  removeOnMessageListener(): void;
}

export class ChatStore implements IChatStore {

  @observable listMessage: any = [];
  @observable adminData: any = {}
  newMessageListener: any = '';

  @action goOnline = async () => {
    FirebaseDbHelper.goOnline();
  }

  @action setOnStartChatListener = async (channelId: string | undefined, onFinish: () => void) => {
    this.listMessage = []
    let adminDataSave = await StorageHelper.getItem(KeyStorage.ADMIN_DATA, TypeStorage.object);
    if (adminDataSave) {
      this.adminData = adminDataSave;
    }
    FirebaseDbHelper.getAdminData((adminData) => {
      if (JSON.stringify(adminDataSave) !== JSON.stringify(adminData)) {
        this.adminData = adminData;
        StorageHelper.setItem(KeyStorage.ADMIN_DATA, adminData);
      }
    });
    let listMessageTemp: any = [];
    FirebaseDbHelper.startChat(channelId, async ({ newMessage, refresh, isFirst, lastItem, isEmpty }) => {
      // this.listMessage.splice(0,this.listMessage.length);
      if (!isEmpty) {
        if (refresh) {
          if (isFirst) {
            // this.listMessage.clear();
            onFinish && onFinish();
          }
        } else {
          EventRegister.emit(EVENT.NEW_MESSAGE);
          if (this.listMessage.length > 50) {
            this.listMessage.pop();
          }
          this.listMessage.unshift(newMessage);
        }
        listMessageTemp.unshift(newMessage);
        if (lastItem) {
          let listMessage = await this.listMessage.slice();
          if (listMessage.length === 0 || listMessage[0].id !== listMessageTemp[0].id) {
            this.listMessage = listMessageTemp;
          }
        }
      } else {
        this.listMessage = []
        onFinish && onFinish();
      }
    })
  }

  setOnMessageListner = async (onReceive: () => any) => {
    this.newMessageListener = EventRegister.addEventListener(EVENT.NEW_MESSAGE, () => {
      onReceive();
    })
  }

  removeOnMessageListener = () => {
    EventRegister.removeEventListener(this.newMessageListener);
  }

  @action goOffline() {
    FirebaseDbHelper.goOffline();
  }

  @action stopChat() {
    FirebaseDbHelper.stopChat();
  }

  @action
  sendMessage(message: Message) {
    FirebaseDbHelper.sendMessage(message);
    // this.listMessage.pop();
    // this.listMessage.unshift(message);
    // this.listMessage.push(message);
  }

}
