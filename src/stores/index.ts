import { CommonStore } from './CommonStore';
import { TestStore } from './TestStore';
import { MatchStore } from './MatchStore';
import { LoginStore } from './LoginStore';
import { ChatStore } from './ChatStore';
import { LeagueStore } from './LeagueStore';
export const stores = {
  commonStore: new CommonStore(),
  testStore: new TestStore(),
  matchStore: new MatchStore(),
  loginStore: new LoginStore(),
  chatStore: new ChatStore(),
  leagueStore: new LeagueStore(),
  setup: () => {
    stores.loginStore.setup();
    stores.commonStore.setup();
  }
}