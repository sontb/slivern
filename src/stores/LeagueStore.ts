import { observable, action } from 'mobx';
import moment from 'moment';
import { NavigationService } from 'helpers';
import { SCREENS_NAME } from 'screens/ScreenName';
import { IItemMatch } from '../screens/match/ItemMatch'
import { LeagueParse } from 'api';
export interface ILeagueStore {
  listMatch: Array<IItemMatch>;
  listRank: Array<any>;
  listRound: Array<any>;
  selectedRound: number;
  selectedRoundName: string;
  getDetailData(href: string): void;
  getListMatchFromRound(selected: number, href: string): void;
}

const FORMAT_TIME = 'YYYYMMDDHHmmss';
const DURATION_RELOAD = 2// hours

export class LeagueStore implements ILeagueStore {

  setup = async () => {
    // this.setLastTimeReload();
  }

  @observable listMatch = [];
  @observable listRank = [];
  @observable listRound = [];
  @observable selectedRound = 0
  @observable selectedRoundName = ""
  @action getDetailData = async (href: string) => {
    try {
      this.listMatch = [];
      this.listRank = [];
      this.listRound = [];
      this.selectedRound = 0
      this.selectedRoundName = ""
      let listData: any = await LeagueParse.getLeagueDetail(href);
      this.listMatch = listData.listMatch
      this.listRank = listData.listRank
      this.listRound = listData.roundData.listRound
      this.selectedRound = listData.roundData.selected
      this.selectedRoundName = listData.roundData.listRound[listData.roundData.selected].name
    } catch {

    }
  }

  @action getListMatchFromRound = async (selected: number, href: string) => {
    try {
      let listData: any = await LeagueParse.getListMatchFromHref(href);
      this.listMatch = listData
      this.selectedRound = selected
      this.selectedRoundName = this.listRound[this.selectedRound].name
    } catch {

    }
  }
}
