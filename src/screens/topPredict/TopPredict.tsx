import { UserTopPredictApi } from 'api';
import { HeaderTranslucent, Loading, GuideTip } from 'components';
import { COLOR } from 'configs';
import { NavigationService } from 'helpers';
import React, { Component } from 'react';
import { ActivityIndicator, FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { SCREENS_NAME } from 'screens/ScreenName';
import { ILoginStore } from 'stores/LoginStore';
import { trans } from 'trans';
import ItemTopPredict from './ItemTopPredict';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { inject, observer } from 'mobx-react';

type Props = { navigation?: any, loginStore: ILoginStore };
@inject('loginStore')
@observer
export default class TopPredict extends Component<Props> {

  state = {
    listTopPredict: [],
    refreshing: false,
    loading: false,
    loadingMore: false,
  }
  userTopPredictApi = new UserTopPredictApi();

  didBlurSubscription: any;
  timeout: any;
  page = 0;
  SIZE = 20;
  canLoadMore = true;
  readyLoadMore = false;
  componentDidMount = async () => {
    this.getData();
  }

  componentWillUnmount() {
  }

  onSelectItem = (url: string) => {
    // let player = SCREENS.Player.name;
    // NavigationService.navigate(SCREENS_NAME.Player, { url });
  }

  getData = async () => {
    try {
      if (this.page == 0) {
        if (!this.state.refreshing) {
          this.setState({ loading: true });
        }
      } else {
        this.setState({ loadingMore: true })
      }
      let listTopPredict: any = await this.userTopPredictApi.getData(this.page, this.SIZE);
      if (listTopPredict) {
        if (this.page == 0) {
          this.setState({ listTopPredict, refreshing: false, loading: false });
        } else {
          let listPage = this.state.listTopPredict.concat(listTopPredict);
          this.setState({ listTopPredict: listPage, loadingMore: false });
        }
        if (listTopPredict.length == 0) {
          this.canLoadMore = false;
        }
      } else {
        this.setState({ loadingMore: false, refreshing: false, loading: false });
        this.canLoadMore = false;

      }
    } catch (err) {
      console.log(err);
      this.setState({ refreshing: false, loadingMore: false, loading: false });
      // this.props.commonStore!.loadingCompleted();
    }
  }

  _renderItem = ({ item, index }: any) => (
    <ItemTopPredict itemUser={item} position={index + 1} onSelectUser={() => NavigationService.navigate(SCREENS_NAME.PredictHistory, { email: item.email })} />
  )

  _onRefresh = async () => {
    this.refreshState();
    await this.setState({ refreshing: true })
    this.getData();
  }

  refreshState = () => {
    this.page = 0;
    this.canLoadMore = true;
    this.readyLoadMore = false;
    // this.setState({ refreshing: false, loadingMore: false });
  }

  _loadMore = () => {
    if (this.canLoadMore && this.readyLoadMore) {
      this.page = this.page + 1;
      this.getData();
    }
  }

  _onMomentumScrollBegin = () => {
    this.readyLoadMore = true;
  }

  _renderFooter = () => {
    if (!this.state.loadingMore) {
      return null;
    }
    return (<ActivityIndicator
      color={COLOR.PRIMARY_COLOR}
      animating
      size='large'
    />);
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>
        <HeaderTranslucent hasTabs title={trans.topChuyenGia} canBack />
        <View style={{ flex: 1, backgroundColor: COLOR.BG, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden', marginTop: 5 }}>
          {
            this.props.loginStore.isShowTipTopPredict &&
            <GuideTip text={trans.helpTopPredict} onPressClose={() => this.props.loginStore.closeTipTopPredict()} />

          
          }
          <FlatList
            contentContainerStyle={{ paddingBottom: 50 }}
            data={this.state.listTopPredict}
            renderItem={this._renderItem}
            extraData={this.state.refreshing}
            keyExtractor={(item: any, index) => `${item.uid}`}
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            onEndReachedThreshold={0.01}
            onEndReached={() => this._loadMore()}
            onMomentumScrollBegin={this._onMomentumScrollBegin}
            ListFooterComponent={this._renderFooter}
            removeClippedSubviews
          />
        </View>
        <Loading loading={this.state.loading} />
        {/* <AdmobBanner unitId={AdmobUnitId.banner_top_user_predict} /> */}
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
  },
});