// import SvgUri from 'react-native-svg-uri';
import { LevelIcon, TopIcon } from 'components';
import { COLOR } from 'configs';
import React, { PureComponent } from 'react';
import { Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { trans } from 'trans';
export const TYPE_LEAGUE = 'TYPE_LEAGUE';

export interface IItemTopPredict {
  name: string;
  email: string;
  photo: string;
  point: number;
  total: number;
  correct: number;
  position: number;
}

type Props = { itemUser: IItemTopPredict, position: number, onSelectUser: () => void };

export default class ItemTopPredict extends PureComponent<Props> {


  state = {
    level: '',
  }

  componentDidMount = () => {
    let { itemUser } = this.props;
    let score = itemUser.point;
    if (score < 0) {
      score = 0;
    }
    let level = '';
    if (score < 20) {
      level = trans.chuaCoCapDo;
    } else if (score >= 20 && score < 160) {
      level = trans.nghiepDu;
    } else if (score >= 160 && score < 440) {
      level = trans.banChuyen;
    } else if (score >= 440 && score < 1000) {
      level = trans.chuyenNghiep;
    } else if (score >= 1000) {
      level = trans.chuyenGia;
    }
    this.setState({
      level,
    })
  }

  render() {
    let { itemUser, position } = this.props;
    return (
      <TouchableOpacity style={{
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 10,
        backgroundColor: COLOR.WHITE, marginTop: 10, shadowColor: '#000',
        shadowRadius: 10,
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowOpacity: 0.03,
        borderRadius: 10,
        marginHorizontal: 10,
      }}
        onPress={this.props.onSelectUser}>
        <View style={[styles.toProfile, { marginLeft: 10 }]}>
          <Image style={styles.imgProfile} source={{ uri: itemUser.photo }} resizeMethod='resize' />
        </View>
        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', marginLeft: 15, marginRight: 20, justifyContent: 'center' }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            {
              (itemUser.point < 20) &&
              <Text style={[styles.txtName, { flex: 1 }]}>{itemUser.name}</Text>
            }
            {
              (itemUser.point >= 20 && itemUser.point < 160) &&
              <Text style={[styles.txtName, { flex: 1, color: COLOR.AMATEUR }]}>{itemUser.name}</Text>
            }
            {
              (itemUser.point >= 160 && itemUser.point < 440) &&
              <Text style={[styles.txtName, { flex: 1, color: COLOR.SEMIPRO }]}>{itemUser.name}</Text>
            }
            {
              (itemUser.point >= 440 && itemUser.point < 1000) &&
              <Text style={[styles.txtName, { flex: 1, color: COLOR.PROFESSION }]}>{itemUser.name}</Text>
            }
            {
              itemUser.point >= 1000 &&
              <Text style={[styles.txtName, { flex: 1, color: COLOR.EXPERT }]}>{itemUser.name}</Text>
            }

          </View>
          <Text style={{ color: COLOR.BLACK, marginTop: 5 }}>{`${this.state.level}, ${itemUser.point} ${trans.diem}`}</Text>
        </View>
        <View style={{ alignItems: 'center', marginRight: 10, minHeight: 50, justifyContent: 'center', alignSelf: 'center' }}>
          <LevelIcon score={itemUser.point} size={25} notHide={true} />
          <TopIcon position={this.props.position} size={20} containStyles={{ marginTop: 5 }} notHide={true} />
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 3,
    paddingBottom: 3,
  },
  toProfile: { width: 40, height: 40, borderRadius: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: COLOR.OVERLEY_DARK },
  imgProfile: { width: 36, height: 36, borderRadius: 18, backgroundColor: COLOR.WHITE },
  txtName: { color: COLOR.BLACK, fontSize: 18 },
});