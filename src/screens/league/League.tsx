import { LeagueApi } from 'api';
import { HeaderTranslucent, Loading } from 'components';
import { AdmobUnitId, COLOR, EVENT } from 'configs';
// import { NativeAdsManager } from 'node_customs/react-native-fbads';
import { TYPE_ADS } from 'configs/AdFanId';
import { NavigationService } from 'helpers';
import { inject, observer } from 'mobx-react';
// import { InterstitialAdManager } from 'node_customs/react-native-fbads';
import React, { Component } from 'react';
import { FlatList, StyleSheet, View } from 'react-native';
import { SCREENS_NAME } from 'screens/ScreenName';
import { ICommonStore } from 'stores/CommonStore';
import { trans } from 'trans';
import ItemLeague, { IItemLeague } from './ItemLeague';

type Props = { navigation?: any, commonStore: ICommonStore };
@inject('commonStore')
@observer
export default class League extends Component<Props> {

  state = {
    listLeague: [],
    refreshing: false,
    loading: false,
    lang: 'en',
  }
  leagueApi = new LeagueApi();
  // page = 0;
  // canLoadMore = true;
  // readyLoadMore = false;
  // didBlurSubscription: any;
  leagueID = -1;
  leagueName = '';
  focusListener: any;
  isFirstFullAds = true;
  nativeAdView: any;
  nativeAdId = 0;
  constructor(props: any) {
    super(props)
  }

  componentDidMount = async () => {
    this.getData();
    this.focusListener = this.props.navigation.addListener(
      'focus',
      () => {
        if (this.nativeAdView) {
          this.nativeAdView.refreshAdview()
        }
      }
    );

    let lang = trans.getLanguage();
    this.setState({ lang });
    // this.admobHelper.interstitial(AdmobUnitId.full_league);
    // IronSourceInterstitials.addEventListener('interstitialDidLoad', this.showInterstitial);
  }

  componentWillUnmount() {
    if (this.focusListener) {
      this.focusListener();
    }
    this.leagueApi.cancel();
    // IronSourceInterstitials.removeEventListener('interstitialDidLoad', this.showInterstitial);

  }

  // showInterstitial = () => {
  //   // IronSourceInterstitials.showInterstitial();
  //   IronSourceHelper.showInterstitial()
  // }

  onSelectItem = (item: IItemLeague) => {
    this.leagueID = item.ID;
    this.leagueName = item.NAME;
    let random = Math.floor(Math.random() * 3);

    if ((this.isFirstFullAds || random == 1) && EVENT.SHOW_ADS) {
      // InterstitialAdManager.showAd(AdFanId.full_league)
      //   .then((_) => {

      //   }).catch((_) => {

      //   })
      // IronSourceInterstitials.loadInterstitial();
      // IronSourceHelper.showInterstitial()
      AdmobHelper.interstitial(AdmobUnitId.full_league)
      this.isFirstFullAds = false
      // this.admobHelper.interstitial(AdmobUnitId.full_league);
    }
    NavigationService.navigate(SCREENS_NAME.LeagueTab, { leagueLogo: item.leagueLogo, leagueName: this.leagueName, href: item.href });


  }

  getData = async () => {
    try {
      if (!this.state.refreshing) {
        this.setState({ loading: true });
      }
      let listLeague: any = await this.leagueApi.getData();
      if (listLeague && listLeague.length > 0) {
        if (EVENT.SHOW_ADS) {
          listLeague.splice(2, 0, { ID: TYPE_ADS + this.nativeAdId, viewType: TYPE_ADS });
        }
        this.setState({ listLeague, refreshing: false, loading: false });
      } else {
        this.setState({ refreshing: false, loading: false });
      }
    } catch (err) {
      console.log(err);
      this.setState({ refreshing: false, loading: false });
    }
  }

  onAdFailedToLoad = () => {
    this.nativeAdId++
  }

  _renderItem = ({ item }: any) => (
    item.viewType === TYPE_ADS ?
      <AdmobNative style={{ marginTop: 10 }} adUnitID={AdmobUnitId.native_league} showAds={true}
        onAdFailedToLoad={this.onAdFailedToLoad} />
      // null
      // <AdsFanNative ref={(ref) => { this.nativeAdView = ref }} style={{ marginTop: 10 }} placementId={AdFanId.native_league} showAds={true} />
      :
      <ItemLeague itemLeague={item} onPress={this.onSelectItem} lang={this.state.lang} />
  )

  _onRefresh = async () => {
    await this.setState({ refreshing: true });
    this.getData();
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>
        <HeaderTranslucent hasTabs title={trans.league} />
        <View style={{ flex: 1, backgroundColor: COLOR.BG, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden' }}>
          <FlatList
            data={this.state.listLeague}
            renderItem={this._renderItem}
            // extraData={this.state.listLeague}
            keyExtractor={(item: IItemLeague, index) => `${item.ID}`}
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            contentInset={{ bottom: 100 }}
            removeClippedSubviews
          // refreshControl={
          //   <RefreshControl
          //     refreshing={this.state.refreshing}
          //     onRefresh={this._onRefresh}
          //   />
          // }
          />
        </View>
        {/* <AdmobBanner unitId={AdmobUnitId.banner_league} /> */}

        <Loading loading={this.state.loading} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
  },
});