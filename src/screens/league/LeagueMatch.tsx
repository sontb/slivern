import { AdsFanNative } from 'components';
import { AdFanId, COLOR } from 'configs';
import { NavigationService } from 'helpers';
import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import { FlatList, StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { SCREENS_NAME } from 'screens/ScreenName';
import PopupRight from 'screens/tips/PopupRight';
import { ICommonStore } from 'stores/CommonStore';
import { ILeagueStore } from 'stores/LeagueStore';
import { trans } from 'trans';
import ItemMatch, { IItemMatch } from '../match/ItemMatch';


const TYPE_ADS = 'TYPE_ADS';
type Props = { commonStore?: ICommonStore, leagueStore?: ILeagueStore };

@inject('commonStore', 'leagueStore')
@observer
export default class LeagueMatch extends Component<Props> {

  state = {
    refreshing: false,
    showPopupRound: false,
  }

  focusListener: any;
  nativeAdView: any;

  constructor(props: any) {
    super(props)

  }

  componentDidMount = async () => {
  }

  componentWillUnmount() {
    if (this.focusListener) {
      this.focusListener();
    }
  }

  onSelectedMatch = (itemMatch: IItemMatch) => {
    if (itemMatch.status != 4) {
      NavigationService.navigate(SCREENS_NAME.MatchDetail, { itemMatch });
    }
  }

  _renderItem = ({ item, index }: any) => {
    if (item.viewType === TYPE_ADS) {
      return <AdsFanNative ref={(ref) => { this.nativeAdView = ref }} style={{ marginVertical: 5 }} placementId={AdFanId.native_kqltd} showAds={true} />
    } else {
      return <ItemMatch itemMatch={item} onSelectedMatch={this.onSelectedMatch} />
    }
  };

  _onRefresh = async () => {
    // await this.setState({ refreshing: true })
  }

  onSelectedItemPopup = async (selected: number, item: any) => {
    this.setState({ showPopupRound: false })
    this.props.commonStore?.toggleLoading()
    try {
      await this.props.leagueStore?.getListMatchFromRound(selected, item.href)
    } catch {

    }
    this.props.commonStore?.loadingCompleted()
  }

  render() {
    let { leagueStore } = this.props
    return (
      <View style={styles.container}>
        {
          (leagueStore?.selectedRoundName ? true : false) &&
          <TouchableOpacity style={{
            alignSelf: 'stretch', padding: 10, backgroundColor: COLOR.WHITE,
            flexDirection: 'row', alignItems: 'center',
            borderBottomWidth: 1, borderBottomColor: COLOR.BORDER
          }}
            onPress={() => this.setState({ showPopupRound: true })}>
            <Text style={{ fontSize: 18, color: COLOR.MAIN_TEXT, marginLeft: 10 }}>{leagueStore?.selectedRoundName}</Text>
            <Icon name='caret-down-circle-outline' style={{ color: COLOR.MAIN_TEXT, fontSize: 20, marginLeft: 10 }} />
          </TouchableOpacity>
        }

        <FlatList
          contentContainerStyle={{ paddingBottom: 100, paddingTop: 10 }}
          data={leagueStore?.listMatch}
          extraData={this.state.refreshing}
          renderItem={this._renderItem}
          keyExtractor={(item, index) => `${item.id}`}
          refreshing={this.state.refreshing}
          onRefresh={this._onRefresh}
          removeClippedSubviews
        />

        <PopupRight
          showPopupRight={this.state.showPopupRound}
          listData={leagueStore?.listRound}
          style={{ position: 'absolute', bottom: 0, left: 0, right: 0 }}
          selected={leagueStore?.selectedRound}
          onSelectedItem={this.onSelectedItemPopup}
          onCancel={() => this.setState({ showPopupRound: false })}
          title={trans.round}
        />
      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
  },
});

