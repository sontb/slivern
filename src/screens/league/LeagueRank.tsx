import { COLOR } from 'configs';
import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import { ILeagueStore } from 'stores/LeagueStore';

// import { InterstitialAdManager } from 'node_customs/react-native-fbads';

type Props = { navigation?: any, route?: any, leagueStore?: ILeagueStore };
export interface IItemRank {
  DataH: string;
  DataHS: string;
  DataB: string;
  DataT: string;
  DataD: string;
  TeamName: string;
  DataST: string;
}

export interface IRankData {
  Title: string
  Data: IItemRank[]
}

@inject('leagueStore')
@observer
export default class LeagueRank extends Component<Props> {

  state = {
  }
  adsManager: any;
  constructor(props: any) {
    super(props)

  }

  componentDidMount = async () => {

  }

  componentWillUnmount() {
    // this.leagueDetailApi.cancel();
  }

  render() {
    let listRank = this.props.leagueStore?.listRank
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.BG }}>
        <View style={styles.row}>
          <Text style={[styles.name, { fontWeight: 'bold' }]}>Tên đội</Text>
          <Text style={[styles.cell, { fontWeight: 'bold' }]}>ST</Text>
          <Text style={[styles.cell, { fontWeight: 'bold' }]}>T</Text>
          <Text style={[styles.cell, { fontWeight: 'bold' }]}>H</Text>
          <Text style={[styles.cell, { fontWeight: 'bold' }]}>B</Text>
          <Text style={[styles.cell, { fontWeight: 'bold' }]}>HS</Text>
          <Text style={[styles.cell, { fontWeight: 'bold' }]}>Đ</Text>
        </View>
        <View style={{ width: '100%', height: 2, backgroundColor: COLOR.BG_GREY }} />
        <ScrollView style={{ flex: 1 }} contentContainerStyle={{ paddingBottom: 100 }}>
          {
            listRank && listRank.map((item: IRankData, index: number) => {
              return (
                <View style={{}} key={item.Title}>
                  { item.Title !== 'regular' &&
                    <View style={{ padding: 5, flexDirection: 'row', justifyContent: 'center' }}>
                      <Text style={{ fontWeight: 'bold', color: COLOR.GREEN }}>{item.Title}</Text>
                    </View>
                  }
                  {
                     item.Data.map((item: IItemRank, index: number) => {
                      return (
                        <View style={index % 2 === 0 ? styles.row1 : styles.row} key={item.TeamName}>
                          <Text style={styles.name}>{item.TeamName}</Text>
                          <Text style={styles.cell}>{item.DataST}</Text>
                          <Text style={styles.cell}>{item.DataT}</Text>
                          <Text style={styles.cell}>{item.DataH}</Text>
                          <Text style={styles.cell}>{item.DataB}</Text>
                          <Text style={styles.cell}>{item.DataHS}</Text>
                          <Text style={styles.cellD}>{item.DataD}</Text>
                        </View>
                      );
                    })
                  }
                </View>
              )
            })
          }
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
  },
  row: { flexDirection: 'row', padding: 6, backgroundColor: COLOR.WHITE },
  row1: { flexDirection: 'row', padding: 6, backgroundColor: COLOR.BG_HIGHLIGHT },
  name: { flex: 4, textAlign: 'left' },
  cell: { flex: 1, textAlign: 'center' },
  cellD: { flex: 1, textAlign: 'center', color: COLOR.RED }
});