import { HeaderTranslucent } from 'components';
import { COLOR } from 'configs';
import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import { Dimensions, StyleSheet, View, Image } from 'react-native';
import { ICommonStore } from 'stores/CommonStore';
import { trans } from 'trans';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import LeagueMatch from './LeagueMatch'
import LeagueRank from './LeagueRank';
import { ILeagueStore } from 'stores/LeagueStore';

const initialLayout = { width: Dimensions.get('window').width };

type Props = { navigation?: any, commonStore: ICommonStore, route?: any, leagueStore: ILeagueStore };
@inject('commonStore', 'leagueStore')
@observer
export default class LeagueTab extends Component<Props> {

  state = {
    routes: [
      { key: 'rank', title: trans.tabBXH },
      { key: 'match', title: trans.tabKQLTD },
    ],
    index: 0,
  }

  leagueName = this.props.route.params.leagueName
  href = this.props.route.params.href
  leagueLogo = this.props.route.params.leagueLogo


  constructor(props: any) {
    super(props)
  }

  componentDidMount = async () => {
    this.props.commonStore.toggleLoading()
    try {
      await this.props.leagueStore.getDetailData(this.href)
    } catch (err) {

    }
    this.props.commonStore.loadingCompleted()
  }

  componentWillUnmount() {

  }

  LeagueRank1 = () => (
    <LeagueRank />
  );

  LeagueMatch1 = () => (
    <LeagueMatch />
  );
  renderScene = SceneMap({
    rank: this.LeagueRank1,
    match: this.LeagueMatch1,
  });

  renderTabBar = (props: any) => (
    <TabBar
      {...props}
      indicatorStyle={{ backgroundColor: COLOR.ACCENT }}
      style={{ backgroundColor: COLOR.TRANS }}
      labelStyle={{fontWeight: 'bold'}}
    />
  );


  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>
        <HeaderTranslucent title={this.leagueName} canBack backgroundImage={this.leagueLogo} />

        <TabView
          style={{ marginTop: -10 }}
          navigationState={{ index: this.state.index, routes: this.state.routes }}
          renderScene={this.renderScene}
          onIndexChange={(index) => { this.setState({ index }) }}
          initialLayout={initialLayout}
          renderTabBar={this.renderTabBar}
        />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
});