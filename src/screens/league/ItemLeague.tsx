import { COLOR, GlobalStyles } from 'configs';
import React, { PureComponent } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
//lightbulb-dollar
export interface IItemLeague {
  ID: number;
  NAME: string;
  name_en: string;
  leagueLogo: string;
  href: string;
}

type Props = { itemLeague: IItemLeague, onPress: (item: IItemLeague) => void, lang: string };

export default class ItemLeague extends PureComponent<Props> {
  render() {
    let { itemLeague, onPress } = this.props;
    return (
      <TouchableOpacity style={[{
        flexDirection: 'row',
        alignItems: 'center', paddingLeft: 20,
        paddingRight: 10, paddingTop: 10, paddingBottom: 10, backgroundColor: COLOR.WHITE,
        borderRadius: 10,
        marginHorizontal: 10,
        marginTop: 10
      }, GlobalStyles.ShadowItemStyle]}
        onPress={() => onPress(itemLeague)}>
        <Image style={{ width: 40, height: 40 }} source={{ uri: itemLeague.leagueLogo }} resizeMethod='resize' resizeMode='contain' />
        <Text style={{ textAlign: 'left', fontWeight: 'bold', marginLeft: 15, marginTop: 10, marginBottom: 10, marginRight: 10 }}>{this.props.lang === 'vi' ? itemLeague.NAME : itemLeague.name_en}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});