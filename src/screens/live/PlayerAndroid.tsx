import { StreamById } from 'api';
// import { AdmobNativeBanner } from "components";
import { AdmobUnitId, COLOR, EVENT } from 'configs';
import {  NavigationService } from 'helpers';
import { inject, observer } from 'mobx-react';
import Video from 'node_customs/react-native-video';
import React, { Component, useState } from 'react';
import { ActivityIndicator, BackHandler, Dimensions, Platform, StatusBar, StyleSheet, Text, TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native';
import { GoogleSigninButton } from 'react-native-google-signin';
import KeepAwake from 'react-native-keep-awake';
import {
  hideNavigationBar,
  showNavigationBar
} from 'react-native-navigation-bar-color';
import Orientation, { OrientationType } from 'react-native-orientation-locker';
import SafeAreaView from 'react-native-safe-area-view';
import { default as Icon5, default as IconAwesome5 } from 'react-native-vector-icons/FontAwesome5';
// import Icon from 'react-native-vector-icons/Ionicons';
import IconMaterial from 'react-native-vector-icons/MaterialIcons';
import Chat from 'screens/chat/Chat';
// import { IChatStore } from 'stores/ChatStore';
import { ILoginStore } from 'stores/LoginStore';


const { height, width } = Dimensions.get('window');
type Props = { navigation: any, route: any, loginStore: ILoginStore };

@inject('loginStore')
@observer
export default class PlayerAndroid extends Component<Props> {
  player: any;
  intervalShowNavBar: any = undefined;

  streamById = new StreamById();

  state = {
    source: {},
    isFullScreen: false,
    loading: false,
    showNavBar: false,
    isSigninInProgress: false,
    showBanner: false,
    data: this.props.route.params.data,
    indexLink: 0,
  }

  isLockToLandscape = false;
  adsManager: any;
  timeoutAds: any;
  autoRotateState = false;
  currentUrl = this.state;
  onAdsClosed: any;
  constructor(props: any) {
    super(props)
   
  }



  componentDidMount = async () => {
    Orientation.addOrientationListener(this._orientationDidChange)
    Orientation.addLockListener
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)
    let random = Math.floor(Math.random() * 3);
    if (this.props.route.params.isFirstFullAds || random == 1) {
      // if (true) {

      // IronSourceHelper.showInterstitial({
      //   onError: () => {
      //     this.setState({ showBanner: true })
      //   },
      //   onClose: () => {
      //     this.setState({ showBanner: true })
      //   }
      // })

      // new AdmobHelper().interstitial(AdmobUnitId.full_live)
      console.log("test")

      // AdmobHelper.interstitial(AdmobUnitId.full_live, (isOk) => {
      //   if (!isOk) {
      //     this.setState({ showBanner: true })
      //   }
      // }, () => {
      //   this.setState({ showBanner: true })
      // })
      // .then(() => {
      //   console.log("sdfs")
      //   AdManager.checkAdsClose('fsd',()=>{
      //     console.log("sdfs")
      //     this.setState({ showBanner: true })
      //   })
      // }).catch(() => {
      //   this.setState({ showBanner: true })
      // })

      // let a = await AdManager.isTestDevice()
    } else {
      this.setState({ showBanner: true })
    }

    this.getData(this.state.data.url);
    this.showNavBar(true)
    Orientation.getOrientation(async (orientation) => {
      if (orientation === 'LANDSCAPE-LEFT' || orientation === 'LANDSCAPE-RIGHT') {
        StatusBar.setHidden(true)
        await this.setState({ isFullScreen: true });
        // this.props.chatStore.goOffline();
        hideNavigationBar()
      } else {
        StatusBar.setHidden(false)
        await this.setState({ isFullScreen: false });
        showNavigationBar()
      }
    })
  }

  componentWillUnmount = () => {
    Orientation.removeOrientationListener(this._orientationDidChange);
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick)
    StatusBar.setHidden(false)

    this.streamById.cancel()
    // Orientation.unlockAllOrientations()
    showNavigationBar()
    if (this.intervalShowNavBar) {
      clearTimeout(this.intervalShowNavBar);
    }
    if (this.timeoutAds) {
      clearTimeout(this.timeoutAds);
    }
  }

  handleBackButtonClick = () => {
    if (this.state.isFullScreen) {
      Orientation.lockToPortrait()
      StatusBar.setHidden(false)
      this.setState({ isFullScreen: false })
      // this.props.chatStore.goOnline();
      return true
    }
    return false
  }

  _orientationDidChange = async (orientation: OrientationType) => {
    if (orientation === 'LANDSCAPE-RIGHT' || orientation === 'LANDSCAPE-LEFT') {
      this.setState({ isFullScreen: true })
      StatusBar.setHidden(true)
      hideNavigationBar()
      // this.props.chatStore.goOffline();
    } else {
      this.setState({ isFullScreen: false })
      StatusBar.setHidden(false)
      showNavigationBar()
      // this.props.chatStore.goOnline();
    }
  }

  onClickChangeOrientation = async () => {
    await this.setState({ isFullScreen: !this.state.isFullScreen })
    this.showNavBar(true);
    if (this.state.isFullScreen) {
      Orientation.lockToLandscapeLeft();
    } else {
      Orientation.lockToPortrait();
    }
  }

  getData = async (urlIn: string, showNavBar?: boolean, indexLink?: number) => {
    this.setState({ loading: true, source: {} })
    if (showNavBar) {
      this.showNavBar(true, indexLink)
    }
    try {
      if (urlIn && !urlIn.startsWith('http') && !urlIn.startsWith('rtmp')) {
        let linkData = await this.streamById.getData(urlIn);
        console.log(linkData);
        if (linkData) {
          let headers: any = {}
          if (linkData.header.length > 0) {
            linkData.header.map((item: any) => {
              headers[item.name] = item.value;
            })
          }
          if (linkData.link && linkData.link !== '') {
            this.setState({ source: { uri: linkData.link, headers } });
          } else {
            this.setState({ loading: false })
            this.showLoadError();
          }
        } else {
          this.setState({ loading: false })
          this.showLoadError();
        }
      } else {
        await this.setState({ source: {} });
        this.setState({ source: { uri: urlIn, headers: {} } })
      }
    } catch (err) {
      console.log(err);
      this.setState({ loading: false })
      this.showLoadError();
    }

  }

  onBuffer = (state: any) => {
    this.setState({ loading: state.isBuffering })
  }
  videoError = (err: any) => {
    console.log(err);
    this.setState({ loading: false });
    this.showLoadError();
  }
  onLoad = () => {
    this.setState({ loading: false });
  }

  showLoadError = () => {
    // Alert.alert('Lỗi', 'Vui lòng thử lại');
  }


  onBack = async () => {
    NavigationService.back();
  }



  onRefresh = async () => {
    this.getData(this.state.data.url, true);
  }

  showNavBar = async (show?: boolean, indexLink?: number) => {
    let a = show ? true : false
    let b = 1
    await this.setState({ showNavBar: show ? show : !this.state.showNavBar, indexLink: indexLink ? indexLink : this.state.indexLink })
    if (this.intervalShowNavBar) {
      clearTimeout(this.intervalShowNavBar);
    }
    if (this.state.showNavBar) {
      this.intervalShowNavBar = setTimeout(() => {
        this.setState({ showNavBar: false })
      }, 7000)
    }
  }

  onSignIn = async () => {
    let { loginStore } = this.props;
    loginStore!.signIn();
  }

  onProgress = async () => {
    console.log("onProgress")
  }

  render() {
    return (
      <SafeAreaView style={{ backgroundColor: COLOR.BLACK_DARK, flex: 1 }} forceInset={{ top: 'always', left: this.state.isFullScreen ? 'never' : 'always', right: this.state.isFullScreen ? 'never' : 'always' }} >
        <StatusBar barStyle='light-content' backgroundColor={COLOR.BLACK_DARK} />

        <KeepAwake />

        <View style={styles.container}>
          <View
            style={{
              width: '100%',
              height: this.state.isFullScreen ? '100%' : width * 9 / 16,
              backgroundColor: COLOR.BLACK_DARK,
            }}
          >
            <TouchableWithoutFeedback onPress={() => this.showNavBar(false)}>

              <Video
                style={{ flex: 1 }}
                key={this.state.source.uri}
                source={this.state.source}
                ref={(ref) => {
                  this.player = ref
                }}
                ignoreSilentSwitch={"ignore"}
                onBuffer={this.onBuffer}
                onError={this.videoError}
                onLoad={this.onLoad}
                onPlaybackResume={() => {
                  console.log("sdf")
                }}
                playWhenInactive={false}
                playInBackground={false}
                resizeMode='contain'
              />

            </TouchableWithoutFeedback>
            {
              this.state.loading &&
              <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, alignItems: 'center', justifyContent: 'center' }}
                pointerEvents='none'>
                <ActivityIndicator
                  color={COLOR.PRIMARY_COLOR}
                  animating
                  size={Platform.OS === 'ios' ? 1 : 35}
                />
              </View>
            }

            {
              // this.state.showBanner && EVENT.SHOW_ADS &&
              // false &&
              // <AdmobNativeBanner style={{ position: 'absolute', bottom: 0, left: 0, right: 0 }}
              //   adUnitID={AdmobUnitId.native_banner_player} showAds={true} />
              //  <AdsFanNativeBanner placementId={AdFanId.native_banner_player} showAds={true} /> 
            }
            {
              this.state.showNavBar &&
              <NavigationBar
                onBack={this.onBack}
                onClickChangeOrientation={this.onClickChangeOrientation}
                onClickInside={() => this.showNavBar(true)}
                onClickLink={(url, indexLink) => this.getData(url, true, indexLink)}
                onRefresh={this.onRefresh}
                mainUrl={this.state.data.url}
                links={this.state.data.links}
                isFullScreen={this.state.isFullScreen}
                indexLink={this.state.indexLink}
              />
            }

          </View>


          {
            !this.state.isFullScreen &&
            <View style={{ flex: 1, flexDirection: 'column' }}>

              {
                this.props.loginStore.user ?
                  <Chat hideHeader channelId={this.state.data.href_id} />
                  :
                  <View style={{ alignSelf: 'stretch', flex: 1, backgroundColor: COLOR.BG, alignItems: 'center', justifyContent: 'center' }}>
                    <GoogleSigninButton
                      style={{}}
                      size={GoogleSigninButton.Size.Standard}
                      color={GoogleSigninButton.Color.Light}
                      onPress={this.onSignIn}
                      disabled={this.state.isSigninInProgress} />
                  </View>

              }

            </View>
          }

        </View>

        {/* </View> */}
      </SafeAreaView>
    );
  }
}

const NavigationBar = (props: {
  onBack: () => void,
  onClickInside: () => void,
  onRefresh: () => void,
  onClickLink: (url: string, indexLink: number) => void,
  links: any,
  mainUrl: string,
  onClickChangeOrientation: () => void,
  isFullScreen: boolean,
  indexLink: number,
}) => {
  const [indexLink, setIndexLink] = useState(props.indexLink)

  return (
    <View style={{
      position: 'absolute',
      paddingTop: 10,
      paddingBottom: 10,
      top: 0,
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: COLOR.BG_OVERLEY_DARK,
      width: '100%',
    }} onTouchEnd={(e) => {
      e.stopPropagation()
    }}>

      <TouchableOpacity style={{ padding: 7, marginLeft: 15 }} onPress={props.onBack}>
        {/* <Icon name='arrow-back' style={{ color: COLOR.WHITE, fontSize: 30 }} /> */}
        <Icon5 name='chevron-left' style={{ color: COLOR.WHITE, fontSize: 25, fontWeight: '100' }} />

      </TouchableOpacity>
      <TouchableWithoutFeedback onPress={props.onClickInside}>
        <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'flex-end' }}>
          <TouchableOpacity style={{ marginRight: 20, padding: 2, marginTop: 1 }} onPress={props.onRefresh}>
            <IconMaterial name='autorenew' size={25} color={COLOR.WHITE} />
          </TouchableOpacity>
          <TouchableOpacity style={[styles.linkContainer, { borderColor: indexLink == 0 ? COLOR.GREEN : COLOR.WHITE }]} onPress={() => {
            setIndexLink(0)
            props.onClickLink(props.mainUrl, 0)
          }}>
            <Text style={{ color: COLOR.WHITE }}>{`Link 1`}</Text>
          </TouchableOpacity>
          {props.links.map((item: any, index: any) => {
            return (
              <TouchableOpacity style={[styles.linkContainer, { borderColor: indexLink == index + 1 ? COLOR.GREEN : COLOR.WHITE }]} key={`key${index}`} onPress={() => {
                setIndexLink(index + 1)
                props.onClickLink(props.links[index].url, index + 1)
              }}>
                <Text style={{ color: COLOR.WHITE }}>{`Link ${index + 2}`}</Text>
              </TouchableOpacity>
            );
          })}

          <TouchableOpacity style={{ padding: 5, marginRight: 20, marginLeft: 20 }} onPress={props.onClickChangeOrientation}>
            {
              props.isFullScreen ?
                <IconAwesome5 name='compress' size={21} color={COLOR.WHITE} /> :
                <IconAwesome5 name='expand' size={21} color={COLOR.WHITE} />
            }
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>

    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: COLOR.BLACK_DARK
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  icon: {
    width: 24,
    height: 24,
  },
  backgroundVideo: {
    width: '100%',
    height: '30%',
    backgroundColor: COLOR.BG,
  },
  linkContainer: {
    borderRadius: 10,
    borderColor: COLOR.WHITE,
    borderWidth: 1,
    paddingVertical: 5,
    paddingHorizontal: 7,
    marginHorizontal: 5,
  },
});
