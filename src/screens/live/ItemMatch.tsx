import { COLOR, EVENT, GlobalStyles } from 'configs';
import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export const TYPE_MATCH = 'TYPE_MATCH';

export interface IChannel {
  id: number;
  name: string;
  url: string;
  time: string;
  date: string;
  group: string;
  logo: string;
  showGroup: boolean;
  logo_home: string;
  logo_away: string;
  notify: number;
  href_id: string;
}

type Props = {
  itemMatch: IChannel, onPress: (itemMatch: IChannel) => void,
  onPressCanNotify?: (itemMatch: IChannel) => void,
  onPressNotifyNow?: (itemMatch: IChannel) => void,
  onPressDelete?: (itemMatch: IChannel) => void
};

export default class ItemMatch extends React.PureComponent<Props> {

  // shouldComponentUpdate(nextProps: Props, nextState: any) {

  //   let a=  Object.keys(nextProps.itemMatch).filter(k => nextProps.itemMatch[k] !== this.props.itemMatch[k]);
  //   return false;
  // }

  onPressMatch = () => this.props.onPress(this.props.itemMatch)

  render() {
    let { itemMatch } = this.props;
    return (
      <View style={{ flex: 1 }} >
        {
          itemMatch.showGroup ?
            <Text style={{ paddingHorizontal: 10, paddingTop: 15, paddingBottom: 10, marginLeft: 5, marginTop: 2, fontSize: 15, fontWeight: 'bold' }}>{itemMatch.group}</Text>
            :
            <View style={{ width: '100%', height: 0 }} />
        }
        <TouchableOpacity style={[styles.container, GlobalStyles.ShadowItemStyle]} onPress={this.onPressMatch}>
          <View style={{ flexDirection: 'column', flex: 1, alignItems: 'center' }}>
            <View style={{ alignSelf: 'stretch', marginTop: 0, alignItems: 'center', justifyContent: 'center', flexDirection: 'row' }}>
              <View style={styles.logoContainer}>
                <Image style={{ width: 30, height: 30 }} source={itemMatch.logo_home ? { uri: itemMatch.logo_home } : null} resizeMethod='resize' />
              </View>
              <View style={{ flexDirection: 'column', alignItems: 'center', marginHorizontal: 10 }}>
                <Text style={{ fontWeight: 'bold', marginTop: 3, fontSize: 16, color: COLOR.GREEN }}>{itemMatch.time}</Text>
              </View>
              <View style={[styles.logoContainer]}>
                <Image style={{ width: 30, height: 30 }} source={itemMatch.logo_away ? { uri: itemMatch.logo_away } : null} resizeMethod='resize' />
              </View>
            </View>
            <Text style={{ fontSize: 16, marginTop: 10, fontWeight: 'bold' }}>{itemMatch.name}</Text>
            <View style={{ position: 'absolute', flexDirection: 'row', alignItems: 'center', top: 0, left: 0 }}>

              {
                itemMatch.logo ?
                  // true?
                  <Icon style={{ marginRight: 5 }} name="circle" size={10} color='red' />
                  : null
              }
              <Text style={{ fontSize: 13, color: COLOR.TITLE_BOX }}>{itemMatch.date}</Text>

            </View>
            {
              EVENT.DEV &&
              <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <TouchableOpacity style={{ padding: 10 }}
                  onPress={() => this.props.onPressCanNotify && this.props.onPressCanNotify(itemMatch)}>
                  <Text style={{ color: itemMatch.notify == 0 ? COLOR.MAIN_TEXT : COLOR.GREEN }}>Can Notify</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ padding: 10 }}
                  onPress={() => this.props.onPressNotifyNow && this.props.onPressNotifyNow(itemMatch)}>
                  <Text style={{ color: COLOR.MAIN_TEXT }}>Push Now</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ padding: 10 }}
                  onPress={() => this.props.onPressDelete && this.props.onPressDelete(itemMatch)}>
                  <Text style={{ color: COLOR.MAIN_TEXT }}>Delete</Text>
                </TouchableOpacity>
              </View>
            }


          </View>

        </TouchableOpacity>
      </View >

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: COLOR.WHITE,
    borderRadius: 10,
    marginHorizontal: 10,
    marginVertical: 5,
  },
  linkContainer: {
    borderRadius: 7,
    borderColor: COLOR.MAIN,
    borderWidth: 0.5,
    padding: 5,
    paddingLeft: 10,
    paddingRight: 10,
    marginLeft: 10, marginRight: 10
  },
  logoContainer: { width: 40, height: 40, backgroundColor: COLOR.BG_GREY, borderRadius: 100, alignItems: 'center', justifyContent: 'center' }
});