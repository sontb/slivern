import { LiveApi } from 'api';
import { AdmobNative, HeaderTranslucent, Loading } from 'components';
import { AdmobUnitId, COLOR, EVENT } from 'configs';
import { NavigationService } from 'helpers';
import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import { Alert, AppState, FlatList, Platform, StyleSheet, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { SCREENS_NAME } from 'screens/ScreenName';
import { ICommonStore } from 'stores/CommonStore';
import { ILoginStore } from 'stores/LoginStore';
import { trans } from 'trans';
import ItemMatch, { IChannel } from './ItemMatch';
const TYPE_ADS = 'TYPE_ADS';
type Props = { navigation?: any, loginStore: ILoginStore, commonStore: ICommonStore, route: any };
@inject('loginStore', 'commonStore')
@observer
export default class Live extends Component<Props> {
  state = {
    appState: AppState.currentState,
    listMatch: [],
    refreshing: false,
    loading: true,
  }
  constructor(props: any) {
    super(props);
  }


  liveApi = new LiveApi();

  currentUrl = '';
  adsManager: any;
  isFirstFullAds = true;

  focusListener: any;
  nativeAdView: any;
  nativeAdId = 0;

  componentDidMount = async () => {
    this.getData();
    try {
      let notiData = this.props.route.params?.notiData
      if (notiData) {
        let data = JSON.parse(notiData?.eData)
        NavigationService.navigate(SCREENS_NAME.PlayerAndroid, { data, isFirstFullAds: this.isFirstFullAds });
        if (this.isFirstFullAds) {
          this.isFirstFullAds = false
        }

      }
    } catch (err) {
      console.log(err)
    }

  }

  componentWillUnmount() {
    this.liveApi.cancel();

  }

  onSelectItem = (data: any) => {
    // this.currentUrl = url;
    NavigationService.navigate(SCREENS_NAME.PlayerAndroid, { data, isFirstFullAds: this.isFirstFullAds });

    if (this.isFirstFullAds) {
      this.isFirstFullAds = false
    }


  }

  getData = async () => {
    try {
      // let a = this.state.refreshing;
      // console.log(a);
      if (!this.state.refreshing) {
        // this.setState({ loading: true });
      }
      // let resData: any = await this.liveApi.getData();
      let listMatch = await this.liveApi.getDataV2();
      let group = '';
      if (listMatch && listMatch.length > 0) {
        for (let i = 0; i < listMatch.length; i++) {
          let item = listMatch[i];
          if (group === item.group) {
            item.showGroup = false;
          } else {
            item.showGroup = true;
            group = item.group;
          }
        }
        if (EVENT.SHOW_ADS) {
          if (listMatch.length >= 2) {
            listMatch.splice(2, 0, { id: TYPE_ADS + this.nativeAdId, viewType: TYPE_ADS });
          } else {
            listMatch.splice(1, 0, { id: TYPE_ADS + this.nativeAdId, viewType: TYPE_ADS });
          }
        }

        this.setState({ listMatch: listMatch, refreshing: false, loading: false });
      } else {
        this.setState({ refreshing: false, loading: false });
      }
    } catch (err) {
      console.log(err);
      this.setState({ refreshing: false, loading: false });
      // this.props.commonStore!.loadingCompleted();
    }
  }

  updateNotify = async (itemMatch: IChannel) => {
    this.setState({ loading: true })
    try {
      let result = await this.liveApi.updateNotify(itemMatch.id, itemMatch.notify == 0 ? 1 : 0);
      if (result) {
        itemMatch.notify = itemMatch.notify == 0 ? 1 : 0
      }
    } catch {

    }
    this.setState({ loading: false })
  }

  notifyNow = async (itemMatch: IChannel) => {
    this.setState({ loading: true })
    try {
      let result = await this.liveApi.notifyNow(itemMatch.name, itemMatch);
    } catch {

    }
    this.setState({ loading: false })
  }

  deleteChannel = async (itemMatch: IChannel, position: number) => {
    Alert.alert('Thông báo', 'Xóa ' + itemMatch.name, [{
      text: 'Hủy',
      onPress: () => console.log('Cancel Pressed'),
      style: 'cancel',
    },
    {
      text: 'Ok',
      onPress: async () => {
        this.setState({ loading: true })
        try {
          let result = await this.liveApi.delete(itemMatch.id);
          if (result) {
            let listMatch = this.state.listMatch
            listMatch.splice(position, 1);
            this.setState({ listMatch })
          }
        } catch {

        }
        this.setState({ loading: false })
      },
    }])

  }

  onAdFailedToLoad = () => {
    this.nativeAdId++
  }

  _renderItem = ({ item, index }: any) => {
    if (item.viewType === TYPE_ADS) {
      // return <AdsFanNative ref={(ref)=>{this.nativeAdView=ref}}  style={{ marginVertical: 5 }} placementId={AdFanId.native_live} showAds={true} />
      return <AdmobNative style={{ marginVertical: 5 }} adUnitID={AdmobUnitId.native_live} showAds={true}
        onAdFailedToLoad={this.onAdFailedToLoad} />
    }
    return <ItemMatch itemMatch={item} onPress={this.onSelectItem}
      onPressCanNotify={this.updateNotify}
      onPressNotifyNow={this.notifyNow}
      onPressDelete={(itemMatch: IChannel) => this.deleteChannel(itemMatch, index)} />
  }

  _onRefresh = async () => {
    await this.setState({ refreshing: true });
    this.getData();
  }

  goToChat = async () => {
    if (this.props.loginStore.user) {
      NavigationService.navigate(SCREENS_NAME.Chat, null);
      // this.setState({ showPopupChat: true })
    } else {
      if (Platform.OS === 'ios' && EVENT.APPSTORE == 0) {
        Alert.alert(trans.login, trans.chooseLoginMethod, [
          {
            text: trans.huy,
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          {
            text: 'Google',
            onPress: async () => {
              await this.props.loginStore.signIn();
              if (this.props.loginStore.user) {
                NavigationService.navigate(SCREENS_NAME.Chat, null);
              }
            },
          },
          {
            text: 'Apple',
            onPress: async () => {
              await this.props.loginStore.signIn(true);
              if (this.props.loginStore.user) {
                NavigationService.navigate(SCREENS_NAME.Chat, null);
              }
            }
          }
        ])
      } else {
        await this.props.loginStore.signIn();
        if (this.props.loginStore.user) {
          NavigationService.navigate(SCREENS_NAME.Chat, null);
        }
      }
    }
  }



  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>
        <HeaderTranslucent hasTabs title={trans.trucTiepBongDa} onPressRight={this.goToChat}
          iconRight={(<Icon name="md-chatbubbles" size={25} color={COLOR.WHITE} />)} />
        <View style={{ flex: 1, backgroundColor: COLOR.BG, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden' }}>
          <FlatList
            contentContainerStyle={{ paddingBottom: 100 }}
            data={this.state.listMatch}
            renderItem={this._renderItem}
            keyExtractor={(item: IChannel, index) => `${item.id}`}
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            removeClippedSubviews
          />
        </View>
        {/* <PopupChat showPopupChat={this.state.showPopupChat} onCancel={() => this.setState({ showPopupChat: false })} /> */}
        <Loading loading={this.state.loading} />
        {/* <AdmobBanner unitId={AdmobUnitId.banner_live} /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
  },
});