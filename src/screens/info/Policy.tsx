import { HeaderTranslucent } from "components";
import { COLOR } from "configs";
import React, { Component } from "react";
import { Linking, Share, StyleSheet, Text, TouchableOpacity, View, Platform, ScrollView } from "react-native";
// import SvgUri from "react-native-svg-uri";
import codePush from 'react-native-code-push';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconAwesome5 from 'react-native-vector-icons/FontAwesome5';
import DeviceInfo from 'react-native-device-info';

import { trans } from "trans";
import WebView from "react-native-webview";
import { policyHtml } from "./policies";
// const adsManager = new NativeAdsManager(AdFanId.native_kqltd, 10);

export default class Policy extends Component {

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>
        <HeaderTranslucent title='Policy' canBack />
        <WebView style={{ flex: 1 }} source={{ html: policyHtml }} />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    paddingLeft: 50,
    paddingTop: 50
  },
  textTitle: {
    color: COLOR.WHITE,
    fontSize: 45,
    marginBottom: 15,
  },
  textDescription: {
    color: COLOR.WHITE,
    fontSize: 17,
    marginTop: 3,
  },
  textSubText: {
    color: COLOR.OVERLEY_LIGHT,
    fontSize: 14,
    marginTop: 15,
  },
  button: {
    paddingHorizontal: 10,
    padding: 7,
    // flexDirection: 'column',
    // alignItems: 'center',
    borderColor: COLOR.SUB_TEXT_LIGHT,
    borderWidth: 1,
    borderRadius: 7,
    marginTop: 30
  }
})