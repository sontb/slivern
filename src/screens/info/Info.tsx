import { HeaderTranslucent } from "components";
import { COLOR } from "configs";
import React, { Component } from "react";
import { Linking, Share, StyleSheet, Text, TouchableOpacity, View, Platform, ScrollView } from "react-native";
// import SvgUri from "react-native-svg-uri";
import codePush from 'react-native-code-push';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconAwesome5 from 'react-native-vector-icons/FontAwesome5';
import DeviceInfo from 'react-native-device-info';

import { trans } from "trans";
import { NavigationService } from "helpers";
import { SCREENS_NAME } from "screens/ScreenName";
// const adsManager = new NativeAdsManager(AdFanId.native_kqltd, 10);

export default class Info extends Component {

  state = {
    label: '',
    version: DeviceInfo.getVersion(),
  }

  componentDidMount() {
    codePush.getUpdateMetadata().then((metadata) => {
      if (metadata) {
        this.setState({ label: metadata.label, version: metadata.appVersion });
      }
    });
  }

  gotoAppStore = () => {
    if(Platform.OS==='ios'){
      // https://apps.apple.com/vn/app/over-90/id1545518139?l=vi
      Linking.canOpenURL('itms-apps://itunes.apple.com/us/app/id1549899465?mt=8').then(supported => {
        supported && Linking.openURL('itms-apps://itunes.apple.com/us/app/id1549899465?mt=8');
      }, (err) => console.log(err));
    } else {
      Linking.canOpenURL('market://details?id=com.fexpert.on90vip').then(supported => {
        supported && Linking.openURL('market://details?id=com.fexpert.on90vip');
      }, (err) => console.log(err));
    }
   
  }

  openFangape = () => {
    Linking.canOpenURL('fb://profile/102976341089520').then(supported => {
      if (supported) {
        Linking.openURL('fb://profile/102976341089520');
      } else {
        Linking.openURL('https://www.facebook.com/SLive-247-102976341089520');
      }
    }, (err) => console.log(err));
  }

  shareApp = () => {
    let link = 'https://on90.page.link/live';
    Share.share(
      {
        message: link,
        title: 'On 90 Trực tiếp bóng đá - (Android/Ios)'
      })
      .then(result => console.log(result))
      .catch(err => console.log(err));
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>
        <HeaderTranslucent title='' canBack/>
        <ScrollView removeClippedSubviews={true}>
          <View style={styles.container}>
            <Text style={styles.textTitle}>On 90</Text>
            <Text style={styles.textDescription}>{trans.trucTiepBongDa}</Text>
            <Text style={styles.textDescription}>{trans.tinTucBongDa}</Text>
            <Text style={styles.textDescription}>{trans.ketQuaLichThiDau}</Text>
            <Text style={styles.textDescription}>{trans.bangXepHang}</Text>
            <Text style={styles.textSubText}>{trans.phienBan}</Text>
            <Text style={styles.textDescription}>{this.state.version} - {this.state.label}</Text>
            <Text style={styles.textSubText}>{trans.emailGopY}</Text>
            <Text style={[styles.textDescription, { textDecorationLine: 'underline' }]}>sscoreonline@gmail.com</Text>

            <TouchableOpacity style={styles.button} onPress={this.gotoAppStore}>
              <Text style={styles.textDescription}>{trans.danhGiaUngDung}</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20, padding: 10, marginLeft: -5 }} onPress={this.shareApp}>
              <Icon name='share' size={25} color={COLOR.WHITE} style={{ marginRight: 10 }} />
              <Text style={{
                color: COLOR.WHITE,
                fontSize: 15,
              }}>{trans.chiaSeUngDung}</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{}} onPress={()=>NavigationService.navigate(SCREENS_NAME.Policy, {})}>
              <Text style={[styles.textDescription, { textDecorationLine: 'underline' }]}>Policy</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5, padding: 10, marginLeft: -5 }} onPress={this.openFangape}>
              <IconAwesome5 name='thumbs-up' size={25} color={COLOR.WHITE} style={{ marginRight: 10 }} />
              <Text style={{
                color: COLOR.WHITE,
                fontSize: 17,
              }}>Like Fanpage</Text>
            </TouchableOpacity> */}
          </View>
        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'flex-start',
    paddingLeft: 50,
    paddingTop: 50
  },
  textTitle: {
    color: COLOR.WHITE,
    fontSize: 45,
    marginBottom: 15,
  },
  textDescription: {
    color: COLOR.WHITE,
    fontSize: 17,
    marginTop: 3,
  },
  textSubText: {
    color: COLOR.OVERLEY_LIGHT,
    fontSize: 14,
    marginTop: 15,
  },
  button: {
    paddingHorizontal: 10,
    padding: 7,
    // flexDirection: 'column',
    // alignItems: 'center',
    borderColor: COLOR.SUB_TEXT_LIGHT,
    borderWidth: 1,
    borderRadius: 7,
    marginTop: 30
  }
})