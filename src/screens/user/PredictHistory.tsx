import { MatchOddApi } from 'api';
import { HeaderTranslucent, Loading } from 'components';
import { COLOR, EVENT } from 'configs';
import { NavigationService } from 'helpers';
import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import { ActivityIndicator, BackHandler, FlatList, StyleSheet, View } from 'react-native';
import { SCREENS_NAME } from 'screens/ScreenName';
import { ICommonStore } from 'stores/CommonStore';
import { ILoginStore } from 'stores/LoginStore';
import { trans } from 'trans';
import ItemLeague, { TYPE_LEAGUE } from '../match/ItemLeague';
import ItemLoading, { TYPE_LOADING } from '../match/ItemLoading';
import ItemMatch, { IItemMatch, TYPE_MATCH } from '../match/ItemMatch';

const TYPE_ADS = 'TYPE_ADS';
type Props = { navigation: any, loginStore?: ILoginStore, commonStore?: ICommonStore, route?: any };

// @inject('matchStore', 'commonStore')
// @observer
@inject('commonStore', 'loginStore')
@observer
export default class PredictHistory extends Component<Props> {

  state = {
    listMatch: [],
    refreshing: false,
    loadingMore: false,
    loading: false,
  }
  email = this.props.route.params.email

  matchApi = new MatchOddApi();
  page = 0;
  SIZE = 10;
  canLoadMore = true;
  readyLoadMore = false;
  adsManager: any;
  constructor(props: any) {
    super(props)
  }

  componentDidMount = async () => {
    let a = this.constructor.name
    this.refreshState();
    this.getData()
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick)

  }

  componentWillUnmount() {
    this.matchApi.cancel();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick)

  }

  handleBackButtonClick = () => {
    NavigationService.back()
    return true
  }

  getData = async () => {
    try {

      if (this.page == 0) {
        if (!this.state.refreshing) {
          this.setState({ loading: true });
        }
      } else {
        this.setState({ loadingMore: true })
      }
      let email = this.email ? this.email : this.props.loginStore!.user.email;

      let listMatch: any = await this.matchApi.getListMatchPredict(email, this.page, this.SIZE);
      if (listMatch) {
        let listData = this.getListLeagueFromListMatch(listMatch);
        if (this.page == 0) {
          this.setState({ listMatch: listData, refreshing: false, loading: false });
        } else {
          let listPage = this.state.listMatch.concat(listData);
          this.setState({ listMatch: listPage, loadingMore: false });
        }
        if (listMatch.length == 0) {
          this.canLoadMore = false;
        }
      } else {
        this.canLoadMore = false;
        this.setState({ loadingMore: false, refreshing: false, loading: false });
      }
    } catch (err) {
      console.log(err);
      this.setState({ refreshing: false, loadingMore: false, loading: false });
    }
  }

  getListLeagueFromListMatch = (listMatch: any) => {
    let listData: any = [];
    if (listMatch && listMatch.length > 0) {
      let checkLeague = ''
      for (let i = 0; i < listMatch.length; i++) {
        let match = listMatch[i];
        if (checkLeague !== match.leagueName) {
          let leagueMine: any = {};
          leagueMine.id = 'league' + match.id;
          leagueMine.leagueName = match.leagueName;
          leagueMine.leagueId = match.leagueId;
          leagueMine.leagueLogo = match.leagueLogo;
          leagueMine.viewType = TYPE_LEAGUE;
          leagueMine.hrefLeague = match.hrefLeague;
          listData.push(leagueMine);
          checkLeague = match.leagueName;
        }
        match.viewType = TYPE_MATCH;
        listData.push(match);
      }

    }
    return listData;
  }

  onSelectedMatch = (itemMatch: IItemMatch) => {
    if (itemMatch.status != 4 && !this.email) {
      NavigationService.navigate(SCREENS_NAME.MatchDetail, { itemMatch });
    }
  }

  _renderItem = ({ item, index }: any) => {
    if (item.viewType === TYPE_LEAGUE) {
      return <ItemLeague itemLeague={item} />
    } else if (item.viewType === TYPE_LOADING) {
      return <ItemLoading />
    } else {
      return <ItemMatch itemMatch={item}
        onSelectedMatch={this.onSelectedMatch} />
    }
  };

  _onRefresh = async () => {
    this.refreshState();
    await this.setState({ refreshing: true })
    this.getData();
  }

  refreshState = () => {
    this.page = 0;
    this.canLoadMore = true;
    this.readyLoadMore = false;
    // this.setState({ refreshing: false, loadingMore: false });
  }

  _loadMore = () => {
    if (this.canLoadMore && this.readyLoadMore) {
      this.page = this.page + 1;
      this.getData();
    }
  }

  _onMomentumScrollBegin = () => {
    this.readyLoadMore = true;
  }

  _renderFooter = () => {
    if (!this.state.loadingMore) {
      return null;
    }
    return (<ActivityIndicator
      color={COLOR.PRIMARY_COLOR}
      animating
      size='large'
    />);
  }

  render() {
    let { loginStore } = this.props;
    return (
      <View style={styles.container}>
        <HeaderTranslucent hasTabs title={trans.lichSuDiem} canBack />

        {/* <View style={{
          alignItems: 'center',
          flexDirection: 'row',
          marginTop: -10,
        }}> */}


        <View style={{ flex: 1, backgroundColor: COLOR.BG, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden', marginTop: 5 }}>
          <FlatList
            contentContainerStyle={{ paddingBottom: 100 }}
            data={this.state.listMatch}
            extraData={this.state.refreshing}
            renderItem={this._renderItem}
            keyExtractor={(item: IItemMatch, index) => `${item.id}`}
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            onEndReachedThreshold={0.01}
            onEndReached={() => this._loadMore()}
            onMomentumScrollBegin={this._onMomentumScrollBegin}
            ListFooterComponent={this._renderFooter}
            removeClippedSubviews
          />
        </View>
        <Loading loading={this.state.loading} />
      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.MAIN,
  },
});

