import { AppleButton } from '@invertase/react-native-apple-authentication';
import { Avatar, HeaderTranslucent, LevelIcon, TopIcon } from 'components';
import { AdmobUnitId, COLOR, EVENT } from 'configs';
import { GlobalFuntion, NavigationService } from 'helpers';
import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import {
  Alert,
  Dimensions, Image, Linking,
  Platform, RefreshControl,
  ScrollView, StyleSheet, Text,
  TouchableOpacity, View
} from 'react-native';
// import SvgUri from 'react-native-svg-uri';
import { EventRegister } from 'react-native-event-listeners';
import { GoogleSigninButton } from 'react-native-google-signin';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { SCREENS_NAME } from 'screens/ScreenName';
import { IChatStore } from 'stores/ChatStore';
import { ICommonStore } from 'stores/CommonStore';
import { ILoginStore } from 'stores/LoginStore';
import { trans } from 'trans';

const width = Dimensions.get('window').width - 100;

type Props = { loginStore?: ILoginStore, commonStore?: ICommonStore, chatStore?: IChatStore, navigation: any };
@inject('loginStore', 'commonStore', 'chatStore')
@observer
export default class User extends Component<Props> {

  state = {
    level: '',
    missing: '',
    refreshing: false,
  };
  loginStatusListener: any;
  focusListener: any;
  componentDidMount = async () => {
    let { loginStore } = this.props;
    this.loginStatusListener = EventRegister.addEventListener(EVENT.LOGIN_STATUS, () => {
      if (loginStore!.user) {
        // this.getData();
      }
    });
    this.focusListener = this.props.navigation.addListener(
      'focus',
      () => {
        // this.getData();
      }
    );
  }

  componentWillUnmount = () => {
    EventRegister.removeEventListener(this.loginStatusListener);
    this.focusListener()
  }

  seeAds = async () => {
    this.props.commonStore!.toggleLoading();
    // IronSourceHelper.showInterstitial({
    //   onLoaded: () => {
    //     this.props.commonStore!.loadingCompleted();
    //   },
    //   onError: () => {
    //     this.props.commonStore!.loadingCompleted();
    //     Alert.alert("Thống báo", trans.seeAdsError)
    //   },
    //   onClose: () => {
    //     this.addRewardPoint()
    //   }
    // })
    // AdmobHelper.interstitial(AdmobUnitId.full_user, (isOk) => {
    //   this.props.commonStore!.loadingCompleted();
    //   if (!isOk) {
    //     Alert.alert("Thống báo", trans.seeAdsError)

    //   }
    // }, () => {
    //   this.addRewardPoint()

    // })

  }

  addRewardPoint = async () => {
    this.props.commonStore!.toggleLoading();
    await this.props.loginStore?.addRewardPoint()
    this.props.commonStore!.loadingCompleted();
  }

  getData = async () => {
    this.props.commonStore!.toggleLoading();
    try {
      await this.props.loginStore!.getUserPredict();
      let userPredict = this.props.loginStore!.userPredict;
      if (userPredict) {
        let level = '';
        let missing = '';
        if (userPredict.point === 0) {
          level = `${trans.capDo}: ${trans.chuaDat}`;
          missing = trans.banChuaDuDoanKetQua;
        } else if (userPredict.point < 20) {
          level = `${trans.capDo}: ${trans.chuaDat}`;
          missing = `${trans.banConThieu} ${20 - userPredict.point} ${trans.diemDeDatDenCapDo} ${trans.nghiepDu}`;
        } else if (userPredict.point >= 20 && userPredict.point < 160) {
          level = `${trans.capDo}: ${trans.nghiepDu}`;
          missing = `${trans.banConThieu} ${160 - userPredict.point} ${trans.diemDeDatDenCapDo} ${trans.banChuyen}`;
        } else if (userPredict.point >= 160 && userPredict.point < 440) {
          level = `${trans.capDo}: ${trans.banChuyen}`;
          missing = `${trans.banConThieu} ${440 - userPredict.point} ${trans.diemDeDatDenCapDo} ${trans.chuyenNghiep}`;
        } else if (userPredict.point >= 440 && userPredict.point < 1000) {
          level = `${trans.capDo}: ${trans.chuyenNghiep}`;
          missing = `${trans.banConThieu} ${1000 - userPredict.point} ${trans.diemDeDatDenCapDo} ${trans.chuyenGia}`;
        } else if (userPredict.point >= 1000) {
          level = `${trans.capDo}: ${trans.chuyenGia}`;
          missing = `${trans.banDaLaChuyenGia}`;
        }
        this.setState({
          level,
          missing,
        })
      }
      this.props.commonStore!.loadingCompleted();
    } catch (e) {
      this.props.commonStore!.loadingCompleted();
      console.log(e);
    }
  }


  onSignOut = async () => {
    let { loginStore, chatStore } = this.props;
    // await NavigationService.back();
    loginStore!.signOut();
    chatStore!.goOffline();
    chatStore!.stopChat();
  }

  gotoProfile = async () => {
    Linking.canOpenURL('https://myaccount.google.com/personal-info').then(supported => {
      supported && Linking.openURL('https://myaccount.google.com/personal-info');
    }, (err) => console.log(err));
  }

  onSignIn = async (isApple?: boolean) => {
    let { loginStore } = this.props;
    GlobalFuntion.requestLogin(loginStore!)
  }

  gotoTopUserPredict = async () => {
    NavigationService.navigate(SCREENS_NAME.TopPredict, {});
  }

  gotoPredictHistory = async () => {
    NavigationService.navigate(SCREENS_NAME.PredictHistory, {});
  }

  goToInfo = async () => {
    NavigationService.navigate(SCREENS_NAME.Info, {});
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    // await this.getData()
    this.setState({ refreshing: false })
  }

  render() {
    let { loginStore } = this.props;
    let userPredict = loginStore!.userPredict;
    if (loginStore!.user) {
      return (
        <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>
          <HeaderTranslucent title={trans.taiKhoan} onPressRight={this.goToInfo}
            hasTabs
            iconRight={(<Image style={{ width: 24, height: 24, tintColor: COLOR.WHITE }} source={require('assets/ic_drawer_info_1.png')} resizeMode='contain' />)} />

          <View style={{ flex: 1, backgroundColor: COLOR.WHITE, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden' }}>

            <ScrollView style={{ flex: 1 }} contentContainerStyle={{ paddingBottom: 100 }}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }>
              <View style={{ flex: 1 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', paddingVertical: 20, borderBottomWidth: 0, borderBottomColor: COLOR.BG }}>

                  <Avatar size={74} photo={loginStore!.user.photo} style={{ marginLeft: 20 }} />
                  <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', marginLeft: 20, marginRight: 20, justifyContent: 'center' }}>
                    <Text style={[styles.txtName, { flex: 1 }]}>
                      {loginStore!.user !== null ? loginStore!.user.displayName : ''}
                    </Text>
                    <Text style={{ color: COLOR.SUB_TEXT }}>
                      {loginStore!.user !== null ? loginStore!.user.email : ''}
                    </Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                      <TouchableOpacity style={styles.button} onPress={this.gotoProfile}>
                        <Icon name='edit' size={20} color={COLOR.BLACK} />
                      </TouchableOpacity>
                      <TouchableOpacity style={[styles.button, { marginTop: 2 }]} onPress={this.onSignOut}>
                        <Icon name='sign-out-alt' size={20} color={COLOR.RED} />
                      </TouchableOpacity>
                    </View>
                  </View>
                  {
                    // EVENT.APPSTORE == 1 &&
                    <View style={{ alignItems: 'center', marginRight: 10 }}>
                      <LevelIcon size={40} score={userPredict.point} />
                      <TopIcon position={userPredict.position} size={30} containStyles={{ marginTop: 5 }} />
                    </View>
                  }
                </View>
                {
                  EVENT.APPSTORE == 1 &&
                  <View style={{ marginTop: 20, width: '100%', height: 80, borderBottomWidth: 10, borderBottomColor: COLOR.BG }}>
                    <View style={{ position: "absolute", height: 5, borderRadius: 3, backgroundColor: COLOR.BG_LOWLIGHT, width, left: 50 }} />
                    <View style={{ position: "absolute", height: 5, borderRadius: 3, backgroundColor: COLOR.BLUE, width: (userPredict.point??0) > 1000 ? width : ((userPredict.point??0) / 1000 * width), left: 50 }} />
                    <View style={{ position: "absolute", width: 20, height: 20, left: (20 / 1000 * width) + 40, top: 10 }}>
                      <Image style={{ width: 20, height: 20 }} source={require('assets/ic_amateur.png')} resizeMode='contain' />
                    </View>
                    <View style={{ position: "absolute", width: 20, height: 20, left: (160 / 1000 * width) + 40, top: 10 }}>
                      <Image style={{ width: 20, height: 20 }} source={require('assets/ic_semipro.png')} resizeMode='contain' />
                    </View>
                    <View style={{ position: "absolute", width: 20, height: 20, left: (440 / 1000 * width) + 40, top: 10 }}>
                      <Image style={{ width: 20, height: 20 }} source={require('assets/ic_profession.png')} resizeMode='contain' />
                    </View>
                    <View style={{ position: "absolute", width: 20, height: 20, left: width + 36, top: 10, padding: 3 }}>
                      <Image style={{ width: 20, height: 20 }} source={require('assets/ic_expert.png')} resizeMode='contain' />
                    </View>
                    <Text style={[styles.txtSub, { position: "absolute", left: (20 / 1000 * width) + 41, top: 35 }]} >20</Text>
                    <Text style={[styles.txtSub, { position: "absolute", left: (160 / 1000 * width) + 39, top: 35 }]}>160</Text>
                    <Text style={[styles.txtSub, { position: "absolute", left: (440 / 1000 * width) + 38, top: 35 }]} >440</Text>
                    <Text style={[styles.txtSub, { position: "absolute", left: width + 35, top: 35 }]} >1000</Text>
                  </View>
                }
                {
                  EVENT.APPSTORE == 1 &&
                  <View style={{ padding: 20, paddingHorizontal: 20, borderBottomWidth: 10, borderBottomColor: COLOR.BG }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                      <Text style={{ color: COLOR.PROFESSION, fontSize: 25, fontWeight: 'bold', flex: 1 }}>{userPredict.point} Point</Text>
                      <TouchableOpacity style={{ padding: 5 }} onPress={this.gotoPredictHistory}>
                        <Text style={{ fontSize: 16, color: COLOR.PROFESSION, flex: 1, textDecorationLine: 'underline' }}>{trans.lichSuDiem}</Text>
                      </TouchableOpacity>
                    </View>
                    {
                      EVENT.APPSTORE == 1 &&
                      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>

                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                          <Text style={[{
                            fontSize: 14, color: COLOR.BLACK, flex: 1, fontStyle: 'italic'
                          }]}>{trans.seeAds}</Text>
                          <TouchableOpacity style={{ padding: 5 }} onPress={this.seeAds}>
                            <Text style={{ fontSize: 16, color: COLOR.GREEN, flex: 1, textDecorationLine: 'underline' }}>{trans.seeAdsNow}</Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    }

                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                      <Text style={[{
                        fontSize: 16, color: COLOR.BLACK,
                        fontWeight: 'bold', flex: 1
                      }]}>{this.state.level}</Text>
                      <TouchableOpacity style={{ padding: 5 }} onPress={this.gotoTopUserPredict}>
                        <Text style={{ fontSize: 16, color: COLOR.BLUE, flex: 1, textDecorationLine: 'underline' }}>{trans.topChuyenGia}</Text>
                      </TouchableOpacity>
                    </View>
                    {/* <Text style={[styles.txtNormal, { marginTop: 5 }]}>{this.state.missing}</Text> */}
                  </View>
                }
                {
                  EVENT.APPSTORE == 1 &&
                  <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', padding: 20 }}>
                    <Text style={{ fontSize: 16, color: COLOR.BLACK, fontWeight: 'bold' }}>{`${trans.cacCapDo}:`}</Text>
                    <View style={[styles.viewLevel, { marginTop: 10 }]}>
                      <Image style={{ width: 20, height: 20 }} source={require('assets/ic_amateur.png')} resizeMode='contain' />
                      <Text style={[styles.txtNormal, { marginLeft: 10 }]}>{`${trans.nghiepDu} (20 ${trans.diem})`}</Text>
                    </View>
                    <View style={styles.viewLevel}>
                      <Image style={{ width: 20, height: 20 }} source={require('assets/ic_semipro.png')} resizeMode='contain' />
                      <Text style={[styles.txtNormal, { marginLeft: 10 }]}>{`${trans.banChuyen} (160 ${trans.diem})`}</Text>
                    </View>
                    <View style={styles.viewLevel}>
                      <Image style={{ width: 20, height: 20 }} source={require('assets/ic_profession.png')} resizeMode='contain' />
                      <Text style={[styles.txtNormal, { marginLeft: 10 }]}>{`${trans.chuyenNghiep} (440 ${trans.diem})`}</Text>
                    </View>
                    <View style={styles.viewLevel}>
                      <Image style={{ width: 20, height: 20 }} source={require('assets/ic_expert.png')} resizeMode='contain' />
                      <Text style={[styles.txtNormal, { marginLeft: 10 }]}>{`${trans.chuyenGia} (1000 ${trans.diem})`}</Text>
                    </View>
                    <View style={[styles.viewLevel, { marginTop: 10 }]}>
                      <Text style={[styles.txtNormal]}>{trans.duDoanDung1Tran}</Text>
                    </View>
                    <View style={[styles.viewLevel, { marginTop: 10 }]}>
                      <Text style={[styles.txtNormal]}>{trans.vaoKetQuaLichThiDau}</Text>
                    </View>
                    {
                      EVENT.APPSTORE == 1 &&
                      <TouchableOpacity style={{ padding: 5 }} onPress={() => NavigationService.navigate(SCREENS_NAME.PointHelp, {})}>
                        <Text style={{ fontSize: 16, color: COLOR.PROFESSION, flex: 1, textDecorationLine: 'underline' }}>Xem cách tính tỷ lệ</Text>
                      </TouchableOpacity>
                    }


                  </View>
                }
              </View>

            </ScrollView>
            {/* <AdmobBanner unitId={AdmobUnitId.banner_user} /> */}
          </View >

        </View>

      );
    } else {
      return (
        <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>
          <HeaderTranslucent title={trans.taiKhoan} onPressRight={this.goToInfo}
            hasTabs
            iconRight={(<Image style={{ width: 24, height: 24, tintColor: COLOR.WHITE }} source={require('assets/ic_drawer_info_1.png')} resizeMode='contain' />)} />
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: COLOR.BG, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden' }}>
            <GoogleSigninButton
              style={{}}
              size={GoogleSigninButton.Size.Standard}
              color={GoogleSigninButton.Color.Light}
              onPress={() => this.onSignIn()} />
            {
              Platform.OS === 'ios' && EVENT.APPSTORE == 0 &&
              <AppleButton
                buttonStyle={AppleButton.Style.WHITE_OUTLINE}
                buttonType={AppleButton.Type.SIGN_IN}
                style={{
                  width: 200,
                  height: 40,
                  marginTop: 20,
                }}
                onPress={() => this.onSignIn(true)}
              />

            }

          </View>

        </View>

      );
    }
  }
}

const styles = StyleSheet.create({
  button: {
    paddingHorizontal: 10,
    padding: 7,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  toProfile: { width: 80, height: 80, borderRadius: 40, justifyContent: 'center', alignItems: 'center', backgroundColor: COLOR.OVERLEY_DARK },
  txtName: { color: COLOR.BLACK, fontSize: 20 },
  imgProfile: { width: 74, height: 74, borderRadius: 37, backgroundColor: COLOR.WHITE },
  txtNormal: { color: COLOR.MAIN_TEXT, fontSize: 15 },
  txtSub: { color: COLOR.SUB_TEXT, fontSize: 13 },
  iconLogout: { fontSize: 20, color: COLOR.RED },
  iconEdit: { fontSize: 20, color: COLOR.BLACK },
  viewLevel: { flexDirection: 'row', alignItems: 'center', marginTop: 5 },
})