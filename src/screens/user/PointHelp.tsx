import { HeaderTranslucent } from 'components';
import { COLOR } from 'configs';
import React, { Component } from 'react';
import { StyleSheet, View, Text, ScrollView } from 'react-native';
// import {Test} from './Test'

type Props = { route: any }
export default class PointHelp extends Component<Props> {

  a1 = this.props.route.params.a1
  homeName = this.props.route.params.homeName
  awayName = this.props.route.params.awayName
  state = {
    bettingStr: '',
    bettingDes: '',
  }
  componentDidMount() {
    if (this.a1) {
      let betting = this.getBetting(this.a1, this.homeName, this.awayName)
      this.setState({ bettingStr: betting.bettingStr, bettingDes: betting.bettingDes })
    }
  }
  getBetting = (a1: string, homeName: string, awayName: string): { bettingStr: string, bettingDes: string } => {
    let betting: { bettingStr: string, bettingDes: string }
    if (a1.charAt(0) == '0' && a1.charAt(a1.length - 1) == '0') {
      let bettingStr = `Kèo đồng banh`
      let bettingDes = `- Nếu tỉ số hòa: hòa Point
- Nếu ${homeName} thắng: chọn ${homeName} ăn cả số Point cược, chọn ${awayName} mất cả số Point cược
- Nếu ${homeName} thua : chọn ${homeName} mất cả số Point cược, chọn ${awayName} ăn cả số Point cược`
      betting = { bettingStr, bettingDes }
    } else if (a1.charAt(0) == '0') {
      let oddDown = a1.split(":")[1].trim();
      let offset = 0;
      let oddTypeStr = "";
      if (oddDown.length == 3) {
        oddTypeStr = oddDown;
      } else if (oddDown.length > 3) {
        offset = Number(oddDown.split(" ")[0]);
        oddTypeStr = oddDown.split(" ")[1];
      } else {
        offset = Number(oddDown);
      }
      betting = this.getBettingStrFrom(oddTypeStr, offset, homeName, awayName)
    } else {
      let oddDown = a1.split(":")[0].trim();
      let offset = 0;
      let oddTypeStr = "";
      if (oddDown.length == 3) {
        oddTypeStr = oddDown;
      } else if (oddDown.length > 3) {
        offset = Number(oddDown.split(" ")[0]);
        oddTypeStr = oddDown.split(" ")[1];
      } else {
        offset = Number(oddDown);
      }
      betting = this.getBettingStrFrom(oddTypeStr, offset, awayName, homeName)
    }
    return betting;
  }

  getBettingStrFrom = (oddTypeStr: string, offset: number, homeName: string, awayName: string): { bettingStr: string, bettingDes: string } => {
    let bettingStr = ''
    let bettingDes = ''
    switch (oddTypeStr) {
      case "1/4":
        if (offset == 0) {
          bettingStr = `Kèo ${homeName} chấp 1/4`
          bettingDes = `- Nếu tỷ số hòa: chọn ${homeName} mất nửa số Point cược, chọn ${awayName} ăn nửa số Point cược
- Nếu ${homeName} thắng: chọn ${homeName} ăn cả số Point cược, chọn ${awayName} mất cả số Point cược
- Nếu ${homeName} thua: chọn ${homeName} mất cả sô Point cược, chọn ${awayName} ăn cả số Point cược`
        } else {
          bettingStr = `Kèo ${homeName} chấp ${offset} trái 1/4`
          bettingDes = `- Nếu ${homeName} thắng ${offset} trái: chọn ${homeName} mất nửa số Point cược, chọn ${awayName} ăn nửa số Point cược
- Nếu ${homeName} thắng nhiều hơn ${offset} trái: chọn ${homeName} ăn cả số Point cược, chọn ${awayName} mất cả số Point cược
- Nếu ${homeName} ${offset == 1 ? `hòa hoặc thua` : `thắng it hơn ${offset} trái`}: chọn ${homeName} mất cả số Point cược, chọn ${awayName} ăn cả số Point cược`
        }

        break;
      case "1/2":
        if (offset == 0) {
          bettingStr = `Kèo ${homeName} chấp 1/2`
          bettingDes = `- Nếu ${homeName} thắng: chọn ${homeName} ăn cả số Point cược, chọn ${awayName} mất cả số Point cược
- Nếu ${homeName} thua hoặc hòa: chọn ${homeName} mất cả số Point cược, chọn ${awayName} ăn cả số Point cược`
        } else {
          bettingStr = `Kèo ${homeName} chấp ${offset} trái 1/2`
          bettingDes = `- Nếu ${homeName} thắng nhiều hơn ${offset} trái: chọn ${homeName} ăn cả số Point cược, chọn ${awayName} mất cả số Point cược
- Nếu ${homeName} thắng it hơn  ${offset + 1} trái: chọn ${homeName} mất cả số Point cược, chọn ${awayName} ăn cả số Point cược`
        }
        break;
      case "3/4":
        if (offset == 0) {
          bettingStr = `Kèo ${homeName} chấp 3/4`
          bettingDes = `- Nếu ${homeName} thắng 1 trái: chọn ${homeName} ăn nửa số Point cược, chọn ${awayName} mất nửa số Point cược
- Nếu ${homeName} thắng nhiều hơn 1 trái: chọn ${homeName} ăn cả số Point cược, chọn ${awayName} mất cả số Point cược
- Nếu ${homeName} hòa hoặc thua: chọn ${homeName} mất cả số Point cược, chọn ${awayName} ăn cả số Point cược`
        } else {
          bettingStr = `Kèo ${homeName} chấp ${offset} trái 3/4`
          bettingDes = `- Nếu ${homeName} thắng ${offset + 1} trái: chọn ${homeName} ăn nửa số Point cược, chọn ${awayName} mất nửa số Point cược
- Nếu ${homeName} thắng nhiều hơn ${offset + 1} trái: chọn ${homeName} ăn cả số Point cược, chọn ${awayName} mất cả số Point cược
- Nếu ${homeName} thắng ít hơn ${offset + 1} trái: chọn ${homeName} mất cả số Point cược, chọn ${awayName} ăn cả số Point cược`
        }
        break;
      default:
        bettingStr = `Kèo ${homeName} chấp ${offset} trái`
        bettingDes = `- Nếu ${homeName} thắng ${offset} trái: hòa Point
- Nếu ${homeName} thắng nhiều hơn ${offset} trái: chọn ${homeName} ăn cả số Point cược, chọn ${awayName} mất cả số Point cược
- Nếu ${homeName} ${offset == 1 ? `hòa hoặc thua` : `thắng it hơn ${offset} trái`}: chọn ${homeName} mất cả số Point cược, chọn ${awayName} ăn cả số Point cược`
        break;
    }
    return { bettingStr, bettingDes }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.BG }}>
        <HeaderTranslucent title={'Cách tính tỷ lệ cược'} canBack />
        <ScrollView style={{ flex: 1, backgroundColor: COLOR.WHITE }}
          contentContainerStyle={{ paddingBottom: 20 }}>
          {
            this.a1 ?
              <View style={{ flex: 1, padding: 10 }}>
                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold', fontSize: 22 }]}>{this.homeName} vs {this.awayName}</Text>

                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold', fontSize: 22 }]}>{this.a1}</Text>

                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold', fontSize: 16 }]}>{this.state.bettingStr}</Text>
                <Text style={[styles.txtNormal, { marginTop: 5 }]}>{this.state.bettingDes}  </Text>

              </View>
              :
              <View style={{ flex: 1, padding: 10 }}>

                <Text style={[styles.txtNormal, { marginTop: 5 }]}>- Số "0" ở bên nào thì đội đấy là đội cửa trên (đội chấp)</Text>
                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold' }]}>- Kèo đồng banh   "0 - 0"</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu tỷ số hòa: hòa Point </Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Chọn đội nào thắng sẽ ăn cả số Point cược, thua sẽ mất cả số Point cược</Text>

                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold' }]}>- Kèo chấp n trái   "0 - n"  </Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng n trái: hòa Point </Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng hơn n trái: chọn cửa trên ăn cả số Point cược, ngược lại chọn cửa dưới mất cả số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng ít hơn n trái: chọn cửa trên mất cả số Point cược, ngược lại chọn cửa dưới ăn cả số Point cược</Text>

                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold' }]}>- Kèo chấp 1/4   "0 - 1/4" </Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu tỷ số hòa: chọn cửa trên mất nửa số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng: chọn cửa trên ăn cả số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thua: chọn cửa trên mất cả số Point cược</Text>

                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold' }]}>- Kèo chấp n trái 1/4   "0 - n 1/4" </Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng n trái: chọn cửa trên mất nửa số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng hơn n trái: chọn cửa trên ăn cả số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng ít hơn n trái: chọn cửa trên mất cả số Point cược</Text>

                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold' }]}>- Kèo chấp 1/2   "0 - 1/2" </Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu tỷ số hòa: chọn cửa trên mất cả số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng: chọn cửa trên ăn cả số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thua: chọn cửa trên mất cả số Point cược</Text>

                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold' }]}>- Kèo chấp n trái   1/2 "0 - n 1/2" </Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng n trái: chọn cửa trên mất cả số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng hơn n trái: chọn cửa trên ăn cả số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng ít hơn n trái: chọn cửa trên mất cả số Point cược</Text>

                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold' }]}>- Kèo chấp 3/4   "0 - 3/4" </Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng 1 trái: chọn cửa trên ăn nửa số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng hơn 1 trái: chọn cửa trên ăn cả số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên hòa hoặc thua: chọn cửa trên mất cả số Point cược</Text>

                <Text style={[styles.txtNormal, { marginTop: 5, fontWeight: 'bold' }]}>- Kèo chấp n 3/4   "0 - n 3/4" </Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng n + 1 trái: chọn cửa trên ăn nửa số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng hơn n + 1 trái: chọn cửa trên ăn cả số Point cược</Text>
                <Text style={[styles.txtNormal, { marginLeft: 5 }]}>+ Nếu cửa trên thắng ít hơn n + 1 trái: chọn cửa trên mất cả số Point cược</Text>
              </View>
          }

        </ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
  },
  txtNormal: { color: COLOR.MAIN_TEXT, fontSize: 15 },

});