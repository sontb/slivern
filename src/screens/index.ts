import Live from './live/Live';
import Match from './match/Match'
import Tips from './tips/Tips';
import TipsDetail from './tips/TipsDetail';
import League from './league/League'
import Chat from './chat/Chat'
import Splash from './splash/Splash'
import Info from './info/Info'
import User from './user/User'
import TopPredict from './topPredict/TopPredict'
import PlayerAndroid from './live/PlayerAndroid'
import LeagueTab from './league/LeagueTab'
import MatchDetail from './match/MatchDetail'
import { SCREENS_NAME } from './ScreenName'
import PredictHistory from './user/PredictHistory'
import PointHelp from './user/PointHelp'
import Policy from './info/Policy'
export default {
    Match,
    Live,
    PlayerAndroid,
    Tips,
    TipsDetail,
    League,
    Chat,
    Splash,
    Info,
    User,
    TopPredict,
    LeagueTab,
    MatchDetail,
    PredictHistory,
    PointHelp,
    Policy
}

export { SCREENS_NAME }


