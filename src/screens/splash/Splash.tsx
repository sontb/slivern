import { LiveApi } from 'api';
import { COLOR, EVENT } from 'configs';
import { FirebaseDbHelper, NavigationService, TYPE_NOTIFY } from 'helpers';
import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import { Image, Platform, StyleSheet, Text, View } from 'react-native';
import codePush from "react-native-code-push";
import { SCREENS_NAME } from 'screens/ScreenName';
import { ICommonStore } from 'stores/CommonStore';
import { trans } from 'trans';

type Props = { navigation?: any, commonStore: ICommonStore };
@inject('commonStore')
@observer
export default class Splash extends Component<Props> {

  state = {
    message: trans.dangKiemTraCapNhat,
  }
  liveApi = new LiveApi();

  didBlurSubscription: any;
  timeoutCodePush: any;
  didShowApp = false;

  componentDidMount = async () => {
    this.checkCodePushUpdateWithPercent();
    // request(PERMISSIONS.IOS.APP_TRACKING_TRANSPARENCY).then((result) => {
    // });
  }

  showApp = async () => {
    FirebaseDbHelper.handleNotifyAppClosed()
    this.props.commonStore.setLastTimeReload();
    this.didShowApp = true
  }

  checkCodePushUpdateWithPercent = () => {
    let installMode = codePush.InstallMode.IMMEDIATE;
    this.timeoutCodePush = setTimeout(() => {
      installMode = Platform.OS === 'ios' ? codePush.InstallMode.ON_NEXT_RESUME : codePush.InstallMode.ON_NEXT_RESTART;
      let syncOption: any = { updateDialog: false, installMode }
      codePush.sync(syncOption);
      this.showApp();
    }, 5000);
    codePush.allowRestart();
    let syncOption: any = { updateDialog: false, installMode }
    codePush.sync(syncOption, (status: any) => {
      switch (status) {
        case codePush.SyncStatus.CHECKING_FOR_UPDATE: !this.didShowApp && this.setState({ message: trans.dangKiemTraCapNhat });
          break;
        case codePush.SyncStatus.DOWNLOADING_PACKAGE: !this.didShowApp && this.setState({ message: trans.dangTaiVeBanCapNhat });
          break;
        case codePush.SyncStatus.INSTALLING_UPDATE: !this.didShowApp && this.setState({ message: trans.dangTaiVeBanCapNhat });
          break;
        case codePush.SyncStatus.UP_TO_DATE:
          !this.didShowApp && this.setState({ message: trans.daCapNhat });
          codePush.notifyAppReady();
          if (this.timeoutCodePush) clearTimeout(this.timeoutCodePush);
          if (!this.didShowApp) this.showApp();
          break;
        case codePush.SyncStatus.UNKNOWN_ERROR:
          if (this.timeoutCodePush) clearTimeout(this.timeoutCodePush);
          if (!this.didShowApp) this.showApp();
          break;
        case -1:
          if (this.timeoutCodePush) clearTimeout(this.timeoutCodePush);
          if (!this.didShowApp) this.showApp();
          break;
        default:

      }
    },
      ({ receivedBytes, totalBytes }: any) => {
        if (this.timeoutCodePush) clearTimeout(this.timeoutCodePush);
        // if (parseInt((receivedBytes * 100) / totalBytes, 10) === 100) {
        //   Platform.OS === 'android' && this.showRestartPrompt();
        // } else {
        !this.didShowApp &&
          this.setState({
            message: `Updating ${parseInt(`${(receivedBytes * 100) / totalBytes}`, 10)}%...`,
          });
        // }
      });
  }

  componentWillUnmount() {
    clearTimeout(this.timeoutCodePush)
  }

  render() {
    return (
      <View style={styles.container}>

        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Image style={{ width: 200, height: 200 }} source={require('assets/ic_iconapp.png')} resizeMode={'contain'} />
          <Text style={{ color: COLOR.BLACK, fontSize: 30, fontWeight: 'bold', marginTop: 10 }}> On 90 </Text>
          <Text style={{ color: COLOR.BLACK, fontSize: 20, marginTop: 10 }}> {trans.yeuBongDa} </Text>
          {
            EVENT.APPSTORE == 1 &&
            <Text style={{ color: COLOR.SUB_TEXT, fontSize: 13, marginTop: 10 }}> {this.state.message}</Text>

          }

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.WHITE,
  },
});