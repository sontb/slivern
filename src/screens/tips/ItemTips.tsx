import ItemComment from 'components/ItemComment';
import { COLOR, EVENT, GlobalStyles } from 'configs';
import moment from 'moment';
import React, { PureComponent } from 'react';
import { Image, StyleProp, StyleSheet, Text, TouchableOpacity, View, ViewStyle } from 'react-native';
import Icon5 from 'react-native-vector-icons/FontAwesome5';

//lightbulb-dollar
export interface IItemTips {
  id: number,
  href: string;
  title: string;
  img: string;
  desc: string;
  type: number;
  countComment?: number;
  emailComment?: string;
  nameComment?: string;
  contentComment?: string;
  mentionComment?: string;
  dateComment?: number;
  userPointComment?: number;
  photoComment?: string;
  positionComment?: number;
  date_create?: number;
}

type Props = {
  itemTips: IItemTips,
  onPress?: (itemTips: IItemTips) => void,
  onPressComment?: () => void,
  style?: StyleProp<ViewStyle>,
  showComment?: boolean,
  onPressNotifyNow?: (itemTips: IItemTips) => void,
  onPressDelete?: (itemTips: IItemTips) => void
};

export default class ItemOdds extends PureComponent<Props> {

  // shouldComponentUpdate(nextProps: Props, nextState: any) {

  //   return nextProps.itemTips.href !== this.props.itemTips.href;
  // }

  onPressComment = () => {
    this.props.onPressComment && this.props.onPressComment()
  }

  onPressItem = () => {
    let { itemTips, onPress } = this.props;
    onPress && onPress(itemTips)
  }

  render() {
    let { itemTips, onPress } = this.props;
    return (
      <View style={this.props.style} onStartShouldSetResponder={() => true}>
        <TouchableOpacity style={[{
          flexDirection: 'row',
          alignItems: 'center',
          backgroundColor: COLOR.WHITE,
          borderRadius: 10,
          marginHorizontal: 10,
          marginTop: 15,
        }, GlobalStyles.ShadowItemStyle]} onPress={this.onPressItem}>
          <Image style={{ width: 150, height: 95, borderTopLeftRadius: 10, borderBottomLeftRadius: 10, overflow: 'hidden' }} source={{ uri: itemTips.img }} resizeMethod='resize' />
          <View style={{ marginLeft: 10, flex: 1, marginRight: 5 }}>
            <Text style={{ fontSize: 14, fontWeight: '500' }} numberOfLines={2}>{itemTips.title}</Text>
            <Text style={{ fontSize: 14, marginTop: 3, color: COLOR.SUB_TEXT }} numberOfLines={1}>{itemTips.desc}</Text>
            {
              (itemTips.date_create ? true : false) &&
              <Text style={{ fontSize: 13, marginTop: 7, color: COLOR.GREY }}>{moment.unix(itemTips.date_create!).format('DD/MM HH:mm')}</Text>
            }
          </View>
          {
            (itemTips.type == 1) &&
            <View style={{
              width: 150, height: 95, position: 'absolute',
              backgroundColor: COLOR.OVERLEY_DARK, alignItems: 'center', justifyContent: 'center',
              borderTopLeftRadius: 10, borderBottomLeftRadius: 10
            }}>
              <Icon5 name='play' size={20} style={{ color: COLOR.OVERLEY_LIGHT_LIGHT }} />
            </View>
          }
          {
            this.props.showComment &&
            <View style={{ position: 'absolute', bottom: 0, right: 0 }}>
              <ButtonComment onPressComment={this.onPressComment} />
            </View>
          }

        </TouchableOpacity>
        {
          this.props.showComment && itemTips.countComment! > 0 &&
          <ItemComment
            style={{ marginHorizontal: 10, marginTop: 5, marginLeft: 20 }}
            itemComment={{
              id: 0,
              content: itemTips.contentComment!,
              name: itemTips.nameComment!,
              date_create: itemTips.dateComment!,
              email: itemTips.emailComment!,
              mention: itemTips.mentionComment!,
              photo: itemTips.photoComment!,
              point: itemTips.userPointComment!,
              position: itemTips.positionComment!
            }} onPressComment={this.onPressComment} />

        }
        {
          this.props.showComment && itemTips.countComment! > 1 &&
          <TouchableOpacity style={{
            borderRadius: 20, backgroundColor: COLOR.OVERLEY_LIGHT, marginLeft: 60,
            alignSelf: 'baseline', paddingHorizontal: 10, marginTop: 5, marginBottom: 0
          }} onPress={this.onPressComment}>
            <Text style={{
              fontSize: 30, marginTop: -15,
              color: COLOR.SUB_TEXT
            }}>...</Text>
          </TouchableOpacity>

        }
        {
          EVENT.DEV &&
          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>

            <TouchableOpacity style={{ padding: 10 }}
              onPress={() => this.props.onPressNotifyNow && this.props.onPressNotifyNow(itemTips)}>
              <Text style={{ color: COLOR.MAIN_TEXT }}>Push Now</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ padding: 10 }}
              onPress={() => this.props.onPressDelete && this.props.onPressDelete(itemTips)}>
              <Text style={{ color: COLOR.MAIN_TEXT }}>Delete</Text>
            </TouchableOpacity>
          </View>
        }

      </View>

    );
  }
}

const ButtonComment = ({ style, onPressComment }: { style?: StyleProp<ViewStyle>, onPressComment: () => void }) => {
  return (
    <TouchableOpacity style={[{
      paddingBottom: 5, paddingLeft: 30, paddingTop: 30, paddingRight: 10
    }, style]}
      onPress={onPressComment}>
      <Icon5 name="comment-alt" size={17} color={COLOR.SUB_TEXT} />

    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,

  },
});