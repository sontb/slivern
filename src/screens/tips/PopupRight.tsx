import { COLOR } from 'configs';
import React, { Component } from 'react';
import {
  FlatList, Image, StyleProp,
  StyleSheet, Text,
  TouchableOpacity,
  TouchableWithoutFeedback, View,
  ViewStyle
} from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon5 from 'react-native-vector-icons/FontAwesome5';


const ITEM_HEIGHT = 50
type Props = {
  style: StyleProp<ViewStyle>, selected?: number, onSelectedItem: (selected: number, item: any) => void,
  onCancel: () => void, listData?: Array<any>, title?: string,
  showPopupRight: boolean
};

export default class PopupRight extends Component<Props> {
  flatlist: any
  onSelectedItem = (selected: number, item: any) => {
    if (selected != this.props.selected) {
      this.props.onSelectedItem(selected, item)
    }
  }

  componentDidUpdate() {
    this.props.showPopupRight &&
      setTimeout(() => {
        this.flatlist.scrollToIndex({ index: this.props.selected, animated: true, viewOffset: 20 })
      }, 200)
  }

  _renderItem = ({ item, index }: any) => (
    <TouchableOpacity style={styles.containRow}
      onPress={() => this.onSelectedItem(index, item)}>
      <View style={styles.viewRow}>
        {
          (item.isPin ? true : false) ?
            // <Icon5 name='bahai' size={15} style={{ color: COLOR.PROFESSION, marginRight: 10 }} />
            <Image style={{ width: 15, height: 15, marginRight: 10 }} source={require('assets/ic_hot.png')} />
            :
            <View style={{ width: 15, marginRight: 10 }} />
        }

        <Text style={styles.textRow}>{item.name}</Text>
        {
          this.props.selected == index ?
            <Icon name='checkmark-outline' style={styles.icon} />
            :
            <View style={styles.empty} />
        }
      </View>
      <View style={styles.line} />
    </TouchableOpacity>
  )

  render() {
    return (
      <Modal
        style={{ margin: 0 }}
        isVisible={this.props.showPopupRight}
        swipeDirection='down'
        onSwipeComplete={this.props.onCancel}
        propagateSwipe
        onBackButtonPress={this.props.onCancel}
      >
        <View style={{ flex: 1 }}>
          <TouchableWithoutFeedback onPress={this.props.onCancel}>
            <View style={{ flex: 2, marginBottom: -15 }} />

          </TouchableWithoutFeedback>
          <View style={{ backgroundColor: COLOR.WHITE, flex: 1, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden' }}>
            {
              this.props.title &&
              <View style={{ paddingVertical: 15, borderBottomWidth: 1, borderBottomColor: COLOR.BORDER, alignItems: 'center' }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{this.props.title}</Text>
              </View>
            }
            <FlatList
              onStartShouldSetResponder={() => true}
              getItemLayout={(_, index) => ({
                length: ITEM_HEIGHT,
                offset: ITEM_HEIGHT * index,
                index,
              })}
              ref={(ref: FlatList) => { this.flatlist = ref }}
              contentContainerStyle={{ paddingBottom: 20, paddingTop: 0 }}
              data={this.props.listData}
              renderItem={this._renderItem}
              keyExtractor={(item: any, index) => `${index}`}
            />
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  containRow: { paddingTop: 0, alignItems: 'flex-start', height: ITEM_HEIGHT },
  viewRow: { flexDirection: 'row', marginHorizontal: 20, flex: 1, alignItems: 'center' },
  textRow: { color: COLOR.MAIN_TEXT, fontSize: 18, flex: 1 },
  icon: { color: COLOR.MAIN_TEXT, fontSize: 20, marginLeft: 10 },
  empty: { width: 20, height: 20 },
  line: { alignSelf: 'stretch', height: 1, backgroundColor: COLOR.OVERLEY_DARK, marginHorizontal: 10 },
})