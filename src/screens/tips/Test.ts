export const  Test = {
  html: `<html><head><style> img { display: block; max-width: 100%; height: auto !important; } .emb-bdp iframe { width:100%; height: 56vw; } .emb-bdp { width:100%; height: 56vw } h1{ font-size: 17px; font-weight: normal; } body { padding-bottom: 5em; padding-top: 5px; padding-left: 5px; padding-right: 5px}  </style><meta name="viewport" content="width=device-width, initial-scale=1.0"></head><body><h1>'Hậu duệ Thể Công' lần đầu tiên vào chung kết Cúp Quốc gia Chiến thắng 2-1 ở những phút cuối cùng trước Than.QN giúp Viettel lần đầu tiên góp mặt ở chung kết Cúp Quốc gia. Đây cũng là lần đầu tiên sau 11 năm, đội bóng nhà binh với tên gọi Viettel - hậu duệ của Thể Công mới đi đến trận đấu cuối cùng.</h1><div id="postContent" class="content"> 
  <p><strong>Bàn thắng</strong><br> <strong>Than.QN:</strong> Hai Long (87’)<br> <strong>Viettel:</strong> Bruno (54’), Tiến Dũng (90’+1)&nbsp;</p> 
  <p><strong>Thẻ đỏ&nbsp;</strong><br> <strong>Viettel:</strong> Khắc Ngọc<em> (90’+1),</em> Văn Hào <em>(90’+3)&nbsp;</em></p> 
  <h2>Bất ngờ từ Viettel&nbsp;</h2> 
  <p>Nếu như ở vòng tứ kết, Viettel ghi 3 bàn chỉ trong 5 phút ở hiệp 1 trước B.Bình Dương thì ở chuyến làm khách đến sân Cẩm Phả tại bán kết trước Than.QN, HLV Trương Việt Hoàng lại chủ động sắp xếp sơ đồ 4-4-2 với lối chơi thiên về phòng ngự. Cũng vì thế mà suốt 45 phút đầu tiên, đội chủ nhà Than.QN có nhiều cơ hội để khai thông bế tắc.&nbsp;</p> 
  <p>Điển hình nhất là 2 tình huống đều đến từ cái chân của tiền vệ sáng tạo Nguyễn Hai Long. Phút 27, Hai Long tung cú sút rất căng đưa bóng đập chân hậu vệ Viettel và đi chệch cầu môn. 8 phút sau, lại là Hai Long từ cánh phải thực hiện pha treo bóng vào vòng cấm vừa tầm cho ngoại binh Claudecir bang vào dứt điểm cận thành nhưng dứt điểm vọt xà. &nbsp;</p> 
  <div class="embedBox" data-clipath="https://bongdaplus.vn/video/than-quang-ninh-1-2-viettel-bk-cup-qg-2020-589222009.html" data-mdapath="" data-title="Than Quảng Ninh 1-2 Viettel (BK Cúp QG 2020)"> 
   <div class="embcap">
    Than Quảng Ninh 1-2 Viettel (BK Cúp QG 2020)
   </div> 
   <div class="emb-bdp rbox">
    <iframe allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen frameborder="0" scrolling="no" src="https://bongdaplus.vn/video-embed/589222009.html?autoplay=false"></iframe>
   </div> 
  </div> 
  <p>&nbsp;</p> 
  <p>Than.QN khởi đầu trận bán kết khá tốt. Nhưng họ lại là đội phải nhận bàn thua trước tiên. Ngay sau giờ nghỉ, ở phút 52, Bruno bên phía Viettel bị hai cầu thủ Than.QN phạm lỗi trong vòng cấm. Để rồi trên chấm phạt đền, tiền đạo chủ lực này của đội bóng áo lính lạnh lùng đánh bại thủ môn Huỳnh Tuấn Linh.&nbsp;</p> 
  <p>Choáng váng sang bàn thua, Than.QN dồn lên tấn công áp đảo. Phút 63, Hai Long thực hiện pha cứa lòng đưa bóng vọt xà ngang Viettel. Vài phút sau, lại là Hai Long thực hiện pha treo bóng cho Jermie Lynch sút bóng trúng cột dọc trong sự ngỡ ngàng của thủ môn Nguyên Mạnh.&nbsp;</p> 
  <h2>Hai Long hay nhưng Tiến Dũng mới là xuất sắc&nbsp;</h2> 
  <p>Nỗ lực tấn công của Than.QN sau cùng cũng được đền đáp. Và người tỏa sáng mang về bàn san bằng cách biệt cho Than.QN không ai khác chính là “sao trẻ” Hai Long. Phút 87, anh nhận được bóng ngoài vòng cấm và bất ngờ tung cú sút từ ngoài vòng 16m50 đưa bóng căng như kẻ chỉ về góc xa, khiến thủ môn Nguyên Mạnh dù bay hết cỡ vẫn không thể cứu thua cho Viettel.&nbsp;</p> 
  <p>Khi người ta đã nghĩ đến loạt sút luân lưu phân định thắng bại cho đội bên thì đẳng cấp ngôi sao một lần nữa lên tiếng. Trong lần tham gia tấn công hiếm hoi, trung vệ Bùi Tiến Dũng phá bẫy việt vị và đón đường chuyền của đồng đội trong vòng cấm. Ngay lập tức, anh thực hiện cú sục bóng loại bỏ thủ môn Tuấn Linh, mang về bàn thắng ấn định tỷ số quý như vàng cho Viettel, qua đó góp mặt ở trận chung kết.&nbsp;</p> 
  <p style="text-align:center"><img alt="Viettel thắng sát nút Than.QN - Ảnh: Phan Tùng" src="https://cdn.bongdaplus.vn/Assets/Media/2020/09/16/56/quang-ninh-2.jpg" style="height:500px; width:680px"></p> 
  <p>Đây là lần đầu tiên, Viettel góp mặt ở trận đấu cuối cùng của Cúp Quốc gia. Đây cũng là lần đầu tiên sau 11 năm, kể từ lần cuối cùng Thể Công giành huy chương Bạc, hậu duệ của đội bóng áo lính mới vào chung kết. Thậm chí nếu thi đấu tốt, thầy trò HLV Trương Việt Hoàng hoàn toàn có thể xóa nhòa cái bóng khổng lồ của Thể Công - đội vốn dĩ thua cả 3 lần trong trận chung kết. Thế nhưng để làm được điều đó, ông Việt Hoàng trước mắt phải giải quyết 2 trường hợp lĩnh thẻ đỏ ở bán kết dẫn đến bị treo giò. Đó là Hồ Khắc Ngọc và Dương Văn Hào. Cả hai cầu thủ này đều đã phải nhận những thẻ vàng thứ 2 không đáng có ở phút bù giờ của trận đấu.&nbsp;</p> 
  <p><strong>Đội hình thi đấu</strong><br> <strong>Than.QN: </strong>Tuấn Linh, Xuân Hùng (Văn Khoa 79’), Thanh Hào, Văn Việt, Trung Hiếu, Jermie Lynch, Trung Hiếu, Nhật Minh (Hoa Hùng 79’), Hai Long, Quách Tân (Hữu Khôi 80’), Cladecir (Hùng Cường 60’).&nbsp;<br> <strong>Viettel: </strong>Nguyên Mạnh, Ngọc Hải, Tiến Dũng, Caique (Minh Tuấn 63’), Trọng Hoàng, Ngọc Sơn (Đức Chiến 63’), Văn Trâm (Trọng Đại 30’), Khắc Ngọc, Hoàng Đức, Bruno, Duy Thường</p> 
  <p style="text-align:center"><img alt="" src="https://cdn.bongdaplus.vn/Assets/Media/2020/09/16/62/tk_than-qn-vs-viettel.jpg" style="height:757px; width:680px"></p> 
  <p style="text-align:center"><img alt="" src="https://cdn.bongdaplus.vn/Assets/Media/2020/09/16/62/kq-ltd-cup-quoc-gia-2020.jpg" style="height:420px; width:680px"></p> 
  <p><strong>XEM THÊM</strong></p> 
  <p></p> 
  <p></p> 
  </div> </body></html>"`
}