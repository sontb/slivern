export const getHtml = (title: string, desc: string, body: string) => {

  return `<html>
  <head>
  <style>
    img {
      display: block;
      max-width: 100%;
      height: auto !important;
    }

    .emb-bdp iframe {
      width: 100%;
      height: 56vw;
    }

    .emb-bdp {
      width: 100%;
      height: 56vw
    }

    .rbox video {
      width: 100%;
      height: 56vw;
    }

    .video {
      width: 100%;
      height: 56vw
    }

    h1 {
      font-size: 17px;
      font-weight: bold;
    }

    body {
      padding-bottom: 5em;
      padding-top: 5px;
      padding-left: 5px;
      padding-right: 5px
    }
  </style>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  </head>

  <body>
  <h1>${title}</h1>
  <dev>${desc}</dev>
  ${body} 
  </body>
</html>`
}