import { NewsApi } from 'api';
import {  HeaderTranslucent, Loading } from 'components';
import { IItemComment } from 'components/ItemComment';
import { AdmobUnitId, COLOR, EVENT } from 'configs';
import { TYPE_ADS } from 'configs/AdFanId';
import {  GlobalFuntion, NavigationService, TYPE_NOTIFY } from 'helpers';
import { inject, observer } from 'mobx-react';
// import { NativeAdsManager } from 'react-native-fbads';
// import * as FacebookAds from 'node_customs/react-native-fbads';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, FlatList, StyleSheet, View } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import Icon from 'react-native-vector-icons/Ionicons';
import PopupComment from 'screens/match/PopupComment';
import { SCREENS_NAME } from 'screens/ScreenName';
import { ICommonStore } from 'stores/CommonStore';
import { ILoginStore } from 'stores/LoginStore';
import { trans } from 'trans';
import ItemTips, { IItemTips } from './ItemTips';
import PopupRight from './PopupRight';
// import { InterstitialAdManager } from 'node_customs/react-native-fbads';

const listPopup = [
  { name: 'Xem nhiều nhất', id: 99 },
  { name: 'Mới nhất', id: 0 },
  { name: 'Euro 2021', id: 9, isPin: true },

  { name: 'Nhận định', id: 1 },
  { name: 'Người đẹp', id: 2, isPin: true },
  { name: 'Video', id: 3 },
  { name: 'Ngoại hạng Anh', id: 4 },
  { name: 'La Liga', id: 5 },
  { name: 'Ligue 1', id: 6 },
  { name: 'Serie A', id: 7 },
  { name: 'Bundesliga', id: 8 },

]
const TYPE_CATEGORY = 'TYPE_CATEGORY'
type Props = { navigation: any, loginStore: ILoginStore, commonStore: ICommonStore, route?: any }
@inject('loginStore', 'commonStore')
@observer
export default class Tips extends Component<Props> {

  state = {
    listTips: [],
    refreshing: false,
    loadingMore: false,
    loading: false,
    selectedCategoryId: 0,
    selectedCategoryPosition: this.props.loginStore.selectedPositionNewsCategory,
    showPopupCategory: false,
    selectedCategoryName: 'Mới nhất',
    listPopup: listPopup,
    showPopupComment: false,
    currentItemTipsComment: null,
    requestRefreshList: false,
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      ... this.state,
      selectedCategoryId: this.state.listPopup[this.state.selectedCategoryPosition].id,
      selectedCategoryName: this.state.listPopup[this.state.selectedCategoryPosition].name,
    }
  }

  tipsApi = new NewsApi();
  page = 0;
  canLoadMore = true;
  readyLoadMore = false;
  focusListener: any;
  adsManager: any;
  isFirstFullAds = true;
  timeoutShowAds: any;
  nativeAdView: any;
  nativeAdId = 0;
  flatList: FlatList<never> | undefined;
  commentListener: any;

  componentDidMount = async () => {

    this.getData();
    this.focusListener = this.props.navigation.addListener(
      'focus',
      () => {

        if (this.nativeAdView) {
          this.nativeAdView.refreshAdview()
        }
      }
    );

    this.commentListener = EventRegister.addEventListener(EVENT.COMMENT_NEWS, this.onRefreshComment);
    this.handleNotify()
  }

  handleNotify = () => {
    try {
      let notiData = this.props.route.params?.notiData
      if (notiData) {
        let itemTips: IItemTips = JSON.parse(notiData?.eData)
        if(notiData.type == TYPE_NOTIFY.TipsComment){
          this.onPressComment(itemTips, true)
        } else {
          this.onSelectItem(itemTips)
        }
      }
    } catch (err) {
      console.log(err)
    }

  }

  onRefreshComment = () => {
    this.setState({ listTips: this.state.listTips })

  }

  onNewComment = (requestRefreshList: boolean) => {
    try {
      if(requestRefreshList){
        this._onRefresh()
      } else {
        this.setState({ listTips: this.state.listTips })
      }
    } catch {

    }
  }

  handleBackButtonClick = () => {
    if (this.state.showPopupCategory) {
      this.setState({ showPopupCategory: false })
      return true
    }
    return false
  }

  componentWillUnmount() {
    this.tipsApi.cancel();
    if (this.focusListener) {
      this.focusListener();
    }
    // IronSourceInterstitials.removeEventListener('interstitialDidLoad', this.showInterstitial);

    clearTimeout(this.timeoutShowAds);
    EventRegister.removeEventListener(this.commentListener);

  }

  onSelectItem = (itemTips: IItemTips) => {
    let random = Math.floor(Math.random() * 3);
    if ((this.isFirstFullAds || random == 1) && EVENT.SHOW_ADS) {
      // InterstitialAdManager.showAd(AdFanId.full_tips)
      // IronSourceHelper.showInterstitial()
      // IronSourceInterstitials.loadInterstitial();
      // IronSourceInterstitials.addEventListener('interstitialDidLoad', this.showInterstitial);
      // AdmobHelper.interstitial(AdmobUnitId.full_tips)
      this.isFirstFullAds = false

    }
    NavigationService.navigate(SCREENS_NAME.TipsDetail, { itemTips });

  }

  // showInterstitial = () => {
  //   IronSourceInterstitials.showInterstitial();

  // }

  getData = async () => {
    try {
      if (this.page == 0) {
        if (!this.state.refreshing) {
          this.setState({ loading: true });
        }
      } else {
        this.setState({ loadingMore: true })
      }
      let listTips: any = await this.tipsApi.getListNews(0, this.page, 10);
      if (listTips) {
        if (this.page == 0) {
          if (EVENT.SHOW_ADS && listTips.length > 0) {
            listTips.splice(2, 0, { href: TYPE_ADS + this.nativeAdId, viewType: TYPE_ADS });
          }
          this.setState({ listTips, refreshing: false, loading: false });
        } else {
          let listPage = this.state.listTips.concat(listTips);
          this.setState({ listTips: listPage, loadingMore: false });
        }
        if (listTips.length == 0) {
          this.canLoadMore = false;
        }
      } else {
        this.canLoadMore = false;
        this.setState({ loadingMore: false, refreshing: false, loading: false });
      }
    } catch (err) {
      console.log(err);
      this.setState({ refreshing: false, loadingMore: false, loading: false });
    }
  }

  onAdFailedToLoad = () => {
    this.nativeAdId++
  }

  onDelete = (itemTips: IItemTips, position: number) => {
    Alert.alert('Thông báo', 'Xóa ' + itemTips.title, [{
      text: 'Hủy',
      onPress: () => console.log('Cancel Pressed'),
      style: 'cancel',
    },
    {
      text: 'Ok',
      onPress: async () => {
        this.setState({ loading: true })
        try {
          let result = await this.tipsApi.delete(itemTips.id);
          if (result) {
            let listTips = this.state.listTips
            listTips.splice(position, 1);
            this.setState({ listTips })
          }
        } catch {

        }
        this.setState({ loading: false })
      },
    }])
  }

  onNotify = async (itemTips: IItemTips) => {
    this.setState({ loading: true })
    let tipNotifi = {
      id: itemTips.id,
      href: itemTips.href,
      title: itemTips.title,
      desc: itemTips.desc,
    }
    try {
      let result = await this.tipsApi.notifyNow(itemTips.title, tipNotifi);
    } catch {

    }
    this.setState({ loading: false })
  }

  _renderItem = ({ item, index }: any) => {
    if (item.viewType === TYPE_ADS) {
      // return <AdsFanNative ref={(ref) => { this.nativeAdView = ref }} style={{ marginTop: 10 }} placementId={AdFanId.native_tips} showAds={true} />
      // return null
      // return <AdmobNative style={{ marginTop: 15 }} adUnitID={AdmobUnitId.native_tips} showAds={true}
      //   onAdFailedToLoad={this.onAdFailedToLoad} />
      return null
    } else {
      if (EVENT.APPSTORE == 0 && item.title.toLocaleLowerCase().includes("covid")) {
        return null
      } else {
        return (
          <ItemTips itemTips={item} onPress={() => this.onSelectItem(item, index)} showComment
            onPressComment={() => this.onPressComment(item)}
            onPressDelete={(itemTips) => this.onDelete(itemTips, index)}
            onPressNotifyNow={this.onNotify} />
        )
      }
    }
  }

  _onRefresh = async () => {
    this.refreshState();
    await this.setState({ refreshing: true })
    this.getData();
  }

  refreshState = () => {
    this.page = 0;
    this.canLoadMore = true;
    // this.readyLoadMore = false;
    // await this.setState({ refreshing: false, loadingMore: false });
  }

  _loadMore = () => {
    if (this.canLoadMore && this.readyLoadMore) {
      this.page = this.page + 1;
      this.getData();
    }
  }

  _onMomentumScrollBegin = () => {
    this.readyLoadMore = true;
  }

  _renderFooter = () => {
    if (!this.state.loadingMore) {
      return null;
    }
    return (<ActivityIndicator
      color={COLOR.PRIMARY_COLOR}
      animating
      size='large'
    />);
  }

  goToChat = async () => {
    GlobalFuntion.requestLogin(this.props.loginStore, () => {
      NavigationService.navigate(SCREENS_NAME.Chat, null);
    })
  }

  onSelectedItemPopup = async (selectedPosition: number, item: any) => {
    await this.setState({
      showPopupCategory: false, selectedCategoryPosition: selectedPosition, selectedCategoryName: item.name,
      selectedCategoryId: item.id
    })
    this.refreshState()
    await this.getData()
    this.flatList!.scrollToIndex({ animated: true, index: 0 })
    this.props.loginStore.setSelectedPositionNewsCategory(selectedPosition)
  }



  onPressComment = (itemMatch: IItemTips, requestRefreshList?: boolean) => {
    GlobalFuntion.requestLogin(this.props.loginStore!, () => {
      this.setState({ showPopupComment: true, currentItemTipsComment: itemMatch, requestRefreshList })
    })
    // this.flatlist.scrollToIndex({ animated: true, index , viewOffset: 50})
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>
        {
          EVENT.APPSTORE == 1 ?
            <HeaderTranslucent
              title={trans.nhanDinhBongDa}
              showPopupRight
              textRight={this.state.selectedCategoryName}
              onShowPopup={() => this.setState({ showPopupCategory: !this.state.showPopupCategory })}
              hasTabs
            />
            :
            <HeaderTranslucent title={trans.nhanDinhBongDa} />
        }


        <View style={{ flex: 1, backgroundColor: COLOR.BG, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden' }}>
          <FlatList
            ref={(ref) => this.flatList = ref!}
            contentContainerStyle={{ paddingBottom: 50 }}
            data={this.state.listTips}
            renderItem={this._renderItem}
            // extraData={this.state.listTips}
            keyExtractor={(item: any, index) => `${item.href}`}
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            onEndReachedThreshold={0.01}
            onEndReached={() => this._loadMore()}
            onMomentumScrollBegin={this._onMomentumScrollBegin}
            ListFooterComponent={this._renderFooter}
            removeClippedSubviews
          />
        </View>
        {/* <AdmobBanner unitId={AdmobUnitId.banner_tips} /> */}
        <Loading loading={this.state.loading} />

        <PopupRight
          listData={this.state.listPopup}
          showPopupRight={this.state.showPopupCategory}
          style={{ position: 'absolute', bottom: 0, left: 0, right: 0 }}
          selected={this.state.selectedCategoryPosition}
          onSelectedItem={this.onSelectedItemPopup}
          onCancel={() => this.setState({ showPopupCategory: false })}
          title={trans.choiceCategory}
        />
        {
          this.state.showPopupComment &&
          <PopupComment
            requestRefreshList={this.state.requestRefreshList}
            itemTips={this.state.currentItemTipsComment!}
            showPopupComment={this.state.showPopupComment}
            onCancel={() => this.setState({ showPopupComment: false })}
            onComment={this.onNewComment}
            type={1} />
        }

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
  },
});