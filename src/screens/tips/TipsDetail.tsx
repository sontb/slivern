import { NewsApi, TipsDetailApi, UserPredictApi } from 'api';
import { HeaderTranslucent, Loading } from 'components';
import ItemComment, { IItemComment } from 'components/ItemComment';
import { COLOR, EVENT } from 'configs';
import { GlobalFuntion } from 'helpers';
import { inject, observer } from 'mobx-react';
import React, { Component } from 'react';
import { Dimensions, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';
import PopupComment from 'screens/match/PopupComment';
import { ILoginStore } from 'stores/LoginStore';
import { getHtml } from './HtmlTemp';
import { IItemTips } from './ItemTips';
import Icon5 from 'react-native-vector-icons/FontAwesome5';
import { EventRegister } from 'react-native-event-listeners';
import AutoHeightWebView from 'react-native-autoheight-webview'

// import {Test} from './Test'
type Props = { navigation?: any, route?: any, loginStore?: ILoginStore };

@inject('loginStore')
@observer
export default class TipsDetail extends Component<Props> {
  itemTips: IItemTips = this.props.route.params.itemTips
  webView: WebView
  state = {
    html: '',
    loading: false,
    itemTips: this.itemTips,
    showPopupComment: false,
    webViewHeight: 0,
    refreshing: false,
  }

  constructor(props: any) {
    super(props)
  }

  tipDetailApi = new TipsDetailApi();
  userPredictApi = new UserPredictApi()

  adsManager: any;
  tipsApi = new NewsApi();

  componentDidMount = async () => {
    this.getData();
    this.getDataComment();
    this.readNews();
  }

  componentWillUnmount() {
    this.tipDetailApi.cancel();
  }

  getData = async () => {
    this.setState({ loading: true })
    try {
      let href = this.itemTips.href;
      let title = this.itemTips.title;
      let desc = this.itemTips.desc;
      let tipsDetail = await this.tipDetailApi.getData(href);
      let html = getHtml(title, desc, tipsDetail.detail)
      this.setState({ html });
      // this.setState({ loading: false })
    } catch (err) {
      console.log(err);
      // this.setState({ loading: false });
    }
  }

  getDataComment = async () => {
    try {
      let listData = await this.userPredictApi.userListComment(this.itemTips.id, 0, 1000, 1)
      let itemTips = this.state.itemTips
      if (listData) {
        if (listData.length != itemTips.countComment) {
          itemTips.countComment = listData.length
          let itemComment: IItemComment = listData[0];
          itemTips.dateComment = itemComment.date_create
          itemTips.nameComment = itemComment.name
          itemTips.emailComment = itemComment.email
          itemTips.photoComment = itemComment.photo
          itemTips.mentionComment = itemComment.mention
          itemTips.contentComment = itemComment.content
          itemTips.userPointComment = itemComment.point
          itemTips.positionComment = itemComment.position
          this.setState({ itemTips })
          EventRegister.emit(EVENT.COMMENT_NEWS, itemComment);

        }
      }

    } catch {

    }

  }

  readNews = () => {
    try {
      this.tipsApi.readNews(this.itemTips.href)
    } catch {

    }
  }

  onPressComment = () => {
    GlobalFuntion.requestLogin(this.props.loginStore!, () => {
      this.setState({ showPopupComment: true })
    })
    // this.flatlist.scrollToIndex({ animated: true, index , viewOffset: 50})
  }

  onSummitComment = (itemComment: IItemComment) => {
    try {
      let itemMatch: IItemTips = this.state.itemTips!
      itemMatch.countComment = itemMatch.countComment! + 1
      itemMatch.dateComment = itemComment.date_create
      itemMatch.nameComment = itemComment.name
      itemMatch.emailComment = itemComment.email
      itemMatch.photoComment = itemComment.photo
      itemMatch.mentionComment = itemComment.mention
      itemMatch.contentComment = itemComment.content
      itemMatch.userPointComment = itemComment.point
      itemMatch.positionComment = itemComment.position
      this.setState({ itemTips: itemMatch })
      EventRegister.emit(EVENT.COMMENT_NEWS, itemComment);

    } catch {

    }
  }

  onMessage = (event: any) => {
    let height = Number(event.nativeEvent.data)
    let screenHeight = Dimensions.get('window').height
    this.setState({ webViewHeight: Number(event.nativeEvent.data) })
  }

  injectedJavaScript = `
  window.ReactNativeWebView.postMessage(
    Math.max(document.body.offsetHeight, window.innerHeight)
  );`

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.getDataComment()
    this.setState({ refreshing: false })
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.WHITE }}>
        <HeaderTranslucent title={'Tin tức'} canBack
          iconRight={(<Icon5 name="comment-alt" size={20} color={COLOR.WHITE} />
          )}
          onPressRight={this.onPressComment}
        />

        {/* <View style={{ flex: 1, backgroundColor: COLOR.WHITE }} > */}
        <ScrollView style={{
          flex: 1,
          backgroundColor: COLOR.WHITE
        }}
          contentContainerStyle={{
          }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>
          {
            this.state.html !== '' &&
            // <WebView
            //   ref={ref => this.webView = ref}
            //   style={{ height: this.state.webViewHeight, opacity: 0.99 }}
            //   source={{ html: this.state.html }}
            //   onLoadStart={() => {
            //     this.setState({ loading: true });
            //   }}
            //   allowsFullscreenVideo={true}
            //   onLoad={() => {
            //     this.setState({ loading: false });
            //   }}
            //   scrollEnabled={false}
            //   onMessage={this.onMessage}
            //   injectedJavaScript={this.injectedJavaScript}
            // />
            <AutoHeightWebView
              style={{ width: Dimensions.get('window').width - 20, marginLeft: 10, marginTop: 10, opacity: 0.99 }}

              // pointerEvents="none"
              source={{ html: this.state.html }}
              onLoadStart={() => {
                this.setState({ loading: true });
              }}
              onLoad={() => {
                this.setState({ loading: false });
              }}
              scalesPageToFit={true}

              allowsFullscreenVideo={true}
              scrollEnabled={false}
              viewportContent={'width=device-width, user-scalable=no'}

            />
          }
        </ScrollView>
        <View style={{ backgroundColor: COLOR.BG }}>

          {
            this.state.itemTips.countComment! > 0 &&
            <ItemComment
              style={{ marginHorizontal: 10, marginTop: 10 }}
              itemComment={{
                id: 0,
                content: this.state.itemTips.contentComment!,
                name: this.state.itemTips.nameComment!,
                date_create: this.state.itemTips.dateComment!,
                email: this.state.itemTips.emailComment!,
                mention: this.state.itemTips.mentionComment!,
                photo: this.state.itemTips.photoComment!,
                point: this.state.itemTips.userPointComment!,
                position: this.state.itemTips.positionComment!
              }} onPressComment={this.onPressComment} />

          }
          {
            this.state.itemTips.countComment! > 1 &&
            <View style={{
              borderRadius: 20, backgroundColor: COLOR.OVERLEY_LIGHT, marginLeft: 50,
              alignSelf: 'baseline', paddingHorizontal: 10, marginTop: 5, marginBottom: 10
            }}>
              <Text style={{
                fontSize: 30, marginTop: -15,
                color: COLOR.SUB_TEXT
              }}>...</Text>
            </View>
          }
          {
            this.state.itemTips.countComment == 1 &&
            <View style={{ height: 10 }} />
          }
        </View>
        {
          this.state.showPopupComment &&
          <PopupComment
            hideItem
            itemTips={this.state.itemTips}
            showPopupComment={this.state.showPopupComment}
            onCancel={() => this.setState({ showPopupComment: false })}
            onComment={this.onSummitComment}
            type={1} />
        }

        <Loading loading={this.state.loading} />
        {/* <AdmobBanner unitId={AdmobUnitId.banner_tips_detail} /> */}
        {/* <FacebookAds.BannerAd
          style={{ paddingBottom: -20 }}
          placementId={AdFanId.banner_league_detail}
          type="standard"

        /> */}
        {/* <AdComponentSmall adsManager={this.adsManager} /> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
  },
});