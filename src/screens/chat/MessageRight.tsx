import React, { Component, PureComponent } from 'react';
import { View, StyleSheet, Text, Image, Animated, TouchableWithoutFeedback } from 'react-native';
import { COLOR } from 'configs';
import Message from 'models/Message';
import moment from 'moment';
import { LevelIcon, TopIcon } from 'components';
import { GlobalFuntion } from 'helpers';

type Props = { itemMessage: Message, admin: boolean, animation: boolean };

export default class MessageRight extends PureComponent<Props> {
  state = {
    scaleValue: new Animated.Value(0)
  }
  componentDidMount() {
    // if (this.props.animation) {
    //   Animated.timing(this.state.scaleValue, {
    //     toValue: 1,
    //     duration: 600,
    //     delay: 350
    //   }).start();
    // }
  }

  render() {
    let { itemMessage, admin } = this.props;
    return (

        <View style={[styles.container]}>
          {
            this.props.admin ?
              <Text style={{ fontSize: 15, fontWeight: 'bold', color: COLOR.RED, marginRight: 10 }}>Admin</Text>
              :
              <View style={styles.viewName}>
                <Text style={{ fontSize: 15, fontWeight: 'bold', color: GlobalFuntion.getLevelColor(itemMessage.score), marginRight: 10 }}>{itemMessage.name}</Text>
                <TopIcon position={itemMessage.position} size={20} containStyles={{ marginRight: 5 }} />
                <LevelIcon score={itemMessage.score} size={23} containStyles={{ marginRight: 6 }} />
              </View>
          }
          <View style={{
            padding: 10, marginLeft: 20,
            borderRadius: 20, backgroundColor: COLOR.WHITE,
            marginTop: 5, marginBottom: 5,
          }}>
            <Text style={{ textAlign: 'right' }}>{itemMessage.text}</Text>
          </View>
          <Text style={{ fontSize: 13, color: COLOR.SUB_TEXT, textAlign: 'right' }}>{moment(itemMessage.timestamp).format('HH:mm DD/MM')}</Text>
        </View>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5, paddingLeft: 10, paddingRight: 10,
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
  viewName: { flexDirection: 'row', alignItems: 'center', flex: 1, justifyContent: 'flex-end' },
});