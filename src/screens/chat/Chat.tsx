import { HeaderTranslucent, Loading } from 'components';
import { COLOR } from 'configs';
// import { Header } from 'react-navigation';
// import { NavigationService } from 'helpers'
// import { SCREENS, SCREENS_NAME } from 'screens';
// import SvgUri from 'react-native-svg-uri';
import { inject, observer } from 'mobx-react';
import Message from 'models/message';
import React, { Component } from 'react';
import {
  Dimensions, FlatList,
  KeyboardAvoidingView, NativeModules, Platform,
  StatusBarIOS, StyleSheet,
  TextInput, TouchableOpacity, View
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { IChatStore } from 'stores/ChatStore';
import { ILoginStore } from 'stores/LoginStore';
import { trans } from 'trans';
import MessageLeft from './MessageLeft';
import MessageRight from './MessageRight';
// import { inject, observer } from 'mobx-react';
// import { ICommonStore } from 'stores/CommonStore';

const offset = Dimensions.get('window').width * 9 / 16 + 20;
const { StatusBarManager } = NativeModules;

type Props = { navigation?: any, chatStore?: IChatStore, loginStore?: ILoginStore, hideHeader?: boolean, channelId?: string };
@inject('chatStore', 'loginStore')
@observer
export default class Chat extends Component<Props> {

  state = {
    loading: false,
    text: '',
    offset: 0,
    statusBarHeight: 0,
  }

  newMessageListener = null;
  flatList: FlatList| null = null;
  autoScrollBottom = true;
  animation = false;
  statusBarListener: any
  componentDidMount() {
    this.setState({ loading: true })
    this.props.chatStore!.goOnline();
    this.props.chatStore!.setOnStartChatListener(this.props.channelId, this.onFinishStartChat);
    if (Platform.OS === 'ios') {

      StatusBarManager.getHeight((statusBarFrameData: any) => {
        this.setState({ statusBarHeight: statusBarFrameData.height });
        console.log('statusBarFrameData.height', statusBarFrameData.height);
      });
      this.statusBarListener = StatusBarIOS.addListener('statusBarFrameWillChange', (statusBarData) => {
        this.setState({ statusBarHeight: statusBarData.frame.height });
        console.log('statusBarData.frame.height', statusBarData.frame.height);
      });
    }
    this.props.chatStore!.setOnMessageListner(this.onReceiveMessage);
  }

  onFinishStartChat = () => {
    this.setState({ loading: false })
  }

  onReceiveMessage = () => {
    this.animation = true;
    if (this.autoScrollBottom) {
      if (this.flatList) {
        this.flatList.scrollToOffset({ offset: 0, animated: true })
      }
    }
  }

  componentWillUnmount() {
    this.props.chatStore!.goOffline();
    this.props.chatStore!.stopChat();
    this.props.chatStore!.removeOnMessageListener();
    if (this.statusBarListener) {
      this.statusBarListener.remove();
    }

  }

  _renderItem = ({ item, index }: any) => (
    this.props.loginStore!.user.uid === item.id ?
      <MessageRight itemMessage={item} admin={this.props.chatStore!.adminData[item.id] === 1} key={item.key} animation={false} />
      :
      <MessageLeft itemMessage={item} admin={this.props.chatStore!.adminData[item.id] === 1} key={item.key} animation={false} />
  );

  onSendMessage = async () => {
    let { chatStore, loginStore } = this.props;
    this.autoScrollBottom = true;
    if (this.state.text !== '') {

      // Alert.alert('message', this.state.text)
      let message: Message = {
        id: loginStore!.user.uid,
        text: this.state.text,
        name: loginStore!.user.displayName,
        photo: loginStore!.user.photo,
        score: loginStore!.userPredict.point,
        position: loginStore!.userPredict.position,
      }
      chatStore!.sendMessage(message);
      await this.setState({ text: '' })
    }
  }

  render() {
    let { chatStore } = this.props;
    return (
      // <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : undefined} enabled
      //   keyboardVerticalOffset={0}>
      <KeyboardAvoidingView style={{ flex: 1 }} behavior={Platform.OS === 'ios' ? 'padding' : undefined} keyboardVerticalOffset={this.props.hideHeader ? offset + this.state.statusBarHeight - 20 : 0}>

        <View style={{ backgroundColor: COLOR.BG, flex: 1 }}>
          {
            !this.props.hideHeader &&
            <HeaderTranslucent title={trans.thaoLuan} canBack />
          }
          <FlatList
            // style={{ backgroundColor: COLOR.RED }}
            inverted
            ref={(ref: any) => {
              this.flatList = ref;
            }}
            data={chatStore!.listMessage.slice()}
            extraData={chatStore!.adminData}
            renderItem={this._renderItem}
            onScroll={(e) => {
              if (e.nativeEvent.contentOffset.y <= 100) {
                // make something...
                this.autoScrollBottom = true;
              } else {
                this.autoScrollBottom = false;
              }
            }}
            keyExtractor={(item: Message, index) => `${item.key}`}
            maxToRenderPerBatch={20}
            keyboardShouldPersistTaps="handled"
          />

          <View style={{ flexDirection: 'row', backgroundColor: COLOR.BG_GREY, padding: 10, paddingBottom: this.props.hideHeader ? 10 : 20, alignItems: 'center' }}>
            <View style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center', alignItems: 'center',
              backgroundColor: COLOR.WHITE,
              borderRadius: 10,
              paddingVertical: Platform.OS === 'ios' ? 10 : 0,
              paddingHorizontal: 10,
            }}>
              <TextInput
                style={{ flex: 1 }}
                selectionColor={COLOR.MAIN}
                placeholder={trans.noiDung}
                returnKeyType='send'
                onSubmitEditing={this.onSendMessage}
                multiline={true}
                onChangeText={(text) => this.setState({ text })}
                value={this.state.text}
                blurOnSubmit={true}
                maxLength={100}
                // numberOfLines={3}
                placeholderTextColor={COLOR.SUB_TEXT}
                underlineColorAndroid="transparent" />
            </View>
            <TouchableOpacity style={{ padding: 10 }} onPress={this.onSendMessage}>
              <Icon name='md-send' style={{ fontSize: 25, color: COLOR.MAIN }} />
            </TouchableOpacity>
          </View>

          <Loading loading={this.state.loading} />
        </View>
      </KeyboardAvoidingView>

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.BG,
    flexDirection: 'column',
  },
});