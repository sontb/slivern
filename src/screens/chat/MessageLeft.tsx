import React, { Component, PureComponent } from 'react';
import { View, StyleSheet, Text, Image, Animated, Alert, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';
import { COLOR } from 'configs';
import Message from 'models/Message';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';
// import SvgUri from 'react-native-svg-uri';
import { Avatar, LevelIcon, TopIcon } from 'components';
import { GlobalFuntion } from 'helpers';

type Props = { itemMessage: Message, admin: boolean, animation: boolean };

export default class MessageLeft extends PureComponent<Props> {
  state = {
    scaleValue: new Animated.Value(0),
    report: false,
    hidden: false,
  }
  componentDidMount() {
    // if (this.props.animation) {
    //   Animated.timing(this.state.scaleValue, {
    //     toValue: 1,
    //     duration: 600,
    //     delay: 350
    //   }).start();
    // }
  }
  onReport = () => {
    Alert.alert(
      'Báo vi phạm',
      `Bạn muốn ẩn hoặc gắn cờ nội dung này?`,
      [
        {
          text: 'Hủy',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'Ẩn nội dung này', onPress: () => this.setState({ hidden: true }),
        },
        {
          text: 'Gắn cờ', onPress: () => this.setState({ report: true }),
          style: 'destructive'
        },
      ],
      { cancelable: false },
    );
  }



  render() {
    let { itemMessage, admin } = this.props;
    return (

        <View style={[styles.container]}>
          {
            admin ?
              <Text style={{ fontSize: 15, fontWeight: 'bold', color: COLOR.RED, marginLeft: 39 }}>Admin</Text>
              :
              <View style={styles.viewName}>
                <LevelIcon score={itemMessage.score} size={23} containStyles={{ marginLeft: 6 }} notHide={true} />
                <TopIcon position={itemMessage.position} size={20} containStyles={{ marginLeft: 5 }} />
                <Text style={{ fontSize: 15, fontWeight: 'bold', color: GlobalFuntion.getLevelColor(itemMessage.score), marginLeft: 10 }}>{`${itemMessage.name} ${GlobalFuntion.getLevelName(itemMessage.score)}`}</Text>
              </View>
          }

          {
            !this.state.hidden &&
            <View style={{
              flexDirection: 'row',
              alignItems: 'flex-end',
              width: '100%',
              paddingRight: 20,
            }}>
              <Avatar size={33} photo={itemMessage.photo} style={{ marginBottom: 5 }} />
              <View style={{
                padding: 10, marginRight: 10,
                borderRadius: 20, backgroundColor: COLOR.MAIN,
                marginTop: 5, marginBottom: 5, marginLeft: 5,
              }}>
                <Text style={{ color: COLOR.WHITE }}>{itemMessage.text}</Text>
              </View>
              <TouchableOpacity style={{ marginBottom: 13 }} onPress={this.onReport}>
                {
                  this.state.report ?
                    <Icon name='flag' size={17} color={COLOR.BLUE} />
                    :
                    <Icon name='flag' size={17} color={COLOR.GREY} />
                }
              </TouchableOpacity>
            </View>
          }
          {
            !this.state.hidden &&
            <Text style={{ fontSize: 13, color: COLOR.SUB_TEXT, marginLeft: 45 }}>{moment(itemMessage.timestamp).format('HH:mm DD/MM')}</Text>
          }
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5, paddingLeft: 10, paddingRight: 20,
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  viewName: { flexDirection: 'row', alignItems: 'center', flex: 1 },
  txtName: { color: COLOR.BLACK, fontSize: 15 },
});