import { MatchOdds } from 'components';
import ItemComment from 'components/ItemComment';
import { COLOR, GlobalStyles } from 'configs';
import moment from 'moment';
import React from 'react';
import { ColorValue, StyleProp, StyleSheet, Text, TouchableOpacity, View, ViewStyle } from 'react-native';
import IconAwesome from 'react-native-vector-icons/FontAwesome5';

export interface IItemMatch {
  id: number;
  leagueName: string;
  leagueLogo?: string;
  homeName: string;
  awayName: string;
  timeExt: string;
  status: number;
  canPredict: number;
  a1: string;
  a2: string;
  a3: string;
  b1: string;
  b2: string;
  b3: string;
  c1: string;
  c2: string;
  c3: string;
  d1: string;
  d2: string;
  d3: string;
  dateTime: number;
  hrefMatch: string;
  goalHome: number;
  goalAway: number;
  type_result?: number;
  point?: number;
  point_result?: number;
  is_updated_result?: number;
  countComment?: number;
  emailComment?: string;
  nameComment?: string;
  contentComment?: string;
  mentionComment?: string;
  dateComment?: number;
  userPointComment?: number;
  photoComment?: string;
  positionComment?: number;
}

export const TYPE_MATCH = 'TYPE_MATCH';

type Props = {
  itemMatch: IItemMatch, onPredict?: (item: IItemMatch, typeResult: number) => void,
  onSelectedMatch?: (item: IItemMatch) => void,
  onPressComment?: (item: IItemMatch) => void,
  showComment?: boolean,
  style?: StyleProp<ViewStyle>,
  isScheduled?: boolean
  onSchedule?: () => void,
  showSchedule?: boolean,
};

export default class ItemMatch extends React.PureComponent<Props> {

  state = {
    // viewType: 0,
    viewStyle: styles.fixtureStyle,
    textStyle: styles.fixtureTextStyle,
    selectPredict: 0,
    colorWin: COLOR.GREY,
    winStr: '',
    typeOdd: 0,
    score: '0 - 0',
    status: 0,
  }

  static getDerivedStateFromProps(nextProps: Props, prevState: Props) {
    let initState = getInitState(nextProps.itemMatch);
    return (initState)
  }

  constructor(props: Props) {
    super(props)
  }

  onPressMatch = () => this.props.onSelectedMatch && this.props.onSelectedMatch(this.props.itemMatch)

  onPressComment = () => this.props.onPressComment && this.props.onPressComment(this.props.itemMatch)

  render() {
    let { itemMatch, showComment, style, showSchedule } = this.props
    return (
      <View style={style} onStartShouldSetResponder={() => true}>
        <TouchableOpacity style={[styles.container, GlobalStyles.ShadowItemStyle]}
          onPress={this.onPressMatch} disabled={this.props.onSelectedMatch ? false : true}
        >
          <View style={styles.statusContainer}>
            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingVertical: 5 }}>
              {
                itemMatch.status == 2 &&
                <LiveStatus itemMatch={itemMatch} style={{ marginLeft: 10 }} />
              }
              {
                (itemMatch.status === 1 || itemMatch.status == 0) && (itemMatch.dateTime ? true : false) &&
                <Text style={[styles.textStatus, { marginLeft: 10 }]}>{moment.unix(itemMatch.dateTime).format('DD/MM')}</Text>
              }
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', paddingVertical: 5 }}>
              {
                itemMatch.status === 0 && (itemMatch.dateTime ? true : false) &&
                <Text style={styles.textStatus}>{moment.unix(itemMatch.dateTime).format('HH:mm')}</Text>
              }
            </View>
            <View style={{flex: 1}}></View>
            <View style={{ position: 'absolute', top: 0, right: 0, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>

              {
                itemMatch.type_result != null && itemMatch.type_result > 0 &&
                <PredictStatus
                  style={{ marginLeft: 10, paddingBottom: 10, marginRight: 10 }}
                  itemMatch={itemMatch}
                  color={this.state.colorWin}
                  predictStatus={this.state.winStr} />
              }
              {
                showSchedule &&
                <ScheduleStatus isScheduled={this.props.isScheduled} style={{ marginLeft: 10, paddingBottom: 10, paddingHorizontal: 10 }} onSchedule={this.props.onSchedule} />
              }
              {
                showComment &&
                <ButtonComment onPressComment={this.onPressComment} style={{ marginLeft: 10, paddingBottom: 10, paddingHorizontal: 10 }} />

              }
            </View>
          </View>
          <View style={styles.containerScore}>
            <Text style={{ flex: 1, textAlign: 'right', marginRight: 7, fontSize: 16, fontWeight: this.state.typeOdd == 1 ? 'bold' : 'normal' }}>{itemMatch.homeName}</Text>
            <View style={[styles.scoreStyle, this.state.viewStyle]}>
              <Text style={[{ textAlign: 'center', fontSize: 18 }, this.state.textStyle]}>
                {this.state.score}
              </Text>
            </View>

            <Text style={{ flex: 1, textAlign: 'left', marginLeft: 7, fontSize: 16, fontWeight: this.state.typeOdd == 2 ? 'bold' : 'normal' }}>{itemMatch.awayName}</Text>
          </View>
          <MatchOdds itemMatch={itemMatch} />

        </TouchableOpacity>
        {
          showComment && itemMatch.countComment! > 0 &&
          <ItemComment
            style={{ marginHorizontal: 10, marginLeft: 20 }}
            itemComment={{
              id: 0,
              content: itemMatch.contentComment!,
              name: itemMatch.nameComment!,
              date_create: itemMatch.dateComment!,
              email: itemMatch.emailComment!,
              mention: itemMatch.mentionComment!,
              photo: itemMatch.photoComment!,
              point: itemMatch.userPointComment!,
              position: itemMatch.positionComment!
            }} onPressComment={this.onPressComment} />

        }
        {
          showComment && itemMatch.countComment! > 1 &&
          <TouchableOpacity style={{
            borderRadius: 20, backgroundColor: COLOR.OVERLEY_LIGHT, marginLeft: 60,
            alignSelf: 'baseline', paddingHorizontal: 10, marginTop: 5, marginBottom: 10
          }} onPress={this.onPressComment}>
            <Text style={{
              fontSize: 30, marginTop: -15,
              color: COLOR.SUB_TEXT
            }}>...</Text>
          </TouchableOpacity>

        }
        {
          showComment && itemMatch.countComment! == 1 &&
          <View style={{ height: 10 }} />
        }
      </View>

    );
  }
}



const ButtonComment = ({ style, onPressComment }: { style?: StyleProp<ViewStyle>, onPressComment: () => void }) => {
  return (
    <TouchableOpacity style={[{
      padding: 5,
    }, style]}
      onPress={onPressComment}>
      <IconAwesome name="comment-alt" size={17} color={COLOR.SUB_TEXT} />

    </TouchableOpacity>
  )
}

const LiveStatus = ({ itemMatch, style }: { itemMatch: IItemMatch, style?: StyleProp<ViewStyle> }) => {
  return (
    <View style={[{ flexDirection: 'row', alignItems: 'center' }, style]}>
      <IconAwesome name="circle" size={8} color='red' />
      <Text style={{ color: COLOR.RED, fontSize: 11, marginLeft: 5 }}>{itemMatch.timeExt + `'`}</Text>
    </View>
  )

}

const PredictStatus = ({ itemMatch, color, predictStatus, style }:
  { itemMatch: IItemMatch, color: ColorValue, predictStatus: string, style?: StyleProp<ViewStyle> }) => {
  return (
    <View style={[{ padding: 5 }, style]}>

      <View style={[{
        width: 17, height: 17,
        backgroundColor: color,
        borderRadius: 3, justifyContent: 'center', alignItems: 'center',
      }]}>
        <Text style={{ color: COLOR.WHITE, fontSize: 10, fontWeight: 'bold' }}>{predictStatus}</Text>
      </View>
    </View>

  )
}

const ScheduleStatus = ({ isScheduled, onSchedule, style }:
  { isScheduled?: boolean, onSchedule?: () => void, style?: StyleProp<ViewStyle> }) => {
  return (
    <TouchableOpacity style={[{
      padding: 5, justifyContent: 'center', alignItems: 'center',
    }, style]} onPress={onSchedule}>
      {
        isScheduled ?
          <IconAwesome name="bell" size={19} color={COLOR.YELLOW} solid />
          :
          <IconAwesome name="bell" size={19} color={COLOR.OVERLEY_DARK} />

      }
    </TouchableOpacity>
  )
}

const getInitState = (itemMatch: IItemMatch) => {
  let viewStyle;
  let textStyle;
  if (itemMatch.status == 2) {
    viewStyle = styles.liveStyle;
    textStyle = styles.liveTextStyle;
  } else if (itemMatch.status == 0) {
    viewStyle = styles.resultStyle;
    textStyle = styles.resultTextStyle;
  } else if (itemMatch.status == 4) {
    viewStyle = styles.delayStyle;
    textStyle = styles.resultTextStyle;
  } else {
    viewStyle = styles.fixtureStyle;
    textStyle = styles.fixtureTextStyle;
  }
  let colorWin = COLOR.GREY
  let winStr = ''

  if (itemMatch.is_updated_result == 0) {
    colorWin = COLOR.BG_GREY
    winStr = '_'
  } else if (itemMatch.point_result || itemMatch.point_result == 0) {
    if (itemMatch.point_result > 0) {
      colorWin = COLOR.GREEN
      winStr = 'W'
    } else if (itemMatch.point_result < 0) {
      colorWin = COLOR.RED
      winStr = 'L'
    } else {
      colorWin = COLOR.GREY
      winStr = 'D'
    }
  }
  let typeOdd = getTeamOdd(itemMatch.status, itemMatch.a1, itemMatch.goalHome, itemMatch.goalAway)
  let score = itemMatch.status == 4 ? 'Hoãn' : itemMatch.status == 1 ? moment.unix(itemMatch.dateTime).format('HH:mm') : `${itemMatch.goalHome} - ${itemMatch.goalAway}`
  return {
    viewStyle,
    textStyle,
    colorWin,
    winStr,
    typeOdd,
    score,
  }
}

const getTeamOdd = (status: number, odd: string, goalHome: number, goalAway: number) => {
  try {
    if (status == 0 || status == 2) {
      if (goalHome > goalAway) {
        return 1
      } else if (goalHome < goalAway) {
        return 2
      } else {
        return 0
      }
    } else {

      if (odd === '') {
        return 0
      }
      if (odd.charAt(0) === '0' && odd.charAt(odd.length - 1) === '0') {
        return 0
      }
      if (odd.charAt(0) === '0') {
        return 1
      } else {
        return 2
      }
    }
  } catch {

  }
  return 0

}


const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    borderRadius: 10,
    backgroundColor: COLOR.WHITE,
    marginVertical: 5,
  },
  containerScore: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 25,
  },
  image: {
    width: 20,
    height: 20,
  },

  scoreStyle: {
    marginLeft: 5,
    marginRight: 5,
    paddingVertical: 3,
    paddingHorizontal: 3,
    minWidth: 55,
  },
  resultStyle: {
    borderRadius: 7,
    backgroundColor: COLOR.BLACK,
  },
  delayStyle: {
    borderRadius: 7,
    backgroundColor: COLOR.GREY,
  },
  resultTextStyle: {
    color: COLOR.WHITE,
    fontWeight: 'bold',
  },
  fixtureStyle: {

  },
  fixtureTextStyle: {
    color: COLOR.MAIN,
    fontWeight: 'bold',
  },
  liveStyle: {
    borderRadius: 7,
    backgroundColor: COLOR.RED,
  },
  liveTextStyle: {
    fontWeight: 'bold',
    color: COLOR.WHITE,
  },
  textStatus: { fontSize: 12, color: COLOR.TITLE_BOX },
  statusContainer: {
    flexDirection: 'row',
    paddingTop: 0, alignItems: 'center',
    position: 'absolute', left: 0, top: 0, right: 0,
  }
});