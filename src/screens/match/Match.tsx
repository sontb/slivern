import { MatchOddApi, UserPredictApi } from 'api';
import { AdmobNative, AdsFanNative, CalendarCustom, HeaderTranslucent, Loading, GuideTip } from 'components';
import { IItemComment } from 'components/ItemComment';
import { AdFanId, AdmobUnitId, COLOR, EVENT } from 'configs';
import { FirebaseDbHelper, GlobalFuntion, NavigationService, TYPE_NOTIFY } from 'helpers';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import React, { Component } from 'react';
import { ActivityIndicator, Alert, FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import DateTimePicker from 'react-native-modal-datetime-picker';
import Icon from 'react-native-vector-icons/FontAwesome5';
import IonIcon from 'react-native-vector-icons/Ionicons';
import { SCREENS_NAME } from 'screens/ScreenName';
import { ICommonStore } from 'stores/CommonStore';
import { ILoginStore } from 'stores/LoginStore';
import { trans } from 'trans';
import ItemLeague, { IItemLeague, TYPE_LEAGUE } from './ItemLeague';
import ItemLoading, { TYPE_LOADING } from './ItemLoading';
import ItemMatch, { IItemMatch, TYPE_MATCH } from './ItemMatch';
import PopupComment from './PopupComment';

const weekday = trans.dayNames
const TYPE_ADS = 'TYPE_ADS';
type Props = { navigation: any, loginStore?: ILoginStore, commonStore?: ICommonStore, route?: any };

type States = {

}
// @inject('matchStore', 'commonStore')
// @observer
@inject('loginStore', 'commonStore')
@observer
export default class Match extends Component<Props> {

  state = {
    listMatch: [],
    refreshing: false,
    loadingMore: false,
    loading: false,
    curDate: new Date(),
    curDateStr: '',
    isDateTimePickerVisible: false,
    isToday: true,
    showPopupComment: false,
    currentItemMatchComment: undefined,
    matchSchedule: {},
    requestRefreshList: false,
  }
  flatlist: FlatList
  matchApi = new MatchOddApi();
  userPredictApi = new UserPredictApi();
  page = 0;
  SIZE = 10;
  canLoadMore = true;
  readyLoadMore = false;
  loginStatusListener: any;
  adsManager: any;
  // focusListener: any;
  blurListener: any;
  // timeoutShowAds: any;
  // nativeAdView: any;
  refreshMatchListener: any;
  nativeAdId = 0;
  constructor(props: any) {
    super(props)
    // this.adsManager = new FacebookAds.NativeAdsManager(AdFanId.native_kqltd, 1);
    let checkDate = new Date()
    let curDate = new Date()
    if (checkDate.getHours() >= 0 && checkDate.getHours() < 5) {
      curDate.setDate(curDate.getDate() - 1)
    }
    curDate.setHours(0, 0, 0, 0)
    let curDateStr = `${trans.HOMNAY}, ${weekday[curDate.getDay()]} ${moment(curDate).format('DD-MM-YYYY')}`
    this.state = {
      ... this.state,
      curDate,
      curDateStr,
    }

  }

  componentDidMount = async () => {
    let a = this.constructor.name
    this.refreshState();
    this.getData()
    this.loginStatusListener = EventRegister.addEventListener(EVENT.LOGIN_STATUS, () => {
      this._onRefresh();
    });
    this.refreshMatchListener = EventRegister.addEventListener(EVENT.REFRESH_MATCH, () => {
      this._onRefresh();
    });
    this.handleNotify()
  }


  componentWillUnmount() {
    // this.matchApi.cancel();
    EventRegister.removeEventListener(this.loginStatusListener);
    EventRegister.removeEventListener(this.refreshMatchListener);
    // this.focusListener();
    // clearTimeout(this.timeoutShowAds)
  }

  handleNotify = () => {
    try {
      let notiData = this.props.route.params?.notiData
      if (notiData) {
        let itemMatch: IItemMatch = JSON.parse(notiData?.eData)
        if (notiData.type === TYPE_NOTIFY.MatchComment) {
          this.onPressComment(itemMatch, true)
        } else {
          this.onSelectedMatch(itemMatch)
        }
      }
    } catch (err) {
      console.log(err)
    }
  }

  getData = async () => {
    try {
      if (this.page == 0) {
        if (!this.state.refreshing) {
          this.setState({ loading: true });
        }
      } else {
        this.setState({ loadingMore: true })
      }
      let email = '';
      if (this.props.loginStore!.user != null) {
        email = this.props.loginStore!.user.email;
      }
      let dateIn = moment(this.state.curDate).format('DD-MM-YYYY')
      let listMatch: any = await this.matchApi.getListMatchOdd(dateIn, this.page, email);
      let matchSchedule = await FirebaseDbHelper.getMatchSchedule()
      if (listMatch) {
        let listData = this.getListLeagueFromListMatch(listMatch);
        if (this.page == 0) {
          if (listMatch.length > 0 && EVENT.SHOW_ADS) {
            listData.splice(2, 0, { id: TYPE_ADS + this.nativeAdId, viewType: TYPE_ADS });
          }
          this.setState({ listMatch: listData, refreshing: false, loading: false, matchSchedule });
        } else {
          let listPage = this.state.listMatch.concat(listData);
          this.setState({ listMatch: listPage, loadingMore: false });
        }
        if (listMatch.length == 0) {
          this.canLoadMore = false;
        }
      } else {
        this.canLoadMore = false;
        this.setState({ loadingMore: false, refreshing: false, loading: false });
      }
    } catch (err) {
      console.log(err);
      this.setState({ refreshing: false, loadingMore: false, loading: false });
    }
  }

  getListLeagueFromListMatch = (listMatch: any) => {
    let listData: any = [];
    if (listMatch && listMatch.length > 0) {
      let checkLeague = ''
      for (let i = 0; i < listMatch.length; i++) {
        let match = listMatch[i];
        if (checkLeague !== match.leagueName) {
          let leagueMine: any = {};
          leagueMine.id = 'league' + match.id;
          leagueMine.leagueName = match.leagueName;
          leagueMine.leagueId = match.leagueId;
          leagueMine.leagueLogo = match.leagueLogo;
          leagueMine.viewType = TYPE_LEAGUE;
          leagueMine.hrefLeague = match.hrefLeague;
          listData.push(leagueMine);
          checkLeague = match.leagueName;
        }
        match.viewType = TYPE_MATCH;
        listData.push(match);
      }

    }
    return listData;
  }

  onShowLeague = (itemLeague: IItemLeague) => {
    NavigationService.navigate(SCREENS_NAME.LeagueTab, { leagueLogo: itemLeague.leagueLogo, leagueName: itemLeague.leagueName, href: itemLeague.hrefLeague });

  }

  onSelectedMatch = (itemMatch: IItemMatch) => {
    if (itemMatch.status != 4) {
      NavigationService.navigate(SCREENS_NAME.MatchDetail, { itemMatch });
    }
  }

  onPressComment = (itemMatch: IItemMatch, requestRefreshList?: boolean) => {
    GlobalFuntion.requestLogin(this.props.loginStore!, () => {
      this.setState({ showPopupComment: true, currentItemMatchComment: itemMatch, requestRefreshList })
    })
    // this.flatlist.scrollToIndex({ animated: true, index , viewOffset: 50})
  }

  onAdFailedToLoad = () => {
    this.nativeAdId++
  }

  onScheduleMatch = (item: IItemMatch) => {
    let matchSchedule = this.state.matchSchedule
    if (matchSchedule && matchSchedule[item.id.toString()]) {
      delete matchSchedule[item.id.toString()]
      // Alert.alert(trans.alertScheduleDisbale, `${item.homeName} - ${item.awayName}`)
    } else {
      matchSchedule[item.id.toString()] = 1
      // Alert.alert(trans.alertScheduleSucess, `${item.homeName} - ${item.awayName}`)

    }
    this.setState({ matchSchedule })
    FirebaseDbHelper.scheduleNotificaton(item.id.toString(), `Hẹn giờ`, `${item.homeName} - ${item.awayName}`, item, item.dateTime, item)
  }

  _renderItem = ({ item, index }: any) => {
    if (item.viewType === TYPE_ADS) {
      // return <AdsFanNative style={{ marginVertical: 5 }} placementId={AdFanId.native_kqltd} showAds={true} />
      return <AdmobNative style={{ marginVertical: 5 }} adUnitID={AdmobUnitId.native_kqltd} showAds={true}
        onAdFailedToLoad={this.onAdFailedToLoad} />
      // return <AdmobNativeBanner style={{ marginVertical: 5 }} adUnitID={AdmobUnitId.native_banner_player} showAds={true} />

    } else if (item.viewType === TYPE_LEAGUE) {
      return <ItemLeague itemLeague={item} onPressLeague={this.onShowLeague} />
    } else if (item.viewType === TYPE_LOADING) {
      return <ItemLoading />
    } else {
      return <ItemMatch itemMatch={item}
        onSelectedMatch={this.onSelectedMatch}
        onPressComment={(itemMatch) => this.onPressComment(item)} showComment
        isScheduled={this.state.matchSchedule ? this.state.matchSchedule[item.id.toString()] : false}
        onSchedule={() => this.onScheduleMatch(item)}
        showSchedule />
    }
  };

  _onRefresh = async () => {
    this.refreshState();
    await this.setState({ refreshing: true })
    this.getData();
  }

  refreshState = () => {
    this.page = 0;
    this.canLoadMore = true;
    this.readyLoadMore = false;
    // this.setState({ refreshing: false, loadingMore: false });
  }

  _loadMore = () => {
    if (this.canLoadMore && this.readyLoadMore) {
      this.page = this.page + 1;
      this.getData();
    }
  }

  _onMomentumScrollBegin = () => {
    this.readyLoadMore = true;
  }

  _renderFooter = () => {
    if (!this.state.loadingMore) {
      return null;
    }
    return (<ActivityIndicator
      color={COLOR.PRIMARY_COLOR}
      animating
      size='large'
    />);
  }

  onCloseTips = () => {
    this.props.loginStore!.closeTipMatch();
    // this.setState({ guideId: this.props.loginStore!.userPredict.guideId })
  }

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true })
  }
  goToToday = async () => {
    let curDate = new Date()
    curDate.setHours(0, 0, 0, 0)
    await this.setState({ curDate, curDateStr: `${trans.HOMNAY}, ${weekday[curDate.getDay()]} ${moment(curDate).format('DD-MM-YYYY')}` })
    this.refreshState()
    this.getData()
  }

  handleDatePicked = async (date: any) => {
    // this.initTab(date, true);
    let today = new Date()
    today.setHours(0, 0, 0, 0)
    let curDateStr = ''
    if (moment(date).isSame(today)) {
      curDateStr = `${trans.HOMNAY}, ${weekday[date.getDay()]} ${moment(date).format('DD-MM-YYYY')}`
    } else {
      curDateStr = `${weekday[date.getDay()]} ${moment(date).format('DD-MM-YYYY')}`
    }
    await this.setState({ curDate: date, isDateTimePickerVisible: false, curDateStr })
    this.refreshState()
    await this.getData()
    if (this.state.listMatch && this.state.listMatch.length > 0) {
      this.flatlist.scrollToIndex({ animated: true, index: 0 })
    }
  }

  onCancelCalendar = () => this.setState({ isDateTimePickerVisible: false })

  onSummitComment = (requestRefreshList: boolean) => {
    try {
      if (requestRefreshList) {
        this._onRefresh();
      } else {
        this.setState({ listMatch: this.state.listMatch })
      }
    } catch {

    }
  }

  render() {
    let { loginStore } = this.props;
    return (
      <View style={styles.container}>
        <HeaderTranslucent
          hasTabs title={trans.ketQuaLichThiDau}
          onPressRight={this.showDateTimePicker}
          onLongPressRight={this.goToToday}
          iconRight={(
            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
              <IonIcon name="md-calendar" size={25} color={COLOR.WHITE} />
              <Text style={{ color: COLOR.WHITE, fontWeight: 'normal', fontSize: 18, marginLeft: 5 }}>{trans.monthNames[this.state.curDate.getMonth()]}</Text>
            </View>
          )} />
        {/* <View style={{
          alignItems: 'center',
          flexDirection: 'row',
          marginTop: -10,
        }}> */}

        <CalendarCustom curentDate={this.state.curDate}
          onSelectedDate={this.handleDatePicked}
          style={{ marginTop: -5 }} />

        <View style={{ flex: 1, backgroundColor: COLOR.BG, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden', marginTop: 5 }}>
          {/* {
            loginStore?.isShowTipMatch &&
            <GuideTip text={trans.helpMatchBetting} onPressClose={this.onCloseTips} />
          } */}
          <FlatList
            ref={(ref) => this.flatlist = ref}
            contentContainerStyle={{ paddingBottom: 50 }}
            data={this.state.listMatch}
            extraData={this.state.refreshing}
            renderItem={this._renderItem}
            keyExtractor={(item: IItemMatch, index) => `${item.id}`}
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
            onEndReachedThreshold={0.01}
            // windowSize={3}
            onEndReached={this._loadMore}
            onMomentumScrollBegin={this._onMomentumScrollBegin}
            ListFooterComponent={this._renderFooter}
            removeClippedSubviews
          />
        </View>
        <DateTimePicker
          mode="date"
          date={this.state.curDate}
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          confirmTextIOS='Xác nhận'
          onCancel={this.onCancelCalendar}
          cancelTextIOS='Hủy bỏ'
        />
        {
          this.state.showPopupComment &&
          <PopupComment
            requestRefreshList={this.state.requestRefreshList}
            itemMatch={this.state.currentItemMatchComment}
            showPopupComment={this.state.showPopupComment}
            onCancel={() => this.setState({ showPopupComment: false })}
            onComment={this.onSummitComment}
            type={0} />
        }

        <Loading loading={this.state.loading} />

      </View>
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.MAIN,
  },
});

