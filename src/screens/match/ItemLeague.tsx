import { COLOR } from 'configs';
import React, { PureComponent } from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { trans } from 'trans';

export const TYPE_LEAGUE = 'TYPE_LEAGUE';

export interface IItemLeague {
  id: number;
  leagueName: string;
  leagueLogo: string;
  leagueId: number;
  hrefLeague: string;
}

type Props = { itemLeague: IItemLeague, onPressLeague?: (itemLeague: IItemLeague) => void };

export default class ItemLeague extends PureComponent<Props> {
  // shouldComponentUpdate(nextProps: Props, nextState: any) {

  //   return nextProps.itemMatch.id !== this.props.itemMatch.id;
  // }
  onPressLeague = () => this.props.onPressLeague && this.props.onPressLeague(this.props.itemLeague)
  render() {
    let { itemLeague } = this.props;
    return (
      <TouchableOpacity style={styles.container}
        onPress={this.onPressLeague}>
        {
          itemLeague.leagueLogo ?
            <Image style={{ width: 30, height: 30 }} source={{ uri: itemLeague.leagueLogo }} resizeMethod='resize' resizeMode='contain' />
            :
            <View style={{ width: 30, height: 20, backgroundColor: COLOR.OVERLEY_DARK }} />
        }

        <Text style={{ fontWeight: 'bold', marginLeft: 10 }}>{itemLeague.leagueName}</Text>
        {
          (itemLeague.leagueId ? true : false) &&
          <Text style={{ fontWeight: 'bold', marginLeft: 10, color: COLOR.SUB_TEXT }}>- {trans.tabBXH}</Text>

        }
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginTop: 10,
    marginBottom: 0,
  },
});