import { COLOR } from 'configs';
import React, { Component } from 'react';
import {
  StyleProp,
  StyleSheet, Text,
  TouchableWithoutFeedback, View,
  ViewStyle,
  TouchableOpacity,
  TextInput,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Alert,
} from 'react-native';
import { inject, observer } from 'mobx-react';
import { ILoginStore } from 'stores/LoginStore';
import Modal from 'react-native-modal';
import { trans } from 'trans';

const ITEM_HEIGHT = 50
type Props = {
  style: StyleProp<ViewStyle>, onCancel: () => void, onPredictMatch: (typeResult: number, pointPredict: number) => void,
  homeName: string, awayName: string, loginStore?: ILoginStore,
  showPopupPredict: boolean,
};

@inject('loginStore')
@observer
export default class PopupPredict extends Component<Props> {

  state = {
    selectedTeam: 0,
    pointStr: '',
  }

  onPredictMatch = () => {
    if (this.state.selectedTeam == 0) {
      Alert.alert(trans.thongBao, trans.vuiLongChonDoi)
      return
    }
    let pointPredict = 0
    try {
      pointPredict = Number(this.state.pointStr)
    } catch {

    }
    if (pointPredict == 0 || !pointPredict) {
      Alert.alert(trans.thongBao, trans.vuiLongNhapDiem)
      return
    }
    if (pointPredict > 20) {
      Alert.alert(trans.thongBao, trans.moiTranChiDuoc)
      return
    }
    if (pointPredict > this.props.loginStore?.userPredict.point) {
      Alert.alert(trans.thongBao, trans.banKhongDuocCuocToiDa)
      return
    }
    this.props.onPredictMatch(this.state.selectedTeam, pointPredict)
  }

  onChangeText = (text: string) => {
    let newText = '';
    let numbers = '0123456789';

    for (var i = 0; i < text.length; i++) {
      if (numbers.indexOf(text[i]) > -1) {
        newText = newText + text[i];
      }
      // else {
      //   return;
      // }
    }
    let number = '';
    if (newText) {
      try {
        let n = Number(newText)
        if (n > 20) {
          n = 20
        }
        number = `${n}`
      } catch {

      }
    }
    this.setState({ pointStr: number })
  }

  render() {
    let userPredict = this.props.loginStore?.userPredict
    return (
      <Modal
        style={{ margin: 0 }}
        isVisible={this.props.showPopupPredict}
        swipeDirection='down'
        onSwipeComplete={this.props.onCancel}
        onBackButtonPress={this.props.onCancel}
      >
        <View style={{ flex: 1 }}>
          <TouchableWithoutFeedback onPress={this.props.onCancel}>
            <View style={{ flex: 1, marginBottom: -15 }} />
          </TouchableWithoutFeedback>
          <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'position' : undefined} enabled
            keyboardVerticalOffset={0}>
            <ScrollView style={{ backgroundColor: COLOR.WHITE, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden' }}
              keyboardShouldPersistTaps="handled">
              <View style={{ paddingVertical: 15, borderBottomWidth: 1, borderBottomColor: COLOR.BORDER, alignItems: 'center' }}>
                <Text style={{ fontSize: 18, fontWeight: 'bold' }}>{trans.keo}</Text>
              </View>
              <View style={{ paddingTop: 10, paddingHorizontal: 20 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                  <TouchableOpacity style={{
                    padding: 10,
                    backgroundColor: this.state.selectedTeam == 1 ? COLOR.PROFESSION : COLOR.OVERLEY_DARK,
                    borderRadius: 5,
                    flex: 1, alignItems: 'center'
                  }}
                    onPress={() => this.setState({ selectedTeam: 1 })}>
                    <Text style={{ fontWeight: 'bold', color: this.state.selectedTeam == 1 ? COLOR.WHITE : COLOR.MAIN_TEXT }}>{this.props.homeName}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{
                    padding: 10,
                    backgroundColor: this.state.selectedTeam == 2 ? COLOR.PROFESSION : COLOR.OVERLEY_DARK,
                    borderRadius: 5,
                    marginLeft: 20,
                    flex: 1, alignItems: 'center'
                  }}
                    onPress={() => this.setState({ selectedTeam: 2 })}>
                    <Text style={{ fontWeight: 'bold', color: this.state.selectedTeam == 2 ? COLOR.WHITE : COLOR.MAIN_TEXT }}>{this.props.awayName}</Text>
                  </TouchableOpacity>

                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                  <View style={{
                    paddingHorizontal: 10, paddingVertical: 8, backgroundColor: COLOR.WHITE,
                    borderRadius: 5, borderColor: COLOR.PROFESSION, borderWidth: 1,
                    flex: 1
                  }}>
                    <TextInput
                      value={this.state.pointStr}
                      style={{ fontSize: 18, paddingVertical: 0 }}
                      keyboardType='numeric'
                      placeholder={userPredict && userPredict.point > 20 ? trans.soPointDatCuocToiDa20 : `${trans.soPointDatCuocToiDa} ${userPredict?.point} Point`}
                      placeholderTextColor={COLOR.GREY}
                      onChangeText={this.onChangeText} />

                  </View>

                </View>
                <TouchableOpacity style={{
                  backgroundColor: COLOR.GREEN, borderRadius: 5, paddingHorizontal: 40, paddingVertical: 10,
                  marginTop: 20, alignSelf: 'center', marginBottom: 20
                }} onPress={this.onPredictMatch}>
                  <Text style={{ color: COLOR.WHITE, fontWeight: 'bold' }}>{trans.hoanTat}</Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </Modal>

    );
  }
}

const styles = StyleSheet.create({
  containRow: { paddingTop: 0, alignItems: 'flex-start', height: ITEM_HEIGHT },
  viewRow: { flexDirection: 'row', marginHorizontal: 30, flex: 1, alignItems: 'center' },
  textRow: { color: COLOR.MAIN_TEXT, fontSize: 18, flex: 1 },
  icon: { color: COLOR.MAIN_TEXT, fontSize: 20, marginLeft: 10 },
  empty: { width: 20, height: 20 },
  line: { alignSelf: 'stretch', height: 1, backgroundColor: COLOR.OVERLEY_DARK, marginHorizontal: 10 },
})