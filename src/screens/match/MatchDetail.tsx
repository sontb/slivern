import { LeagueParse, MatchOddApi, UserPredictApi } from 'api';
import { HeaderTranslucent } from 'components';
import { COLOR, EVENT } from 'configs';
import { GlobalFuntion, NavigationService } from 'helpers';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import React, { Component } from 'react';
import { Alert, Dimensions, Image, RefreshControl, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { EventRegister } from 'react-native-event-listeners';
import Icon from 'react-native-vector-icons/Ionicons';
import { SCREENS_NAME } from 'screens/ScreenName';
import { ICommonStore } from 'stores/CommonStore';
import { ILoginStore } from 'stores/LoginStore';
import { IMatchStore } from 'stores/MatchStore';
import { trans } from 'trans';
import { IItemMatch } from './ItemMatch';
import PopupPredict from './PopupPredict';

const initialLayout = { width: Dimensions.get('window').width };

type Props = { navigation?: any, commonStore: ICommonStore, route?: any, matchStore: IMatchStore, loginStore: ILoginStore };
export interface IPlayer {
  name: string;
  number: string;
  type: number;
}

export interface IStatistic {
  name: string;
  value1: number;
  value2: number;
  valueStr1: number;
  valueStr2: number;
}

export interface IMatch {
  date: string;
  name1: string;
  name2: string;
  score: string;
  result: number;
}

@inject('commonStore', 'leagueStore', 'loginStore')
@observer
export default class MatchDetail extends Component<Props> {
  itemMatch: IItemMatch = this.props.route.params.itemMatch
  userPredictApi = new UserPredictApi();

  state = {
    logo1: "",
    logo2: "",
    dateStr: "",
    scoreStr: "",
    scoreExt: [],
    leagueName: "",
    goal1: [],
    goal2: [],
    players1: [],
    players2: [],
    subPlayers1: [],
    subPlayers2: [],
    statistics: [],
    listMatch: [],
    listMatch1: [],
    listMatch2: [],
    showSubPlayer: false,
    win1: 0,
    win2: 0,
    draw: 0,
    showPopupPredict: false,
    typeResult: this.itemMatch.type_result,
    pointPredict: this.itemMatch.point,
    pointResult: this.itemMatch.point_result,
    status: this.itemMatch.status,
    isUpdatedResult: this.itemMatch.is_updated_result,
    refreshing: false,
  }

  // leagueName = this.props.route.params.leagueName
  // href = this.props.route.params.href
  // leagueLogo = this.props.route.params.leagueLogo
  matchApi = new MatchOddApi();


  constructor(props: any) {
    super(props)
    let dateStr = ""
    let scoreStr = ""
    if (this.itemMatch.status == 1) {
      dateStr = moment.unix(this.itemMatch.dateTime).format('DD/MM')
      scoreStr = moment.unix(this.itemMatch.dateTime).format('HH:mm')
    } else if (this.itemMatch.status == 0) {
      dateStr = 'FT'
      scoreStr = `${this.itemMatch.goalHome} - ${this.itemMatch.goalAway}`
    } else if (this.itemMatch.status == 4) {
      dateStr = ''
      scoreStr = 'Hoãn'
    } else {
      dateStr = this.itemMatch.timeExt + `'`
      scoreStr = `${this.itemMatch.goalHome} - ${this.itemMatch.goalAway}`
    }
    this.state = {
      ...this.state,
      dateStr, scoreStr
    }
  }

  componentDidMount = async () => {
    this.getData()
    this.getMatchDetailData()
  }

  getData = async () => {
    this.props.commonStore.toggleLoading()
    try {
      let detail = await LeagueParse.getMatchDetail(this.itemMatch.hrefMatch, this.itemMatch.id, this.state.status)
      this.setState({ ...detail, refreshing: false })
      console.log("")
    } catch (err) {
      console.error(err)
      this.setState({ refreshing: false })
    }
    this.props.commonStore.loadingCompleted()
  }

  getMatchDetailData = async () => {
    try {
      let email = '';
      if (this.props.loginStore!.user) {
        email = this.props.loginStore!.user.email;
      }
      let itemMatch: IItemMatch = await this.matchApi.getMatchDetail(this.itemMatch.id, email)
      this.setState({
        typeResult: itemMatch.type_result, pointPredict: itemMatch.point,
        pointResult: itemMatch.point_result,
        status: itemMatch.status,
        isUpdatedResult: itemMatch.is_updated_result,
      })
      if (itemMatch.status == 2) {
        this.getMatchLive()
      }
      console.log("")
    } catch (err) {
      // this.setState({ refreshing: false })
    }
  }

  getMatchLive = async () => {
    try {
      let email = '';
      if (this.props.loginStore!.user) {
        email = this.props.loginStore!.user.email;
      }
      let listMatchLive: { matchId: number, firstTeamGoal: number, secondTeamGoal: number, status: number, liveTime: number }[]
        = await this.matchApi.getListLiveMatch()
      let scoreStr = ""
      let dateStr = ""
      let status = 2
      listMatchLive.some(item => {
        if (item.matchId == this.itemMatch.id) {
          scoreStr = item.firstTeamGoal + " - " + item.secondTeamGoal
          if (item.status == 2) {
            dateStr = "HT"
            status = 2
          } else if (item.status == 5) {
            status = 0
            dateStr = ""
          } else {
            dateStr = item.liveTime + `'`
            status = 2
          }
          return true
        }
      })
      if (status == 0) {
        this.setState({ status, scoreStr })
      } else {
        this.setState({ status, dateStr, scoreStr })
      }
      console.log("")
    } catch (err) {
      // this.setState({ refreshing: false })
    }
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    await this.getMatchDetailData()
    this.getData()
    this.setState({ refreshing: false })
  }

  showPredictMatch = async () => {
    GlobalFuntion.requestLogin(this.props.loginStore, (isLogin) => {
      this.setState({ showPopupPredict: true })
      if (isLogin) {
        this.getMatchDetailData()
      }
    })

  }

  onPredictMatch = async (typeResult: number, pointPredict: number) => {
    if (this.props.loginStore!.user != null) {
      this.props.commonStore!.toggleLoading();
      try {
        let result = await this.userPredictApi.predict(this.props.loginStore!.user.email, this.itemMatch.id, typeResult, pointPredict);
        if (result.canpredict == 0) {
          Alert.alert(trans.duDoanKetQua, trans.tranDauNayKhongThe)
        } else if (result.canpredict == -1) {
          Alert.alert(trans.loi, trans.khongTheDatCuoc)

        } else {
          this.props.loginStore.userPredict = { ...this.props.loginStore.userPredict, point: this.props.loginStore.userPredict.point - pointPredict }
          this.setState({ typeResult, pointPredict, showPopupPredict: false })
          EventRegister.emit(EVENT.REFRESH_MATCH);
          Alert.alert(trans.thongBao, trans.datCuocThanhCong)

        }
        this.props.commonStore!.loadingCompleted();
        console.log('result match');
      } catch (e) {
        console.log(e);
        Alert.alert(trans.loi, trans.khongTheDatCuoc)
        this.props.commonStore!.loadingCompleted();
      }
    } else {
      this.props.loginStore!.signIn();
    }
    this.setState({ showPopupPredict: false })
  }


  render() {
    let { loginStore } = this.props
    return (
      <View style={{ flex: 1, backgroundColor: COLOR.MAIN }}>

        <HeaderTranslucent title={this.state.leagueName} canBack
          titleFontSize={18} />
        <Text style={{
          marginTop: -10,
          fontSize: 16,
          color: this.itemMatch.status == 2 ? COLOR.RED : COLOR.WHITE,
          alignSelf: 'center', fontWeight: this.itemMatch.status == 0 ? 'bold' : 'normal'
        }}>{this.state.dateStr}</Text>

        <View style={{ flexDirection: 'row', marginTop: 0, justifyContent: 'center' }}>
          <Team logo={this.state.logo1} name={this.itemMatch.homeName} />
          {
            (this.itemMatch.status == 0 || this.itemMatch.status == 2) &&
            <View style={{ alignItems: 'center' }}>
              <View style={{
                alignItems: 'center', backgroundColor: this.itemMatch.status == 2 ? COLOR.RED : COLOR.OVERLEY_LIGHT_LIGHT, alignSelf: 'flex-start',
                marginHorizontal: 10, marginTop: 10,
                paddingHorizontal: 7,
                borderRadius: 8, marginVertical: 10,
              }}>
                <Text style={{
                  color: COLOR.MAIN,
                  fontSize: 25, fontWeight: 'bold',
                }}>{this.state.scoreStr}</Text>
              </View>
              {
                (this.state.scoreExt ? true : false) && this.state.scoreExt.map((item, index) => {
                  return (
                    <Text style={{ color: COLOR.OVERLEY_LIGHT_LIGHT, fontSize: 12 }} key={item}>{item}</Text>
                  )
                })
              }
            </View>

          }
          {
            this.itemMatch.status == 1 &&

            <Text style={{
              marginTop: 10,
              color: COLOR.WHITE,
              fontSize: 25, fontWeight: 'bold',
            }}>{this.state.scoreStr}</Text>
          }


          <Team logo={this.state.logo2} name={this.itemMatch.awayName} />

        </View>

        <View style={{ flex: 1, backgroundColor: COLOR.BG, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden', marginTop: 10 }}>
          <ScrollView contentContainerStyle={{ padding: 10, paddingBottom: 20 }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }>
            {
              (((this.itemMatch.canPredict ? true : false) || this.itemMatch.type_result != 0)
                // && EVENT.APPSTORE == 1
              )
              &&
              <View>
                <Text style={{ fontWeight: 'bold', marginLeft: 15 }}>{trans.keo}</Text>
                <View style={[styles.itemContainer, { paddingVertical: 10, flexDirection: 'column', alignItems: 'center', paddingHorizontal: 20 }]}>
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={{ flex: 1 }}>{trans.tyle}</Text>
                    <Text style={{ fontWeight: 'bold', color: COLOR.MAIN, fontSize: 18, marginLeft: 10, flex: 1, textAlign: 'right' }}>{this.itemMatch.a1}</Text>
                  </View>
                  <View style={{ height: 1, alignSelf: 'stretch', backgroundColor: COLOR.BORDER, marginTop: 5 }} />

                  {
                    (this.state.typeResult ? true : false) &&
                    <View style={{ alignSelf: 'stretch' }}>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 2 }}>
                        <Text style={{ flex: 1 }}>{trans.daChon}</Text>
                        <Text style={{
                          marginLeft: 10,
                          fontWeight: 'bold', color: COLOR.BLACK,
                          fontSize: 18, flex: 1, textAlign: 'right'
                        }}>{this.state.typeResult == 1 ? this.itemMatch.homeName : this.itemMatch.awayName}</Text>

                      </View>
                      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 2 }}>
                        <Text style={{ flex: 1 }}>{trans.daCuoc}</Text>
                        <Text style={{
                          marginLeft: 10, fontWeight: 'bold',
                          color: COLOR.MAIN, fontSize: 18, flex: 1, textAlign: 'right'
                        }}>{this.state.pointPredict} <Text style={{
                          fontWeight: 'bold', fontSize: 14
                        }}>Point</Text></Text>

                      </View>
                      {
                        this.state.status == 0 && (this.state.pointResult ? true : false) &&
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 2 }}>
                          <Text style={{ flex: 1 }}>{trans.ketQua}</Text>
                          <Text style={{
                            marginLeft: 10, fontWeight: 'bold',
                            color: this.state.pointResult == 0 ? COLOR.GREY : this.state.pointResult > 0 ? COLOR.GREEN : COLOR.RED, fontSize: 18, flex: 1, textAlign: 'right'
                          }}>{this.state.pointResult > 0 ? '+' : ''}{this.state.pointResult} <Text style={{
                            fontWeight: 'bold', fontSize: 14
                          }}>Point</Text></Text>

                        </View>
                      }

                    </View>
                  }

                  {
                    this.state.status == 1 && this.state.typeResult == 0 && (loginStore.user ? true : false) &&
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 2 }}>
                      <Text style={{ flex: 1 }}>{trans.diemCuaBan}</Text>
                      <Text style={{
                        marginLeft: 10, fontWeight: 'bold',
                        color: COLOR.PROFESSION, fontSize: 18, flex: 1, textAlign: 'right'
                      }}>{loginStore.userPredict.point} <Text style={{
                        fontWeight: 'bold', fontSize: 14
                      }}>Point</Text></Text>
                    </View>
                  }


                  {
                    this.state.status == 1 &&
                    ((this.state.typeResult == 0 &&
                      this.props.loginStore.userPredict.point > 0) ||
                      (loginStore.user ? false : true)) &&
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                      <TouchableOpacity style={{ padding: 5, flex: 1 }} onPress={() => {
                        NavigationService.navigate(SCREENS_NAME.PointHelp,
                          {
                            a1: this.itemMatch.a1, homeName: this.itemMatch.homeName,
                            awayName: this.itemMatch.awayName
                          })
                      }}>
                        <Text style={{ fontSize: 16, color: COLOR.SUB_TEXT, flex: 1, textDecorationLine: 'underline' }}>{trans.helpBetting}</Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={{
                        backgroundColor: COLOR.PROFESSION, padding: 7, borderRadius: 5,
                        flexDirection: 'row', alignItems: 'center'
                      }}
                        onPress={this.showPredictMatch}>
                        {
                          EVENT.APPSTORE == 1 &&
                          <Image style={{ width: 15, height: 15, marginRight: 10 }} source={require('assets/ic_betting.png')} />

                        }
                        <Text style={{ color: COLOR.WHITE, fontWeight: 'bold' }}>{trans.datCuoc}</Text>

                      </TouchableOpacity>
                    </View>

                  }
                  {/* <Image style={{ width: 24, height: 24 }} source={require('assets/ic_betting.png')} /> */}

                </View>
              </View>
            }

            {
              (this.state.goal1.length > 0 || this.state.goal2.length > 0) &&
              <View style={{}}>
                <Text style={{ fontWeight: 'bold', marginLeft: 15, marginTop: 10 }}>{trans.ghiBan}</Text>
                <View style={styles.itemContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                      {
                        this.state.goal1.map((item: string, index) => {
                          return (
                            <Text style={{ textAlign: 'right', marginTop: 5, fontSize: 13 }} key={`${index}`}>{item.trim()}</Text>
                          )
                        })
                      }
                    </View>
                    <View style={{ width: 1, alignSelf: 'stretch', backgroundColor: COLOR.GREY, marginHorizontal: 8 }} />
                    <View style={{ flex: 1 }}>
                      {
                        this.state.goal2.map((item: string, index) => {
                          return (
                            <Text style={{ textAlign: 'left', marginTop: 5, fontSize: 13 }} key={`${index}`}>{item.trim()}</Text>
                          )
                        })
                      }
                    </View>
                  </View>

                </View>
              </View>

            }
            {
              this.state.players1.length > 0 && this.state.players2.length > 0 &&
              <View style={{ marginTop: 10 }}>
                <Text style={{ fontWeight: 'bold', marginLeft: 15 }}>{trans.doiHinhRaSan}</Text>
                <View style={styles.itemContainer}>

                  <Text style={{ alignSelf: 'center' }}>{trans.doiHinhChinh}</Text>
                  <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                      {
                        this.state.players1.map((item: IPlayer, index) => {
                          return (
                            <Player item={item} key={`${index}`} />
                          )
                        })
                      }
                    </View>

                    <View style={{ flex: 1, flexDirection: 'column' }}>
                      {
                        this.state.players2.map((item, index) => {
                          return (
                            <Player item={item} key={`${index}`} isRight />
                          )
                        })
                      }
                    </View>
                  </View>
                  <TouchableOpacity style={{ alignSelf: 'center', flexDirection: 'row', alignItems: 'center', paddingTop: 10 }}
                    onPress={() => this.setState({ showSubPlayer: !this.state.showSubPlayer })}>
                    <Text style={{}}>{trans.doiHinhDuBi}</Text>
                    <Icon name='caret-down-circle-outline' style={{ color: COLOR.MAIN_TEXT, fontSize: 20, marginLeft: 10 }} />

                  </TouchableOpacity>
                  {
                    (this.state.showSubPlayer ? true : false) &&
                    <View style={{ flexDirection: 'row', paddingHorizontal: 5 }}>
                      <View style={{ flex: 1, flexDirection: 'column' }}>
                        {
                          this.state.subPlayers1.map((item, index) => {
                            return (
                              <Player item={item} key={`${index}`} />
                            )
                          })
                        }
                      </View>

                      <View style={{ flex: 1, flexDirection: 'column' }}>
                        {
                          this.state.subPlayers2.map((item, index) => {
                            return (
                              <Player item={item} key={`${index}`} isRight />
                            )
                          })
                        }
                      </View>
                    </View>
                  }

                </View>
              </View>

            }
            {
              this.state.statistics.length > 0 && this.itemMatch.status == 0 &&
              <View style={{ marginTop: 10 }}>
                <Text style={{ fontWeight: 'bold', marginLeft: 15, marginTop: 10 }}>{trans.thongKe}</Text>
                <View style={styles.itemContainer}>
                  {
                    this.state.statistics.map((item: any, index) => {
                      return (
                        <View style={{ marginTop: 5 }} key={`${index}`}>
                          <Text style={{ marginLeft: 10, alignSelf: 'center' }}>{item.name}</Text>
                          <View style={{ flexDirection: 'row', marginTop: 2, alignItems: 'center' }}>
                            <View style={{ width: 40, alignItems: 'center' }}>
                              <Text>{item.valueStr1}</Text>
                            </View>
                            <View style={{ backgroundColor: COLOR.YELLOW, height: 5, flex: item.value1 == 0 ? 1 : item.value1 }} />
                            <View style={{ backgroundColor: COLOR.PROFESSION, height: 5, flex: item.value2 == 0 ? 1 : item.value2 }} />
                            <View style={{ width: 40, alignItems: 'center' }}>
                              <Text>{item.valueStr2}</Text>
                            </View>
                          </View>
                        </View>
                      )
                    })
                  }
                </View>
              </View>
            }
            {
              this.state.listMatch.length > 0 &&
              <View style={{ marginTop: 10 }}>
                <Text style={{ fontWeight: 'bold', marginLeft: 15, marginTop: 10 }}>{trans.doiDau}</Text>
                <View style={styles.itemContainer}>
                  <View style={{ flexDirection: 'row' }}>
                    {(this.state.win1 ? true : false) &&
                      Array.from(Array(this.state.win1)).map((_, index: number) => {
                        return (
                          <View style={{
                            backgroundColor: COLOR.YELLOW, height: 5, flex: 1,
                            borderLeftColor: COLOR.BORDER, borderRightColor: COLOR.BORDER,
                            borderLeftWidth: 1, borderRightWidth: 1
                          }} key={`${index}`} />
                        )
                      })
                    }
                    {(this.state.draw ? true : false) &&
                      Array.from(Array(this.state.draw)).map((_, index: number) => {
                        return (
                          <View style={{
                            backgroundColor: COLOR.GREY, height: 5, flex: 1,
                            borderLeftColor: COLOR.BORDER, borderRightColor: COLOR.BORDER,
                            borderLeftWidth: 1, borderRightWidth: 1
                          }} key={`${index}`} />
                        )
                      })
                    }
                    {(this.state.win2 ? true : false) &&
                      Array.from(Array(this.state.win2)).map((_, index: number) => {
                        return (
                          <View style={{
                            backgroundColor: COLOR.PROFESSION, height: 5, flex: 1,
                            borderLeftColor: COLOR.BORDER, borderRightColor: COLOR.BORDER,
                            borderLeftWidth: 1, borderRightWidth: 1
                          }} key={`${index}`} />
                        )
                      })
                    }
                  </View>
                  {
                    this.state.listMatch.map((item, index) => {
                      return (
                        <Match item={item} key={`${index}`} />
                      )
                    })
                  }

                </View>
              </View>
            }
            {
              this.state.listMatch1.length > 0 &&
              <View style={{ marginTop: 10 }}>
                <Text style={{ fontWeight: 'bold', marginLeft: 15, marginTop: 10 }}>{trans.phongDo} {this.itemMatch.homeName}</Text>
                <View style={styles.itemContainer}>
                  {
                    this.state.listMatch1.map((item, index) => {
                      return (
                        <Match item={item} key={`${index}`} teamName={this.itemMatch.homeName} />
                      )
                    })
                  }

                </View>
              </View>
            }
            {
              this.state.listMatch2.length > 0 &&
              <View style={{ marginTop: 10 }}>
                <Text style={{ fontWeight: 'bold', marginLeft: 15, marginTop: 10 }}>Phong độ {this.itemMatch.awayName}</Text>
                <View style={styles.itemContainer}>
                  {
                    this.state.listMatch2.map((item, index) => {
                      return (
                        <Match item={item} key={`${index}`} teamName={this.itemMatch.awayName} />
                      )
                    })
                  }

                </View>
              </View>
            }
          </ScrollView>
        </View>

        <PopupPredict
          style={{ position: 'absolute', bottom: 0, left: 0, right: 0 }}
          showPopupPredict={this.state.showPopupPredict}
          onCancel={() => this.setState({ showPopupPredict: false })}
          onPredictMatch={this.onPredictMatch}
          homeName={this.itemMatch.homeName}
          awayName={this.itemMatch.awayName}
        />

      </View >
    );
  }
}

const Team = ({ logo, name }: any) => {
  return (
    <View style={{ width: 120, flexDirection: 'column', alignItems: 'center' }}>
      <View style={{
        width: 50, height: 50, borderRadius: 100, backgroundColor: COLOR.OVERLEY_LIGHT_LIGHT, alignItems: 'center'
        , justifyContent: 'center', overflow: 'hidden', borderWidth: 4, borderColor: COLOR.WHITE
      }} >
        <Image style={{ width: 40, height: 40 }} source={{ uri: logo }} resizeMethod='resize' resizeMode='contain' />

      </View>

      <Text style={{ color: COLOR.WHITE, fontSize: 15, marginTop: 10, textAlign: 'center', fontWeight: 'bold' }}>{name}</Text>
    </View>
  )
}

const Player = ({ item, isRight }: { item: IPlayer, isRight?: boolean }) => {
  let color
  let type = item.type
  if (type == 1) {
    color = COLOR.PROFESSION
  } else if (type == 2) {
    color = COLOR.GREEN
  } else if (type == 3) {
    color = COLOR.BLUE
  } else if (type == 4) {
    color = COLOR.RED
  } else {
    color = COLOR.GREY
  }
  if (isRight) {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginTop: 10 }}>
        <Text style={{ fontSize: 16, marginRight: 10 }}>{item.name}</Text>
        <View style={{ width: 25, height: 25, backgroundColor: color, alignItems: 'center', justifyContent: 'center', borderRadius: 5 }}>
          <Text style={{ color: COLOR.WHITE, fontSize: 13, fontWeight: 'bold' }}>{item.number}</Text>
        </View>
      </View>
    )
  } else {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
        <View style={{ width: 25, height: 25, backgroundColor: color, alignItems: 'center', justifyContent: 'center', borderRadius: 5 }}>
          <Text style={{ color: COLOR.WHITE, fontSize: 13, fontWeight: 'bold' }}>{item.number}</Text>
        </View>
        <Text style={{ fontSize: 16, marginLeft: 10 }}>{item.name}</Text>
      </View>
    )
  }

}

const Match = ({ item, teamName }: { item: IMatch, teamName?: string }) => {
  let colorWin = COLOR.GREY
  let winStr = 'D'
  if (teamName === item.name1) {
    if (item.result == 1) {
      colorWin = COLOR.GREEN
      winStr = 'W'
    } else if (item.result == 3) {
      colorWin = COLOR.RED
      winStr = 'L'
    }
  } else if (teamName === item.name2) {
    if (item.result == 3) {
      colorWin = COLOR.GREEN
      winStr = 'W'
    } else if (item.result == 1) {
      colorWin = COLOR.RED
      winStr = 'L'
    }
  }
  return (
    <View style={{ marginTop: 15, flexDirection: 'row', alignItems: 'center' }}>
      <Text style={{ fontSize: 14, color: COLOR.SUB_TEXT, width: 50 }}>{item.date}</Text>
      <View style={styles.containerScore}>
        <Text style={{
          flex: 1, textAlign: 'right',
          fontWeight: item.result == 1 ? 'bold' : 'normal',
          color: COLOR.MAIN_TEXT,
          fontSize: 15

        }}>{item.name1}</Text>
        <View style={[styles.scoreStyle]}>
          <Text style={styles.resultTextStyle}>{item.score}</Text>
        </View>

        <Text style={{
          flex: 1, textAlign: 'left',
          fontWeight: item.result == 3 ? 'bold' : 'normal',
          color: COLOR.MAIN_TEXT,
          fontSize: 15

        }}>{item.name2}</Text>
      </View>
      {
        teamName ?
          <View style={{
            width: 17, height: 17,
            backgroundColor: colorWin,
            borderRadius: 3, justifyContent: 'center', alignItems: 'center',
            marginLeft: 5
          }}>
            <Text style={{ color: COLOR.WHITE, fontSize: 10, fontWeight: 'bold' }}>{winStr}</Text>
          </View>
          :
          <View style={{ width: 17, height: 17, marginLeft: 5 }} />
      }

    </View>
  )
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  itemContainer: {
    flex: 1,
    padding: 10,
    flexDirection: 'column',
    backgroundColor: COLOR.WHITE,
    shadowColor: '#000',
    borderRadius: 10,
    shadowRadius: 5,
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.03,
    marginHorizontal: 5,
    marginTop: 10
  },
  containerScore: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  scoreStyle: {
    marginHorizontal: 10,
    padding: 3,
    minWidth: 50,
    borderRadius: 6,
    backgroundColor: COLOR.BLACK,
  },
  resultTextStyle: {
    color: COLOR.WHITE,
    fontWeight: 'bold',
    textAlign: 'center', fontSize: 18
  },
});