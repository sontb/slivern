import { UserPredictApi } from 'api';
import { GuideTip, Loading } from 'components';
import ItemComment, { IItemComment } from 'components/ItemComment';
import { COLOR } from 'configs';
import { FirebaseDbHelper, NavigationService, TYPE_NOTIFY } from 'helpers';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import React, { Component, forwardRef, useImperativeHandle, useRef } from 'react';
import {
  Alert,
  KeyboardAvoidingView,
  Platform, RefreshControl, ScrollView, StyleProp,
  StyleSheet, Text,
  TextInput, TouchableOpacity, TouchableWithoutFeedback, View,
  ViewStyle
} from 'react-native';
import Modal from 'react-native-modal';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon5 from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/Ionicons';
import { SCREENS_NAME } from 'screens/ScreenName';
import ItemTips, { IItemTips } from 'screens/tips/ItemTips';
import { ICommonStore } from 'stores/CommonStore';
import { ILoginStore } from 'stores/LoginStore';
import { trans } from 'trans';
import ItemMatch, { IItemMatch } from './ItemMatch';

// import { SCREENS_NAME } from 'screens';

const ITEM_HEIGHT = 50
type Props = {
  itemMatch?: IItemMatch,
  itemTips?: IItemTips,
  type: number,
  style?: StyleProp<ViewStyle>,
  onCancel: () => void,
  showPopupComment: boolean,
  loginStore?: ILoginStore,
  commonStore?: ICommonStore,
  onComment?: (requestRefreshList: boolean) => void,
  hideItem?: boolean,
  requestRefreshList?: boolean,
};


@inject('loginStore', 'commonStore')
@observer
export default class PopupComment extends Component<Props> {

  state = {
    contentComment: '',
    listComment: [],
    loading: false,
    showInput: false,
    mention: '',
    showOptionChoice: false,
    currentComment: null,
    refreshing: false,
    countComment: 0,
  }

  userPredictApi = new UserPredictApi()
  page = 0
  item = 5

  // componentDidUpdate(previousProps: Props, previousState: Props) {
  //   if (this.props.showPopupComment && !previousProps.showPopupComment) {
  //     // make the API call here
  //     this.page = 0
  //     this.getData()
  //   }
  // }

  componentDidMount() {
    // this.props.commonStore?.hidingTabBar()
    this.getData()
  }

  onCancel = () => {
    // this.props.commonStore?.showTabBar()
    this.props.onCancel()
    this.setState({ loading: true, listComment: [], mention: '', showInput: false })
  }

  getData = async () => {
    let { itemMatch, itemTips, type } = this.props
    try {
      if (this.page == 0) {
        await this.setState({ loading: true, listComment: [], mention: '', showInput: false, contentComment: '' })
      } else {
        await this.setState({ loading: true })
      }
      let id = type == 1 ? itemTips!.id : itemMatch!.id;

      let listData = await this.userPredictApi.userListComment(id, this.page, this.item, type)
      if (listData) {
        if (this.page == 0) {
          this.setState({ listComment: listData, loading: false })
        } else {
          this.setState({ listComment: [...this.state.listComment, ...listData], loading: false })
        }
      }
    } catch {
      // this.setState({ loading: false })

    }
    this.setState({ loading: false })

  }

  onSumbmit = async () => {
    let { loginStore, itemMatch, itemTips, type } = this.props
    if (this.state.contentComment.length == 0) {
      Alert.alert("Thông báo", "Vui lòng nhập bình luận")
      return
    }
    try {
      this.setState({ loading: true, showInput: false })
      let email = '';
      let point = 0;
      let test = this.state.contentComment
      console.log(test)
      if (loginStore!.user != null) {
        email = loginStore!.user.email;
        point = loginStore!.userPredict.point
      }
      let id = type == 1 ? itemTips!.id : itemMatch!.id;
      let res = await this.userPredictApi.userCommentMatch(email, id, this.state.contentComment, type, this.state.mention)

      if (res.success == 1) {
        let itemComment: IItemComment = {
          id: moment().unix(),
          content: this.state.contentComment,
          point,
          photo: loginStore!.user.photo,
          mention: this.state.mention,
          email: loginStore!.user.email,
          date_create: moment().unix(),
          name: loginStore!.user.displayName,
          position: loginStore!.userPredict.position,
        }
        let listComment = [itemComment, ... this.state.listComment]
        this.setState({ listComment, loading: false, contentComment: '' })
        let topic = ''
        let type = ''
        let message = ''
        let data: any
        if (itemMatch) {
          itemMatch.countComment = itemMatch.countComment! + 1
          itemMatch.dateComment = itemComment.date_create
          itemMatch.nameComment = itemComment.name
          itemMatch.emailComment = itemComment.email
          itemMatch.photoComment = itemComment.photo
          itemMatch.mentionComment = itemComment.mention
          itemMatch.contentComment = itemComment.content
          itemMatch.userPointComment = itemComment.point
          itemMatch.positionComment = itemComment.position
          topic = `comment_match_${itemMatch?.id}`
          type = TYPE_NOTIFY.MatchComment
          message = `${loginStore!.user.displayName} cũng đã bình luận trận đấu ${itemMatch?.homeName} - ${itemMatch?.awayName}`
          data = itemMatch
        } else if (itemTips) {
          itemTips.countComment = itemTips.countComment! + 1
          itemTips.dateComment = itemComment.date_create
          itemTips.nameComment = itemComment.name
          itemTips.emailComment = itemComment.email
          itemTips.photoComment = itemComment.photo
          itemTips.mentionComment = itemComment.mention
          itemTips.contentComment = itemComment.content
          itemTips.userPointComment = itemComment.point
          itemTips.positionComment = itemComment.position
          topic = `comment_news_${itemTips?.id}`
          type = TYPE_NOTIFY.TipsComment
          message = `${loginStore!.user.displayName} cũng đã bình luận bài tin ${itemTips.title}`
          data = itemTips
        }
        FirebaseDbHelper.subscribeToTopic(topic)
        this.userPredictApi.notifyNow(message, data, topic, type)
        this.props.onComment && this.props.onComment(this.props.requestRefreshList ? true : false)
      }
    } catch {

    }
    this.setState({ loading: false })
  }

  _renderItem = ({ item, index }: { item: IItemComment, index: number }) => {
    return (
      <ItemComment itemComment={item} key={item.id} style={{ marginHorizontal: 10, marginTop: 10 }} />
    )
  }

  onLoadMore = () => {
    this.page++
    this.getData()
  }

  onOpenInput = () => {
    this.setState({ showInput: true, contentComment: '' })
  }

  onPressName = (item: IItemComment) => {
    this.setState({ showOptionChoice: true, currentComment: item })
  }

  onMention = async () => {
    // if (Platform.OS === 'android') {
    this.setState({ showOptionChoice: false })
    setTimeout(() => {
      this.setState({ showInput: true, mention: this.state.currentComment!.name, contentComment: '' })
    }, 500)
    // } else {
    //   this.setState({ showInput: true, mention: this.state.currentComment!.name, showOptionChoice: false, contentComment: '' })

    // }

  }

  onOpenHistory = () => {
    this.setState({ showOptionChoice: false })
    NavigationService.navigate(SCREENS_NAME.PredictHistory, { email: this.state.currentComment!.email, showOptionChoice: false });
    // this.props.onCancel()
  }

  _onRefresh = async () => {
    this.setState({ refreshing: true })
    this.page == 0
    await this.getData()
    this.setState({ refreshing: false })
  }

  render() {
    return (
      <Modal
        // animationType="fade"
        // transparent={true}
        isVisible={this.props.showPopupComment}
        swipeDirection='down'
        onSwipeComplete={this.onCancel}
        style={{ margin: 0, flex: 1 }}
        propagateSwipe
        coverScreen={false}

        onBackButtonPress={this.onCancel}
      >
        <View style={{ flex: 1 }}>

          <TouchableWithoutFeedback onPress={this.onCancel}>
            <SafeAreaView >
              <View style={{ height: 100 }} />
            </SafeAreaView>
          </TouchableWithoutFeedback>

          <View style={{
            backgroundColor: COLOR.BG, flex: 1,
            borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden'
          }} >
            <View style={{
              backgroundColor: COLOR.WHITE, flexDirection: 'row',
              paddingVertical: 10, borderBottomWidth: 1,
              borderBottomColor: COLOR.BORDER, alignItems: 'center', paddingHorizontal: 10,
            }}>
              <TouchableOpacity style={{ padding: 7 }} onPress={this.onCancel}>
                {/* <Icon name='arrow-back' style={{ color: COLOR.WHITE, fontSize: 30 }} /> */}
                <Icon5 name='chevron-left' style={{ color: COLOR.MAIN_TEXT, fontSize: 25, }} />
              </TouchableOpacity>
              <Text style={{
                flex: 1, fontSize: 18, fontWeight: 'bold', color: COLOR.MAIN_TEXT,
                textAlign: 'center'
              }}>Bình Luận</Text>
              <View style={{ width: 39 }} />
            </View>
            <ScrollView keyboardShouldPersistTaps='never'
              contentContainerStyle={{ paddingBottom: 100 }}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }>
              <View>

                {
                  this.props.type == 0 && !this.props.hideItem &&
                  <ItemMatch itemMatch={this.props.itemMatch!} showComment={false} style={{ marginTop: 5 }} />
                }
                {
                  this.props.type == 1 && !this.props.hideItem &&
                  <ItemTips itemTips={this.props.itemTips!} showComment={false} style={{ marginBottom: 5 }} />
                }
                {
                  this.props.loginStore?.isShowTipComment &&
                  <GuideTip text={trans.helpComment} onPressClose={() => this.props.loginStore?.closeTipComment()} />
                }
                {
                  !this.state.showInput &&

                  <TouchableOpacity style={{
                    flex: 1,
                    flexDirection: 'row', alignItems: 'center',
                    backgroundColor: COLOR.OVERLEY_DARK_LIGHT,
                    borderRadius: 10,
                    paddingVertical: 10,
                    paddingHorizontal: 10,
                    marginHorizontal: 15,
                    marginTop: 5
                  }} onPress={this.onOpenInput}>
                    <Text style={{ color: COLOR.SUB_TEXT }}>Nhập bình luận</Text>
                  </TouchableOpacity>

                }

                {
                  this.state.listComment.map((item: IItemComment, index) => {
                    return (
                      <ItemComment itemComment={item}
                        style={{ marginHorizontal: 10, marginTop: 10 }} key={item.id}
                        onPressName={() => this.onPressName(item)}
                        onPressAvatar={() => this.onPressName(item)} />
                    )
                  })
                }
                {
                  this.state.listComment.length > 0 &&
                  this.state.listComment.length < this.props.itemMatch?.countComment! &&
                  <TouchableOpacity style={{
                    borderRadius: 20, backgroundColor: COLOR.OVERLEY_LIGHT, marginLeft: 50,
                    alignSelf: 'baseline', paddingHorizontal: 15, marginTop: 10, marginBottom: 10,
                    paddingVertical: 3,
                  }} onPress={this.onLoadMore}>
                    <Text style={{
                      fontSize: 30, marginTop: -15,
                      color: COLOR.SUB_TEXT
                    }}>...</Text>
                  </TouchableOpacity>
                }
              </View>
            </ScrollView>
          </View>

          <Loading loading={this.state.loading} />


        </View>
        {
          this.state.showInput &&
          <InputComment
            onChangeText={(text) => this.setState({ contentComment: text })}
            onSubmitEditing={this.onSumbmit}
            value={this.state.contentComment}
            onBlur={() => this.setState({ showInput: false, mention: '' })}
            mention={this.state.mention}
          // ref={(ref) => this.inputComment = ref}
          />
        }
        <OptionChoice showOptionChoice={this.state.showOptionChoice}
          onCancel={() => this.setState({ showOptionChoice: false })}
          title={this.state.currentComment?.name!}
          onMention={this.onMention}
          onOpenHistory={this.onOpenHistory} />

      </Modal>

    );
  }
}

const InputComment = forwardRef((props:
  {
    onSubmitEditing: () => void,
    onChangeText: (text: string) => void,
    value: string,
    ref?: (ref: any) => void,
    onFocus?: () => void,
    onBlur?: () => void,
    mention?: string,
  }, ref) => {
  var refTextInput = useRef()

  useImperativeHandle(ref, () => ({
    focus: () => {
      refTextInput.current!.focus()
    }
  }));

  return (
    <KeyboardAvoidingView style={{ position: 'absolute', bottom: 0, left: 0, right: 0 }}
      behavior={Platform.OS === 'ios' ? 'position' : undefined} enabled>

      <View style={{
        flexDirection: 'row', backgroundColor: COLOR.BG_GREY,
        padding: 10, alignItems: 'center'
      }}>
        <View style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center', alignItems: 'center',
          backgroundColor: COLOR.WHITE,
          borderRadius: 10,
          paddingVertical: Platform.OS === 'ios' ? 10 : 0,
          paddingHorizontal: 10,
        }}>
          {
            props.mention ?
              <Text numberOfLines={1}
                style={{
                  color: COLOR.MAIN_TEXT,
                  width: 50, fontWeight: 'bold',
                  marginRight: 5,
                }}
                ellipsizeMode='tail'>{props.mention}</Text>
              :
              null
          }
          <TextInput
            ref={refTextInput}
            value={props.value}
            autoFocus={true}
            returnKeyType='send'
            style={{ flex: 1 }}
            selectionColor={COLOR.MAIN}
            placeholder={'Nhập bình luận'}
            onSubmitEditing={props.onSubmitEditing}
            onChangeText={props.onChangeText}
            maxLength={200}
            onFocus={props.onFocus}
            onBlur={props.onBlur}
            placeholderTextColor={COLOR.SUB_TEXT}
            underlineColorAndroid="transparent" />
        </View>
        <TouchableOpacity style={{ padding: 10 }} onPress={props.onSubmitEditing}>
          <Icon name='md-send' style={{ fontSize: 25, color: COLOR.MAIN }} />
        </TouchableOpacity>
      </View>
    </KeyboardAvoidingView>

  )
})

const OptionChoice = (props: {
  showOptionChoice: boolean, onCancel: () => void,
  title: string, onMention: () => void, onOpenHistory: () => void
}) => {
  return (
    <Modal
      isVisible={props.showOptionChoice}
      style={{ margin: 0, flex: 1 }}
      animationIn='fadeIn'
      animationInTiming={100}
      animationOut='fadeOut'
      animationOutTiming={100}
    >
      <TouchableWithoutFeedback onPress={props.onCancel}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{ backgroundColor: COLOR.WHITE, borderRadius: 15 }} >
            <TouchableWithoutFeedback>
              <Text style={{
                alignSelf: 'stretch', textAlign: 'center', padding: 15, fontWeight: 'bold',
                fontSize: 18
              }}>{props.title}</Text>
            </TouchableWithoutFeedback>
            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: COLOR.OVERLEY_DARK }} />
            <TouchableOpacity style={{
              alignItems: 'center',
              justifyContent: 'center', alignSelf: 'stretch',
              padding: 10
            }} onPress={props.onMention}>
              <Text style={{ marginHorizontal: 30, fontSize: 16, color: COLOR.BLUE }}>Nhắc đến trong bình luận</Text>
            </TouchableOpacity>
            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: COLOR.OVERLEY_DARK }} />

            {/* <TouchableOpacity style={{
              alignItems: 'center',
              justifyContent: 'center', alignSelf: 'stretch',
              padding: 10
            }} onPress={props.onOpenHistory}>
              <Text style={{ marginHorizontal: 20, fontSize: 16, color: COLOR.BLUE }}>Xem lịch sử cược</Text>
            </TouchableOpacity> */}
            <View style={{ alignSelf: 'stretch', height: 1, backgroundColor: COLOR.OVERLEY_DARK }} />

            <TouchableOpacity style={{
              alignItems: 'center',
              justifyContent: 'center', alignSelf: 'stretch',
              padding: 10
            }} onPress={props.onCancel}>
              <Text style={{ color: COLOR.RED, fontWeight: 'bold', fontSize: 18 }}>Hủy</Text>
            </TouchableOpacity>
          </View>
        </View>
      </TouchableWithoutFeedback>

    </Modal>
  )
}


const styles = StyleSheet.create({
  containRow: { paddingTop: 0, alignItems: 'flex-start', height: ITEM_HEIGHT },
  viewRow: { flexDirection: 'row', marginHorizontal: 30, flex: 1, alignItems: 'center' },
  textRow: { color: COLOR.MAIN_TEXT, fontSize: 18, flex: 1 },
  icon: { color: COLOR.MAIN_TEXT, fontSize: 20, marginLeft: 10 },
  empty: { width: 20, height: 20 },
  line: { alignSelf: 'stretch', height: 1, backgroundColor: COLOR.OVERLEY_DARK, marginHorizontal: 10 },
})