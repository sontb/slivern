import React, { PureComponent } from 'react';
import { View, StyleSheet, Text, ActivityIndicator } from 'react-native';
import { COLOR } from 'configs';

export const TYPE_LOADING = 'TYPE_LOADING';

export default class ItemLoading extends PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size='large'/>        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 3,
    paddingBottom: 3,
    alignItems: 'center',
    justifyContent: 'center',
  },
});